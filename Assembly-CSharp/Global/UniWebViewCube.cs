﻿// Decompiled with JetBrains decompiler
// Type: UniWebViewCube
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UniWebViewCube : MonoBehaviour
{
  private bool firstHit = true;
  public UniWebDemo webViewDemo;
  private float startTime;

  private void Start()
  {
    this.startTime = Time.time;
  }

  private void OnCollisionEnter(Collision col)
  {
    if (!(col.gameObject.name == "Target"))
      return;
    this.webViewDemo.ShowAlertInWebview(Time.time - this.startTime, this.firstHit);
    this.firstHit = false;
  }
}
