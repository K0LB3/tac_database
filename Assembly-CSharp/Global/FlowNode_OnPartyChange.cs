﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_OnPartyChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("Event/OnPartyChange", 58751)]
[FlowNode.Pin(1, "On", FlowNode.PinTypes.Output, 1)]
public class FlowNode_OnPartyChange : FlowNodeEvent<FlowNode_OnPartyChange>
{
}
