﻿// Decompiled with JetBrains decompiler
// Type: PhotonNetworkingMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

public enum PhotonNetworkingMessage
{
  OnConnectedToPhoton,
  OnLeftRoom,
  OnMasterClientSwitched,
  OnPhotonCreateRoomFailed,
  OnPhotonJoinRoomFailed,
  OnCreatedRoom,
  OnJoinedLobby,
  OnLeftLobby,
  OnDisconnectedFromPhoton,
  OnConnectionFail,
  OnFailedToConnectToPhoton,
  OnReceivedRoomListUpdate,
  OnJoinedRoom,
  OnPhotonPlayerConnected,
  OnPhotonPlayerDisconnected,
  OnPhotonRandomJoinFailed,
  OnConnectedToMaster,
  OnPhotonSerializeView,
  OnPhotonInstantiate,
  OnPhotonMaxCccuReached,
  OnPhotonCustomRoomPropertiesChanged,
  OnPhotonPlayerPropertiesChanged,
  OnUpdatedFriendList,
  OnCustomAuthenticationFailed,
  OnCustomAuthenticationResponse,
  OnWebRpcResponse,
  OnOwnershipRequest,
  OnLobbyStatisticsUpdate,
  OnPhotonPlayerActivityChanged,
  OnOwnershipTransfered,
}
