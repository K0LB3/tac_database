﻿// Decompiled with JetBrains decompiler
// Type: Photon.MonoBehaviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Photon
{
  public class MonoBehaviour : UnityEngine.MonoBehaviour
  {
    private PhotonView pvCache;

    public PhotonView photonView
    {
      get
      {
        if ((Object) this.pvCache == (Object) null)
          this.pvCache = PhotonView.Get((Component) this);
        return this.pvCache;
      }
    }
  }
}
