﻿// Decompiled with JetBrains decompiler
// Type: rapidjson.InvalidPointerError
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace rapidjson
{
  public class InvalidPointerError : Exception
  {
    public InvalidPointerError(string pointer)
      : base(pointer)
    {
    }
  }
}
