﻿// Decompiled with JetBrains decompiler
// Type: NazimaseVolume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("SRPG/背景ツール/なじませボリューム")]
public class NazimaseVolume : MonoBehaviour
{
  private void Awake()
  {
    this.tag = "EditorOnly";
  }

  public Bounds Bounds
  {
    get
    {
      return new Bounds(this.transform.position, this.transform.localScale);
    }
  }
}
