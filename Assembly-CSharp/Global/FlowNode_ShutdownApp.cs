﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ShutdownApp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("System/Shutdown Application")]
[FlowNode.Pin(1, "Shutdown", FlowNode.PinTypes.Input, 0)]
public class FlowNode_ShutdownApp : FlowNode
{
  public override void OnActivate(int pinID)
  {
    if (pinID != 1)
      return;
    Application.Quit();
  }
}
