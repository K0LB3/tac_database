﻿// Decompiled with JetBrains decompiler
// Type: MailIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MailIcon : MonoBehaviour
{
  public GameObject ItemIconTemplate;
  public GameObject CoinIconTemplate;
  public GameObject GoldIconTemplate;
  public GameObject ArenaCoinIconTemplate;
  public GameObject MultiCoinIconTemplate;
  public GameObject EventCoinIconTemplate;
  public GameObject KakeraCoinIconTemplate;
  public GameObject SetIconTemplate;
  public GameObject ArtifactIconTemplate;
  public GameObject CurrentIcon;
}
