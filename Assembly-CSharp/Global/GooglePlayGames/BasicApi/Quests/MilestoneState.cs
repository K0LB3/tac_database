﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.Quests.MilestoneState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi.Quests
{
  public enum MilestoneState
  {
    NotStarted = 1,
    NotCompleted = 2,
    CompletedNotClaimed = 3,
    Claimed = 4,
  }
}
