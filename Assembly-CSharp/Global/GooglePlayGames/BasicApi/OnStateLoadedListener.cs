﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.OnStateLoadedListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi
{
  public interface OnStateLoadedListener
  {
    void OnStateLoaded(bool success, int slot, byte[] data);

    byte[] OnStateConflict(int slot, byte[] localData, byte[] serverData);

    void OnStateSaved(bool success, int slot);
  }
}
