﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.Native.AppStateClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using GooglePlayGames.BasicApi;

namespace GooglePlayGames.Native
{
  internal interface AppStateClient
  {
    void LoadState(int slot, OnStateLoadedListener listener);

    void UpdateState(int slot, byte[] data, OnStateLoadedListener listener);
  }
}
