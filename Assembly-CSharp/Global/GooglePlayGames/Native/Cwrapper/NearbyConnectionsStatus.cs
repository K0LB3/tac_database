﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.Native.Cwrapper.NearbyConnectionsStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.Native.Cwrapper
{
  internal static class NearbyConnectionsStatus
  {
    internal enum InitializationStatus
    {
      ERROR_VERSION_UPDATE_REQUIRED = -4,
      ERROR_INTERNAL = -2,
      VALID = 1,
    }
  }
}
