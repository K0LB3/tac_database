﻿// Decompiled with JetBrains decompiler
// Type: SceneManagerHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine.SceneManagement;

public class SceneManagerHelper
{
  public static string ActiveSceneName
  {
    get
    {
      return SceneManager.GetActiveScene().name;
    }
  }

  public static int ActiveSceneBuildIndex
  {
    get
    {
      return SceneManager.GetActiveScene().buildIndex;
    }
  }
}
