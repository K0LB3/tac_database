﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatUserStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public static class ChatUserStatus
  {
    public const int Offline = 0;
    public const int Invisible = 1;
    public const int Online = 2;
    public const int Away = 3;
    public const int DND = 4;
    public const int LFG = 5;
    public const int Playing = 6;
  }
}
