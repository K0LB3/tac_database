﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.CustomAuthenticationType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public enum CustomAuthenticationType : byte
  {
    Custom = 0,
    Steam = 1,
    Facebook = 2,
    Oculus = 3,
    PlayStation = 4,
    Xbox = 5,
    None = 255, // 0xFF
  }
}
