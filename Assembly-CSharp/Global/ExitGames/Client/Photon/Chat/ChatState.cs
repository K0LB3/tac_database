﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public enum ChatState
  {
    Uninitialized,
    ConnectingToNameServer,
    ConnectedToNameServer,
    Authenticating,
    Authenticated,
    DisconnectingFromNameServer,
    ConnectingToFrontEnd,
    ConnectedToFrontEnd,
    DisconnectingFromFrontEnd,
    QueuedComingFromFrontEnd,
    Disconnecting,
    Disconnected,
  }
}
