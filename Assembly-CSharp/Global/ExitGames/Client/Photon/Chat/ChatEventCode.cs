﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatEventCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ChatEventCode
  {
    public const byte ChatMessages = 0;
    public const byte Users = 1;
    public const byte PrivateMessage = 2;
    public const byte FriendsList = 3;
    public const byte StatusUpdate = 4;
    public const byte Subscribe = 5;
    public const byte Unsubscribe = 6;
  }
}
