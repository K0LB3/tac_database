﻿// Decompiled with JetBrains decompiler
// Type: SortingOrder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("Rendering/SortingOrder")]
[RequireComponent(typeof (Renderer))]
[ExecuteInEditMode]
public class SortingOrder : MonoBehaviour
{
  [SerializeField]
  private int mSortingOrder;

  private void Awake()
  {
    this.enabled = false;
  }

  private void OnValidate()
  {
    this.GetComponent<Renderer>().sortingOrder = this.mSortingOrder;
  }
}
