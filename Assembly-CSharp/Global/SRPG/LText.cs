﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class LText : Text
  {
    private string mCurrentText;

    private void LateUpdate()
    {
      if (!Application.isPlaying)
        return;
      if (string.IsNullOrEmpty(this.mCurrentText))
      {
        if (string.IsNullOrEmpty(this.text))
          return;
      }
      else if (!string.IsNullOrEmpty(this.text) && this.mCurrentText.Equals(this.text))
        return;
      this.text = LocalizedText.Get(this.text);
      this.mCurrentText = this.text;
    }
  }
}
