﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TowerRecoverData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class TowerRecoverData
  {
    public string towerID;
    public int useCoin;

    public TowerRecoverData(string towerID, int useCoin)
    {
      this.towerID = towerID;
      this.useCoin = useCoin;
    }
  }
}
