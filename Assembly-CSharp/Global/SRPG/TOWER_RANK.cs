﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TOWER_RANK
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum TOWER_RANK
  {
    S,
    A_PLUS,
    A,
    A_MINUS,
    B_PLUS,
    B,
    B_MINUS,
    C_PLUS,
    C,
    C_MINUS,
    D_PLUS,
    D,
    D_MINUS,
    E_PLUS,
    E,
    E_MINUS,
    NUM,
  }
}
