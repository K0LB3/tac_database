﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_LogoutFacebook
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using Facebook.Unity;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("System/LogoutFacebook", 32741)]
  [FlowNode.Pin(0, "Request", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(1, "Success", FlowNode.PinTypes.Output, 1)]
  public class FlowNode_LogoutFacebook : FlowNode
  {
    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      if (AccessToken.CurrentAccessToken != null)
      {
        PlayerPrefs.SetInt("AccountLinked", 0);
        FB.LogOut();
      }
      this.ActivateOutputLinks(1);
    }
  }
}
