﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum QuestTypes : byte
  {
    Story,
    Multi,
    Arena,
    Tutorial,
    Free,
    Event,
    Character,
    Tower,
    VersusFree,
    VersusRank,
    Gps,
    Extra,
    MultiTower,
    Beginner,
  }
}
