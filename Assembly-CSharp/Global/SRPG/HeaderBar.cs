﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HeaderBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class HeaderBar : PropertyAttribute
  {
    public string Text;
    public Color BGColor;
    public Color FGColor;

    public HeaderBar(string text)
    {
      this.Text = text;
      this.BGColor = new Color(0.0f, 0.2f, 0.5f);
      this.FGColor = Color.white;
    }

    public HeaderBar(string text, Color bg)
    {
      this.Text = text;
      this.BGColor = bg;
      this.FGColor = Color.white;
    }

    public HeaderBar(string text, Color bg, Color fg)
    {
      this.Text = text;
      this.BGColor = bg;
      this.FGColor = fg;
    }
  }
}
