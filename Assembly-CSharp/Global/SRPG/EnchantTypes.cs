﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EnchantTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum EnchantTypes
  {
    Poison,
    Paralysed,
    Stun,
    Sleep,
    Charm,
    Stone,
    Blind,
    DisableSkill,
    DisableMove,
    DisableAttack,
    Zombie,
    DeathSentence,
    Berserk,
    Knockback,
    ResistBuff,
    ResistDebuff,
    Stop,
    Fast,
    Slow,
    AutoHeal,
    Donsoku,
    Rage,
    GoodSleep,
    AutoJewel,
    DisableHeal,
    SingleAttack,
    AreaAttack,
    DecCT,
    IncCT,
  }
}
