﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RecipeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RecipeParam
  {
    public string iname;
    public int cost;
    public string mat1;
    public string mat2;
    public string mat3;
    public string mat4;
    public string mat5;
    public int num1;
    public int num2;
    public int num3;
    public int num4;
    public int num5;
  }
}
