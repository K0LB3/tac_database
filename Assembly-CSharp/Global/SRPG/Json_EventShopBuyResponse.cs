﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_EventShopBuyResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_EventShopBuyResponse
  {
    public Json_Currencies currencies;
    public Json_Item[] items;
    public JSON_EventShopItemListSet[] shopitems;
    public Json_MailInfo mail_info;
  }
}
