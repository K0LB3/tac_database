﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_PlayerParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_PlayerParam
  {
    public int pt;
    public int ucap;
    public int icap;
    public int ecap;
    public int fcap;
  }
}
