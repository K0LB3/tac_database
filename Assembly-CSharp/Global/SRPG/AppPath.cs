﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AppPath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public static class AppPath
  {
    public static string persistentDataPath
    {
      get
      {
        return Application.persistentDataPath;
      }
    }

    public static string temporaryCachePath
    {
      get
      {
        return Application.temporaryCachePath;
      }
    }

    public static string assetCachePath
    {
      get
      {
        return Application.persistentDataPath;
      }
    }

    public static string assetCachePathOld
    {
      get
      {
        return Application.temporaryCachePath;
      }
    }
  }
}
