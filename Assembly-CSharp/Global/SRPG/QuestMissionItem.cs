﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestMissionItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class QuestMissionItem : MonoBehaviour
  {
    public GameParameter Star;
    public GameParameter FrameParam;
    public GameParameter IconParam;
    public GameParameter NameParam;
    public GameParameter AmountParam;
    public GameParameter ObjectigveParam;
  }
}
