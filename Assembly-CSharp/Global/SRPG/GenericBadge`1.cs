﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenericBadge`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  internal class GenericBadge<T> where T : class
  {
    public bool mValue;
    public T mRawData;

    public GenericBadge(T data, bool value = false)
    {
      this.mRawData = data;
      this.mValue = value;
    }
  }
}
