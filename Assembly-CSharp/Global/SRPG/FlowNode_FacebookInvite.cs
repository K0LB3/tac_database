﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_FacebookInvite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using Facebook.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Success", FlowNode.PinTypes.Output, 1)]
  [FlowNode.NodeType("System/FacebookInvite", 32741)]
  [FlowNode.Pin(0, "Request", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(2, "FacebookNotConnected", FlowNode.PinTypes.Output, 2)]
  public class FlowNode_FacebookInvite : FlowNode
  {
    private string lastResponse = string.Empty;
    private string ApiQuery = string.Empty;
    private string applink_url = "https://fb.me/1669343083078137";
    private string image_url = "http://prod-dlc-alcww-gumi-sg.akamaized.net/social/friend_invite_image.png";

    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      if (AccessToken.CurrentAccessToken != null)
        this.InviteFriend();
      else
        FB.LogInWithReadPermissions((IEnumerable<string>) new List<string>()
        {
          "public_profile",
          "email",
          "user_friends"
        }, new FacebookDelegate<ILoginResult>(this.HandleResult));
    }

    private void InviteFriend()
    {
      FB.Mobile.AppInvite(new Uri(this.applink_url), new Uri(this.image_url), (FacebookDelegate<IAppInviteResult>) (result => Debug.Log((object) result.RawResult)));
      this.ActivateOutputLinks(1);
    }

    protected void HandleResult(IResult result)
    {
      if (result == null)
      {
        Debug.Log((object) "FlowNode_FacebookShare | Null Response.");
        this.ActivateOutputLinks(2);
      }
      else if (!string.IsNullOrEmpty(result.Error))
      {
        Debug.Log((object) ("FlowNode_FacebookShare | Error: " + result.Error));
        this.ActivateOutputLinks(2);
      }
      else if (result.Cancelled)
      {
        Debug.Log((object) ("FlowNode_FacebookShare | Error: " + result.RawResult));
        this.ActivateOutputLinks(2);
      }
      else if (!string.IsNullOrEmpty(result.RawResult))
      {
        Debug.Log((object) ("FlowNode_FacebookShare | Success: " + result.RawResult));
        this.InviteFriend();
      }
      else
      {
        Debug.Log((object) "FlowNode_FacebookShare | Empty Response.");
        this.ActivateOutputLinks(2);
      }
    }
  }
}
