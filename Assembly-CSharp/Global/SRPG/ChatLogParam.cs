﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatLogParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class ChatLogParam
  {
    public long id;
    public byte message_type;
    public string fuid;
    public string uid;
    public string icon;
    public string skin_iname;
    public string job_iname;
    public string message;
    public int stamp_id;
    public string name;
    public long posted_at;
  }
}
