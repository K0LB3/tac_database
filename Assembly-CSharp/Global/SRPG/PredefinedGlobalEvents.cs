﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PredefinedGlobalEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum PredefinedGlobalEvents
  {
    REFRESH_COIN_STATUS,
    REFRESH_GOLD_STATUS,
    UNLOAD_MENU,
    ERROR_NETWORK,
    BACK_NETWORK,
    MAINTENANCE_NETWORK,
    REFRESH_PLAYER_STATUS,
    ERROR_APP_QUIT,
  }
}
