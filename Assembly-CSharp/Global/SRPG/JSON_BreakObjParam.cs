﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BreakObjParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_BreakObjParam
  {
    public string iname;
    public string name;
    public string expr;
    public string unit_id;
    public int clash_type;
    public int ai_type;
    public int side_type;
    public int ray_type;
    public int is_ui;
    public string rest_hps;
    public int clock;
    public int appear_dir;
  }
}
