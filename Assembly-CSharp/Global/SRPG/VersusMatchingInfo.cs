﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusMatchingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Matching", FlowNode.PinTypes.Input, 1)]
  public class VersusMatchingInfo : MonoBehaviour, IFlowInterface
  {
    public void Start()
    {
      MonoSingleton<GameManager>.Instance.AudienceMode = false;
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      ProgressWindow.OpenVersusLoadScreen();
    }
  }
}
