﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ArenaResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ArenaResult
  {
    public int rank;
    public int coin;
    public int gold;
    public int ac;
    public string item1;
    public string item2;
    public string item3;
    public string item4;
    public string item5;
    public int num1;
    public int num2;
    public int num3;
    public int num4;
    public int num5;
  }
}
