﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_SectionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_SectionParam
  {
    public string iname;
    public string name;
    public string expr;
    public long start;
    public long end;
    public int hide;
    public string home;
    public string unit;
    public string item;
    public string shop;
    public string inn;
    public string bar;
    public string bgm;
  }
}
