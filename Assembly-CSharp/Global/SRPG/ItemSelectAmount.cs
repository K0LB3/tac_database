﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ItemSelectAmount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ItemSelectAmount : MonoBehaviour, IGameParameter
  {
    public void UpdateValue()
    {
      ItemSelectListItemData dataOfClass = DataSource.FindDataOfClass<ItemSelectListItemData>(this.gameObject, (ItemSelectListItemData) null);
      Text component = this.gameObject.GetComponent<Text>();
      if (!((UnityEngine.Object) component != (UnityEngine.Object) null) || dataOfClass == null)
        return;
      component.text = dataOfClass.num.ToString();
    }
  }
}
