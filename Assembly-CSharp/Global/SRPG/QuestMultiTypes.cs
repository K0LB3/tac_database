﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestMultiTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum QuestMultiTypes : short
  {
    NOT_MULTI = 0,
    RAID = 1,
    VERSUS = 2,
    EVENT_TOP = 100, // 0x0064
    RAID_EVENT = 101, // 0x0065
    VERSUS_EVENT = 102, // 0x0066
    TOWER_EVENT = 103, // 0x0067
  }
}
