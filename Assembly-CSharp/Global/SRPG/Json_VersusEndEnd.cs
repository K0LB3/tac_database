﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_VersusEndEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_VersusEndEnd : Json_PlayerDataAll
  {
    public JSON_QuestProgress[] quests;
    public int wincnt;
    public int win_bonus;
    public int key;
    public int rankup;
    public int floor;
    public int arravied;
  }
}
