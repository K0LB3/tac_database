﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayGimmickEventParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class MultiPlayGimmickEventParam
  {
    public int count;
    public int completed;
  }
}
