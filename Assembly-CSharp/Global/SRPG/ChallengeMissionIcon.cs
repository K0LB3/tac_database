﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChallengeMissionIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using GR;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(0, "更新", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "進捗表示", FlowNode.PinTypes.Output, 10)]
  [FlowNode.Pin(2, "非表示", FlowNode.PinTypes.Input, 2)]
  [FlowNode.Pin(1, "表示", FlowNode.PinTypes.Input, 1)]
  public class ChallengeMissionIcon : MonoBehaviour, IFlowInterface
  {
    public GameObject Badge;
    public Text BadgeCount;

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 0:
          this.Refresh();
          break;
        case 1:
          this.ShowImages(true);
          break;
        case 2:
          if ((MonoSingleton<GameManager>.Instance.Player.TutorialFlags & 1L) == 0L)
            break;
          this.ShowImages(false);
          break;
      }
    }

    public void ShowImages(bool value)
    {
      Image component1 = this.GetComponent<Image>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        component1.enabled = value;
      Button component2 = this.GetComponent<Button>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        component2.enabled = value;
      if (!((UnityEngine.Object) this.Badge != (UnityEngine.Object) null))
        return;
      Image component3 = this.Badge.GetComponent<Image>();
      if (!((UnityEngine.Object) component3 != (UnityEngine.Object) null))
        return;
      component3.enabled = value;
    }

    private void Start()
    {
      this.StartCoroutine(this.SetAsLastSibling());
    }

    [DebuggerHidden]
    private IEnumerator SetAsLastSibling()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ChallengeMissionIcon.\u003CSetAsLastSibling\u003Ec__IteratorE6()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void Refresh()
    {
      if (!((UnityEngine.Object) MonoSingleton<GameManager>.Instance != (UnityEngine.Object) null))
        return;
      bool flag1 = false;
      bool flag2 = true;
      int num = 0;
      foreach (TrophyParam trophyParam in ChallengeMission.GetRootTrophiesSortedByPriority())
      {
        if (!ChallengeMission.GetTrophyCounter(trophyParam).IsEnded)
        {
          num += this.UncollectedRewardCount(trophyParam);
          flag2 = false;
          if (num > 0)
          {
            flag1 = true;
            break;
          }
        }
      }
      this.Badge.SetActive(flag1);
      if (flag1)
        this.BadgeCount.text = num.ToString();
      this.gameObject.SetActive(!flag2);
      if (flag2)
        return;
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }

    private int UncollectedRewardCount(TrophyParam rootTrophy)
    {
      TrophyParam[] childeTrophies = ChallengeMission.GetChildeTrophies(rootTrophy);
      int num = 0;
      foreach (TrophyParam trophy in childeTrophies)
      {
        TrophyState trophyCounter = ChallengeMission.GetTrophyCounter(trophy);
        if (trophyCounter.IsCompleted && !trophyCounter.IsEnded)
          ++num;
      }
      return num;
    }
  }
}
