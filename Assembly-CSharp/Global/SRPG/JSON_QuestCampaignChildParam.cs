﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestCampaignChildParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_QuestCampaignChildParam
  {
    public string iname;
    public int scope;
    public int quest_type;
    public int quest_mode;
    public string quest_id;
    public string unit;
    public int drop_rate;
    public int drop_num;
    public int exp_player;
    public int exp_unit;
    public int ap_rate;
  }
}
