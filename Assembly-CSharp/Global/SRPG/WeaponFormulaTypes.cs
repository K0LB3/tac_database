﻿// Decompiled with JetBrains decompiler
// Type: SRPG.WeaponFormulaTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum WeaponFormulaTypes
  {
    None,
    Atk,
    Mag,
    AtkSpd,
    MagSpd,
    AtkDex,
    MagDex,
    AtkLuk,
    MagLuk,
    AtkMag,
    SpAtk,
    SpMag,
    AtkSpdDex,
    MagSpdDex,
    AtkDexLuk,
    MagDexLuk,
    Luk,
    Dex,
    Spd,
    Cri,
    Def,
    Mnd,
    AtkRndLuk,
    MagRndLuk,
    AtkEAt,
    MagEMg,
    AtkDefEDf,
    MagMndEMd,
    LukELk,
    MHp,
  }
}
