﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TeamNameInputWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class TeamNameInputWindow : MonoBehaviour
  {
    [SerializeField]
    private InputFieldCensorship inputField;

    public void SetInputName()
    {
      if (string.IsNullOrEmpty(this.inputField.text))
        return;
      string str = this.inputField.text;
      if (str.Length > this.inputField.characterLimit)
        str = str.Substring(0, this.inputField.characterLimit);
      GlobalVars.TeamName = str;
    }
  }
}
