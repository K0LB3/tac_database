﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_Mail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_Mail
  {
    public long mid;
    public string msg;
    public Json_Gift[] gifts;
    public int read;
    public long post_at;
    public int period;
    public int type;
  }
}
