﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayResumeBuff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class MultiPlayResumeBuff
  {
    public string iname;
    public int turn;
    public int unitindex;
    public int checkunit;
    public int timing;
    public bool passive;
    public int condition;
    public int type;
    public int calc;
    public int curse;
    public int skilltarget;
  }
}
