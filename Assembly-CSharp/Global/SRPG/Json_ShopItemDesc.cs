﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ShopItemDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_ShopItemDesc
  {
    public string iname;
    public int num;
    public int maxnum;
    public int boughtnum;

    public bool IsArtifact
    {
      get
      {
        return this.iname.StartsWith("AF_");
      }
    }
  }
}
