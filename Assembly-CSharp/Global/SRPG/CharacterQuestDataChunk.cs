﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CharacterQuestDataChunk
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace SRPG
{
  public class CharacterQuestDataChunk
  {
    public List<QuestParam> questParams = new List<QuestParam>();
    public string areaName;
    public string unitName;
    public UnitParam unitParam;

    public void SetUnitNameFromChapterID(string chapterID)
    {
      this.unitName = chapterID;
    }
  }
}
