﻿// Decompiled with JetBrains decompiler
// Type: SRPG.KeyNotFoundException`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class KeyNotFoundException<T> : Exception
  {
    public KeyNotFoundException(string key)
      : base(typeof (T).ToString() + " '" + key + "' doesn't exist.")
    {
    }
  }
}
