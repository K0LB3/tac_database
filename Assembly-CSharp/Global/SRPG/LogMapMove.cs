﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LogMapMove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class LogMapMove : BattleLog
  {
    public Unit self;
    public int ex;
    public int ey;
    public EUnitDirection dir;
    public bool auto;
  }
}
