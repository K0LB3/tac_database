﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DebugMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class DebugMenu : MonoBehaviour
  {
    public static void Start()
    {
    }

    public static void Log(string tag, string text, string trace)
    {
    }

    public static void Log(string tag, string text)
    {
    }

    public static void Log(string text)
    {
    }

    public static void LogSystem(string tag, string text, string trace)
    {
    }

    public static void LogSystem(string tag, string text)
    {
    }

    public static void LogSystem(string text)
    {
    }

    public static void LogWarning(string tag, string text, string trace)
    {
    }

    public static void LogWarning(string tag, string text)
    {
    }

    public static void LogWarning(string text)
    {
    }

    public static void LogError(string tag, string text, string trace)
    {
    }

    public static void LogError(string tag, string text)
    {
    }

    public static void LogError(string text)
    {
    }
  }
}
