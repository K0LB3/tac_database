﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_BtlComEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_BtlComEnd : Json_PlayerDataAll
  {
    public JSON_QuestProgress[] quests;
    public JSON_TrophyProgress[] trophyprogs;
    public Json_BtlQuestRanking quest_ranking;
  }
}
