﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_LimitedShopUpdateResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_LimitedShopUpdateResponse
  {
    public Json_Currencies currencies;
    public JSON_LimitedShopItemListSet[] shopitems;
    public int relcnt;
  }
}
