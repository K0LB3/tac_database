﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqBtlComReset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqBtlComReset : WebAPI
  {
    public ReqBtlComReset(string iname, Network.ResponseCallback response)
    {
      this.name = "btl/com/reset";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"iname\":\"");
      stringBuilder.Append(iname);
      stringBuilder.Append("\"");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
