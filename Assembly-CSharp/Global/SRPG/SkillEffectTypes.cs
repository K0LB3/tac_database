﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillEffectTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum SkillEffectTypes
  {
    None,
    Equipment,
    Attack,
    Defend,
    Heal,
    Buff,
    Debuff,
    Revive,
    Shield,
    ReflectDamage,
    DamageControl,
    FailCondition,
    CureCondition,
    DisableCondition,
    GemsGift,
    GemsIncDec,
    Guard,
    Teleport,
    Changing,
    RateHeal,
    RateDamage,
    PerfectAvoid,
    Throw,
    EffReplace,
    SetTrick,
    TransformUnit,
    SetBreakObj,
    ChangeWeather,
    RateDamageCurrent,
  }
}
