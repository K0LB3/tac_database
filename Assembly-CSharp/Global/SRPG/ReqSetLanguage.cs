﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqSetLanguage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqSetLanguage : WebAPI
  {
    public ReqSetLanguage(string language, Network.ResponseCallback response)
    {
      string isO639 = GameUtility.ConvertLanguageToISO639(language);
      this.name = "setlanguage";
      this.body = WebAPI.GetRequestString("\"lang\":\"" + isO639 + "\"");
      this.callback = response;
    }
  }
}
