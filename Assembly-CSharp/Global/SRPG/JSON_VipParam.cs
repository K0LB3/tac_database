﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VipParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_VipParam
  {
    public int exp;
    public int ticket;
    public int buy_coin_bonus;
    public int buy_coin_num;
    public int buy_stmn_num;
    public int reset_elite;
    public int reset_arena;
  }
}
