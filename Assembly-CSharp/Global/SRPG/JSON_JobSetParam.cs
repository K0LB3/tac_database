﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_JobSetParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_JobSetParam
  {
    public string iname;
    public string job;
    public int lrare;
    public int lplus;
    public string ljob1;
    public int llv1;
    public string ljob2;
    public int llv2;
    public string ljob3;
    public int llv3;
    public string cjob;
    public string target_unit;
  }
}
