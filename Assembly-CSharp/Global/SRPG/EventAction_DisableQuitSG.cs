﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EventAction_DisableQuitSG
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  [EventActionInfo("強制終了/禁止(3D)", "強制終了を禁止します", 5592405, 4473992)]
  public class EventAction_DisableQuitSG : EventAction
  {
    public override void OnActivate()
    {
      EventQuit eventQuit = EventQuit.Find();
      if ((UnityEngine.Object) null == (UnityEngine.Object) eventQuit)
      {
        this.ActivateNext();
      }
      else
      {
        eventQuit.gameObject.SetActive(false);
        this.ActivateNext();
      }
    }
  }
}
