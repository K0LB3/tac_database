﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestCampaignData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class QuestCampaignData
  {
    public QuestCampaignValueTypes type;
    public string unit;
    public int value;

    public float GetRate()
    {
      return (float) this.value / 100f;
    }
  }
}
