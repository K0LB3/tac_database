﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SortBadge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SortBadge : MonoBehaviour
  {
    [FourCC]
    public int ID;
    public Image Icon;
    public Text Value;

    public void SetValue(string value)
    {
      if (!((UnityEngine.Object) this.Value != (UnityEngine.Object) null))
        return;
      this.Value.text = value;
    }

    public void SetValue(int value)
    {
      if (!((UnityEngine.Object) this.Value != (UnityEngine.Object) null))
        return;
      this.Value.text = value.ToString();
    }
  }
}
