﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_VideoAd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace SRPG
{
  [FlowNode.Pin(4, "NotVideoAd", FlowNode.PinTypes.Output, 4)]
  [FlowNode.Pin(5, "AdSkipped", FlowNode.PinTypes.Output, 5)]
  [FlowNode.Pin(3, "NotAvailable", FlowNode.PinTypes.Output, 3)]
  [FlowNode.Pin(2, "AdFailed", FlowNode.PinTypes.Output, 2)]
  [FlowNode.NodeType("System/VideoAd", 32741)]
  [FlowNode.Pin(0, "Start", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(1, "AdRewarded", FlowNode.PinTypes.Output, 1)]
  public class FlowNode_VideoAd : FlowNode
  {
    private bool isTryingToShowAd;

    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      if (GlobalVars.SelectedTrophy == null || !GlobalVars.SelectedTrophy.ToString().Contains("DAILY_GLAPVIDEO"))
      {
        this.ActivateOutputLinks(4);
      }
      else
      {
        if (this.isTryingToShowAd)
          return;
        ShowOptions videoOptions = new ShowOptions();
        videoOptions.set_resultCallback(new Action<ShowResult>(this.HandleShowAdResult));
        if (!this.TryToShowVideoAd(videoOptions, string.Empty))
        {
          this.isTryingToShowAd = false;
          this.ActivateOutputLinks(3);
        }
        else
          this.isTryingToShowAd = true;
      }
    }

    private bool TryToShowVideoAd(ShowOptions videoOptions, string placementName = "")
    {
      if (Advertisement.isSupported && Advertisement.isInitialized && Advertisement.IsReady())
      {
        Advertisement.Show(placementName, videoOptions);
        return true;
      }
      Debug.Log((object) ("Advert is Supported: " + (object) Advertisement.isSupported + "\nAdvert was Initialized: " + (object) Advertisement.isInitialized + "\nAdvert was Ready: " + (object) Advertisement.IsReady()));
      return false;
    }

    private void HandleShowAdResult(ShowResult result)
    {
      this.isTryingToShowAd = false;
      switch (result)
      {
        case ShowResult.Failed:
          DebugUtility.LogError("Video Ad failed.");
          this.ActivateOutputLinks(2);
          break;
        case ShowResult.Skipped:
          DebugUtility.Log("Video Ad skipped.");
          this.ActivateOutputLinks(5);
          break;
        case ShowResult.Finished:
          DebugUtility.Log("Video Ad rewarded.");
          this.ActivateOutputLinks(1);
          break;
      }
    }
  }
}
