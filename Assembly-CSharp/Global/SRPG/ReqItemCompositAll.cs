﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqItemCompositAll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqItemCompositAll : WebAPI
  {
    public ReqItemCompositAll(string iname, bool is_cmn, Network.ResponseCallback response)
    {
      this.name = "item/gouseiall";
      int num = !is_cmn ? 0 : 1;
      this.body = WebAPI.GetRequestString("\"iname\":\"" + iname + "\",\"is_cmn\":" + (object) num);
      this.callback = response;
    }
  }
}
