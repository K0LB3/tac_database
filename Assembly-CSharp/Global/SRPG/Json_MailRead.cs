﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_MailRead
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_MailRead
  {
    public Json_PlayerData player;
    public Json_Unit[] units;
    public Json_Item[] items;
    public Json_Friend[] friends;
    public Json_Artifact[] artifacts;
    public Json_Mail[] processed;
  }
}
