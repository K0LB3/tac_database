﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_GachaExcite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_GachaExcite
  {
    public int pk;
    public Json_GachaExcite.Fields fields;

    public class Fields
    {
      public int id;
      public int rarity;
      public int weight;
      public string color1;
      public string color2;
      public string color3;
      public string color4;
      public string color5;
    }
  }
}
