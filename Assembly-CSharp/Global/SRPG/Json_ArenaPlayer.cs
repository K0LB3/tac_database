﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ArenaPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_ArenaPlayer
  {
    public string result;
    public string fuid;
    public string name;
    public int lv;
    public int rank;
    public Json_Unit[] units;
    public long btl_at;
    public string award;
  }
}
