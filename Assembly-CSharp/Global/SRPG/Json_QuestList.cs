﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_QuestList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_QuestList
  {
    public JSON_SectionParam[] worlds;
    public JSON_ChapterParam[] areas;
    public JSON_QuestParam[] quests;
    public JSON_ObjectiveParam[] objectives;
    public JSON_MagnificationParam[] magnifications;
    public JSON_QuestCondParam[] conditions;
    public JSON_QuestCampaignParentParam[] CampaignParents;
    public JSON_QuestCampaignChildParam[] CampaignChildren;
    public JSON_TowerFloorParam[] towerFloors;
    public JSON_TowerRewardParam[] towerRewards;
    public JSON_TowerRoundRewardParam[] towerRoundRewards;
    public JSON_TowerParam[] towers;
    public JSON_VersusTowerParam[] versusTowerFloor;
    public JSON_VersusSchedule[] versusschedule;
    public JSON_VersusCoin[] versuscoin;
    public JSON_MultiTowerFloorParam[] multitowerFloor;
    public JSON_MultiTowerRewardParam[] multitowerRewards;
    public JSON_MapEffectParam[] MapEffect;
    public JSON_WeatherSetParam[] WeatherSet;
    public JSON_RankingQuestParam[] rankingQuests;
    public JSON_RankingQuestScheduleParam[] rankingQuestSchedule;
    public JSON_RankingQuestRewardParam[] rankingQuestRewards;
  }
}
