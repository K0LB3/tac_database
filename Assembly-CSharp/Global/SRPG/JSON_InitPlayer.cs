﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_InitPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_InitPlayer
  {
    public int gold;
    public int coin;
    public int ap;
    public int exp;
  }
}
