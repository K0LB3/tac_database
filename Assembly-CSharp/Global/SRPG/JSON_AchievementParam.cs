﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AchievementParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_AchievementParam
  {
    public int pk;
    public JSON_AchievementParam.Fields fields;

    public class Fields
    {
      public int id;
      public string iname;
      public string ios;
      public string googleplay;
    }
  }
}
