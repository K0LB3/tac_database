﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_Friend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_Friend
  {
    public string uid;
    public string fuid;
    public string name;
    public string type;
    public int lv;
    public long lastlogin;
    public int is_multi_push;
    public string multi_comment;
    public Json_Unit unit;
    public string created_at;
    public int is_favorite;
    public string award;
    public string wish;
    public string status;
  }
}
