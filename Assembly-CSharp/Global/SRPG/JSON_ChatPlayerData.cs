﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ChatPlayerData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_ChatPlayerData
  {
    public string name;
    public int exp;
    public long lastlogin;
    public byte is_friend;
    public byte is_favorite;
    public string fuid;
    public Json_Unit unit;
    public string award;
  }
}
