﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BundleParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_BundleParam
  {
    public string product_id;
    public string platform;
    public string name;
    public string description;
    public int additional_paid_coin;
    public int additional_free_coin;
    public long start_date;
    public long end_date;
    public JSON_BundleContents contents;
    public int enabled;
    public int display_order;
    public int max_purchase_limit;
    public int purchase_count;
    public int available_days_for_new_player;
    public string image;
  }
}
