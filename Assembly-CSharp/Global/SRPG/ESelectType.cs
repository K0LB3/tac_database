﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ESelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum ESelectType
  {
    Cross,
    Diamond,
    Square,
    Laser,
    All,
    Wall,
    WallPlus,
    Bishop,
    Victory,
    LaserSpread,
    LaserWide,
    Horse,
    LaserTwin,
    LaserTriple,
    SquareOutline,
  }
}
