﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ArenaPlayerHistory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_ArenaPlayerHistory
  {
    public string type;
    public string result;
    public JSON_ArenaRankInfo ranking;
    public long at;
    public Json_ArenaPlayer my;
    public Json_ArenaPlayer enemy;
  }
}
