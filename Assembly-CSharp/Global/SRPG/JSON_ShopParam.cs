﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ShopParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ShopParam
  {
    public string iname;
    public int upd_type;
    public int[] upd_costs;
  }
}
