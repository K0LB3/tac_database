﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_TowerBtlResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_TowerBtlResult : Json_PlayerDataAll
  {
    public JSON_ReqTowerResuponse.Json_TowerPlayerUnit[] pdeck;
    public Json_Artifact[] artis;
    public int arrived_num;
    public int clear;
    public Json_TowerBtlEndRank ranking;
  }
}
