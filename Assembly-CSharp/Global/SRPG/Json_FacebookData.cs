﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_FacebookData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_FacebookData
  {
    public string permissions;
    public long expiration_timestamp;
    public string access_token;
    public string user_id;
    public long last_refresh;
    public string[] granted_permissions;
    public string[] declined_permissions;
    public int callback_id;
  }
}
