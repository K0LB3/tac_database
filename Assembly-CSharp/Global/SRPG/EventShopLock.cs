﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EventShopLock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class EventShopLock : MonoBehaviour
  {
    [SerializeField]
    private GameObject LockObject;

    private void Start()
    {
      if ((UnityEngine.Object) this.LockObject != (UnityEngine.Object) null)
        this.LockObject.SetActive(!(bool) GlobalVars.IsEventShopOpen);
      Button component = this.gameObject.GetComponent<Button>();
      if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
        return;
      component.interactable = (bool) GlobalVars.IsEventShopOpen;
    }
  }
}
