﻿// Decompiled with JetBrains decompiler
// Type: SRPG.NetworkErrorWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using GR;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class NetworkErrorWindow : MonoBehaviour
  {
    public Text Title;
    public Text StatusCode;
    public Text Message;

    private void Awake()
    {
    }

    private void Start()
    {
      if ((UnityEngine.Object) this.Title != (UnityEngine.Object) null)
        this.Title.text = LocalizedText.Get("embed.CONN_ERR");
      if ((UnityEngine.Object) this.StatusCode != (UnityEngine.Object) null)
      {
        if (GameUtility.IsDebugBuild)
        {
          this.StatusCode.text = LocalizedText.Get("errorcode." + ((int) Network.ErrCode).ToString() + "_TITLE");
          this.StatusCode.gameObject.SetActive(true);
        }
        else
          this.StatusCode.gameObject.SetActive(false);
      }
      if ((UnityEngine.Object) this.Message != (UnityEngine.Object) null)
      {
        if (string.IsNullOrEmpty(Network.ErrMsg))
          this.Message.text = LocalizedText.Get("embed.APP_REBOOT", new object[1]
          {
            (object) Network.ErrCode.ToString()
          });
        else
          this.Message.text = Network.ErrMsg;
      }
      Transform child = this.transform.FindChild("window/Button");
      if (!((UnityEngine.Object) child != (UnityEngine.Object) null))
        return;
      Button component = child.GetComponent<Button>();
      if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
        return;
      component.onClick.AddListener(new UnityAction(this.OnClick));
    }

    private void OnClick()
    {
      if (Network.ErrCode != Network.EErrCode.Authorize)
        return;
      MonoSingleton<GameManager>.Instance.ResetAuth();
    }

    public void OpenMaintenanceSite()
    {
      Application.OpenURL(Network.OfficialUrl);
    }

    public void OpenVersionUpSite()
    {
      Application.OpenURL(Network.OfficialUrl);
    }

    public void OpenStore()
    {
      Application.OpenURL("market://details?id=sg.gumi.alchemistww");
    }
  }
}
