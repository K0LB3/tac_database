﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BeginnerQuestIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(0, "更新", FlowNode.PinTypes.Input, 0)]
  public class BeginnerQuestIcon : MonoBehaviour, IFlowInterface
  {
    private const int PLAYER_MAX_LEVEL = 20;

    public void Activated(int pinID)
    {
      if (pinID != 0)
        return;
      this.Refresh();
    }

    private void Refresh()
    {
      bool flag = true;
      if (MonoSingleton<GameManager>.Instance.Player.Lv < 20)
      {
        foreach (QuestParam quest in MonoSingleton<GameManager>.Instance.Quests)
        {
          if (quest.iname.Contains("QE_EV_BEGINNER") && quest.state != QuestStates.Cleared)
          {
            flag = false;
            break;
          }
        }
      }
      this.gameObject.SetActive(!flag);
    }
  }
}
