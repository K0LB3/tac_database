﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_TowerBtlEndRank
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_TowerBtlEndRank
  {
    public int turn_num;
    public int died_num;
    public int retire_num;
    public int recovery_num;
    public int spd_rank;
    public int tec_rank;
    public int spd_score;
    public int tec_score;
    public int ret_score;
    public int rcv_score;
  }
}
