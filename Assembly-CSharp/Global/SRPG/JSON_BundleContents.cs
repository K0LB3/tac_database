﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BundleContents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_BundleContents
  {
    public JSON_BundleItemInfo[] items;
    public JSON_BundleItemInfo[] units;
    public JSON_BundleItemInfo[] artifacts;
  }
}
