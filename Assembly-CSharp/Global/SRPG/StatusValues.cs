﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StatusValues
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  internal enum StatusValues
  {
    Mp,
    MpIni,
    Atk,
    Def,
    Mag,
    Mnd,
    Rec,
    Dex,
    Spd,
    Cri,
    Luk,
    Mov,
    Jmp,
  }
}
