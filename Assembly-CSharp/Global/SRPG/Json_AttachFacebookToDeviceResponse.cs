﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_AttachFacebookToDeviceResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_AttachFacebookToDeviceResponse
  {
    public int linked;
    public string transaction_id;
    public string device_id;
    public string secret_key;
    public string related_id;
    public int lv;
    public string name;
  }
}
