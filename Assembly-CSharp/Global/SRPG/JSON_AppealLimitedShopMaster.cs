﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AppealLimitedShopMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_AppealLimitedShopMaster
  {
    public int pk;
    public JSON_AppealLimitedShopMaster.Fields fields;

    public class Fields
    {
      public string appeal_id;
      public string start_at;
      public string end_at;
      public int priority;
      public float position_chara;
      public float position_text;
    }
  }
}
