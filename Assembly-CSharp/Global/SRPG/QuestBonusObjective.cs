﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestBonusObjective
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class QuestBonusObjective
  {
    public string item;
    public int itemNum;
    public RewardType itemType;
    public EMissionType Type;
    public string TypeParam;

    public bool IsMissionTypeExecSkill()
    {
      return this.Type == EMissionType.UseTargetSkill || this.Type == EMissionType.KillstreakByUsingTargetSkill || this.Type == EMissionType.KillstreakByUsingTargetItem;
    }
  }
}
