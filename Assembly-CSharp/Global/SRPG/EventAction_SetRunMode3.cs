﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EventAction_SetRunMode3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  [EventActionInfo("New/アクター/走りアニメーション変更2", "ユニットの走りアニメーションを変更します。", 5592405, 4473992)]
  public class EventAction_SetRunMode3 : EventAction
  {
    private const string MOVIE_PATH = "Movies/";
    private const string DEMO_PATH = "Demo/";
    public EventAction_SetRunMode3.PREFIX_PATH Path;
    [StringIsActorList]
    public string ActorID;
    public string AnimationName;

    public override bool IsPreloadAssets
    {
      get
      {
        return true;
      }
    }

    [DebuggerHidden]
    public override IEnumerator PreloadAssets()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new EventAction_SetRunMode3.\u003CPreloadAssets\u003Ec__IteratorA4()
      {
        \u003C\u003Ef__this = this
      };
    }

    public override void OnActivate()
    {
      GameObject actor = EventAction.FindActor(this.ActorID);
      if ((UnityEngine.Object) actor != (UnityEngine.Object) null)
      {
        TacticsUnitController component = actor.GetComponent<TacticsUnitController>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          component.SetRunAnimation(this.AnimationName);
      }
      this.ActivateNext();
    }

    public enum PREFIX_PATH
    {
      Demo,
      Movie,
    }
  }
}
