﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AIPatrolPoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class AIPatrolPoint
  {
    public int x;
    public int y;
    public int length;

    public void CopyTo(AIPatrolPoint dst)
    {
      dst.x = this.x;
      dst.y = this.y;
      dst.length = this.length;
    }
  }
}
