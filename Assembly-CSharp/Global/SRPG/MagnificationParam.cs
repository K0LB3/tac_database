﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MagnificationParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class MagnificationParam
  {
    public string iname;
    public int[] atkMagnifications;

    public void Deserialize(JSON_MagnificationParam json)
    {
      if (json == null)
        throw new InvalidJSONException();
      this.iname = json.iname;
      this.atkMagnifications = json.atk;
    }
  }
}
