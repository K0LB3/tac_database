﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Quad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace SRPG
{
  [Serializable]
  public struct Quad
  {
    public Vector2 v0;
    public Color32 c0;
    public Vector2 v1;
    public Color32 c1;
    public Vector2 v2;
    public Color32 c2;
    public Vector2 v3;
    public Color32 c3;
  }
}
