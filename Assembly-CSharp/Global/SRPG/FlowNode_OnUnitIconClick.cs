﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_OnUnitIconClick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  [FlowNode.NodeType("Event/OnUnitIconClick", 58751)]
  [FlowNode.Pin(1, "Clicked", FlowNode.PinTypes.Output, 0)]
  public class FlowNode_OnUnitIconClick : FlowNode
  {
    public override void OnActivate(int pinID)
    {
    }

    public void Click()
    {
      this.ActivateOutputLinks(1);
    }
  }
}
