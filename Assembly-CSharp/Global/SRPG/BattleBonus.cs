﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BattleBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public enum BattleBonus
  {
    EffectRange,
    EffectScope,
    EffectHeight,
    HitRate,
    AvoidRate,
    CriticalRate,
    SlashAttack,
    PierceAttack,
    BlowAttack,
    ShotAttack,
    MagicAttack,
    ReactionAttack,
    JumpAttack,
    GainJewel,
    UsedJewelRate,
    ActionCount,
    GutsRate,
    AutoJewel,
    ChargeTimeRate,
    CastTimeRate,
    BuffTurn,
    DebuffTurn,
    CombinationRange,
    HpCostRate,
    SkillUseCount,
    PoisonDamage,
    PoisonTurn,
    Resist_Slash,
    Resist_Pierce,
    Resist_Blow,
    Resist_Shot,
    Resist_Magic,
    Resist_Reaction,
    Resist_Jump,
    Avoid_Slash,
    Avoid_Pierce,
    Avoid_Blow,
    Avoid_Shot,
    Avoid_Magic,
    Avoid_Reaction,
    Avoid_Jump,
    GainJewelRate,
    UsedJewel,
  }
}
