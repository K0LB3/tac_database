﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GachaBGTextures
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class GachaBGTextures : ScriptableObject
  {
    public Texture2D[] Textures;
  }
}
