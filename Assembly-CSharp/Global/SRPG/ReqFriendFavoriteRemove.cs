﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqFriendFavoriteRemove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqFriendFavoriteRemove : WebAPI
  {
    public ReqFriendFavoriteRemove(string fuid, Network.ResponseCallback response)
    {
      this.name = "friend/favorite/remove";
      this.body = WebAPI.GetRequestString("\"fuid\":\"" + fuid + "\"");
      this.callback = response;
    }
  }
}
