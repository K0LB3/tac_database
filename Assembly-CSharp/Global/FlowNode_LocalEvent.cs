﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_LocalEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("Event/LocalEvent", 58751)]
[FlowNode.Pin(1, "Triggered", FlowNode.PinTypes.Output, 0)]
public class FlowNode_LocalEvent : FlowNode
{
  [FlowNode.ShowInInfo]
  public string EventName;

  public override void OnActivate(int pinID)
  {
    this.ActivateOutputLinks(1);
  }
}
