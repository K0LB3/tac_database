﻿// Decompiled with JetBrains decompiler
// Type: MapPickup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MapPickup : MonoBehaviour
{
  public MapPickup.PickupEvent OnPickup;
  public Transform Shadow;
  public Vector3 DropEffectOffset;

  public delegate void PickupEvent();
}
