﻿// Decompiled with JetBrains decompiler
// Type: SwitchByPlatform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SwitchByPlatform : MonoBehaviour
{
  [SerializeField]
  public RuntimePlatform[] hides = new RuntimePlatform[0];
  public bool CheckAmazonFlag;

  private void Start()
  {
    foreach (RuntimePlatform hide in this.hides)
    {
      if (Application.platform == hide)
        this.gameObject.SetActive(false);
    }
    if (this.CheckAmazonFlag)
      ;
  }
}
