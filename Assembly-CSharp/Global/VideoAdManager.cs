﻿// Decompiled with JetBrains decompiler
// Type: VideoAdManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.Advertisements;

public class VideoAdManager : MonoBehaviour
{
  public static void Init()
  {
    VideoAdManager.InitialiseAds();
  }

  private static void InitialiseAds()
  {
    string gameId = "1476714";
    bool testMode = false;
    if (Advertisement.isInitialized)
      return;
    Advertisement.Initialize(gameId, testMode);
  }
}
