﻿// Decompiled with JetBrains decompiler
// Type: SupportLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SupportLogger : MonoBehaviour
{
  public bool LogTrafficStats = true;

  public void Start()
  {
    if (!((Object) GameObject.Find("PunSupportLogger") == (Object) null))
      return;
    GameObject gameObject = new GameObject("PunSupportLogger");
    Object.DontDestroyOnLoad((Object) gameObject);
    gameObject.AddComponent<SupportLogging>().LogTrafficStats = this.LogTrafficStats;
  }
}
