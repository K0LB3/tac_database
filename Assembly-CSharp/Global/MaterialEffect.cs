﻿// Decompiled with JetBrains decompiler
// Type: MaterialEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class MaterialEffect : MonoBehaviour
{
  public Material Material;

  private void OnRenderImage(RenderTexture src, RenderTexture dest)
  {
    if ((Object) this.Material != (Object) null)
      Graphics.Blit((Texture) src, dest, this.Material);
    else
      Graphics.Blit((Texture) src, dest);
  }
}
