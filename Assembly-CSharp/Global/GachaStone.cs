﻿// Decompiled with JetBrains decompiler
// Type: GachaStone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GachaStone : MonoBehaviour
{
  public Camera TargetCamera;

  public string DROP_ID { get; set; }

  private void Start()
  {
    if (!((Object) this.TargetCamera == (Object) null))
      return;
    this.TargetCamera = Camera.main;
  }

  private void Update()
  {
    this.transform.LookAt(this.TargetCamera.transform);
  }
}
