﻿// Decompiled with JetBrains decompiler
// Type: Fabric.Internal.Runtime.Utils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Fabric.Internal.Runtime
{
  public static class Utils
  {
    public static void Log(string kit, string message)
    {
      new AndroidJavaClass("android.util.Log").CallStatic<int>("d", (object) kit, (object) message);
    }
  }
}
