﻿// Decompiled with JetBrains decompiler
// Type: Fabric.Runtime.Internal.AndroidImpl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Fabric.Runtime.Internal
{
  internal class AndroidImpl : Impl
  {
    private static readonly AndroidJavaClass FabricInitializer = new AndroidJavaClass("io.fabric.unity.android.FabricInitializer");

    public override string Initialize()
    {
      return AndroidImpl.FabricInitializer.CallStatic<string>("JNI_InitializeFabric");
    }
  }
}
