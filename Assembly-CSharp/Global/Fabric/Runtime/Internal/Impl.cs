﻿// Decompiled with JetBrains decompiler
// Type: Fabric.Runtime.Internal.Impl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using Fabric.Internal.Runtime;

namespace Fabric.Runtime.Internal
{
  internal class Impl
  {
    protected const string Name = "Fabric";

    public static Impl Make()
    {
      return (Impl) new AndroidImpl();
    }

    public virtual string Initialize()
    {
      Utils.Log("Fabric", "Method Initialize () is unimplemented on this platform");
      return string.Empty;
    }
  }
}
