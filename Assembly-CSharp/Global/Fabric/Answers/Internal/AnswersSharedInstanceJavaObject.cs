﻿// Decompiled with JetBrains decompiler
// Type: Fabric.Answers.Internal.AnswersSharedInstanceJavaObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Fabric.Answers.Internal
{
  internal class AnswersSharedInstanceJavaObject
  {
    private AndroidJavaObject javaObject;

    public AnswersSharedInstanceJavaObject()
    {
      this.javaObject = new AndroidJavaClass("com.crashlytics.android.answers.Answers").CallStatic<AndroidJavaObject>("getInstance");
    }

    public void Log(string methodName, AnswersEventInstanceJavaObject eventInstance)
    {
      this.javaObject.Call(methodName, new object[1]
      {
        (object) eventInstance.javaObject
      });
    }
  }
}
