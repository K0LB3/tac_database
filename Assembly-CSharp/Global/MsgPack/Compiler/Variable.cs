﻿// Decompiled with JetBrains decompiler
// Type: MsgPack.Compiler.Variable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System.Reflection.Emit;

namespace MsgPack.Compiler
{
  public class Variable
  {
    private Variable(VariableType type, int index)
    {
      this.VarType = type;
      this.Index = index;
    }

    public static Variable CreateLocal(LocalBuilder local)
    {
      return new Variable(VariableType.Local, local.LocalIndex);
    }

    public static Variable CreateArg(int idx)
    {
      return new Variable(VariableType.Arg, idx);
    }

    public VariableType VarType { get; set; }

    public int Index { get; set; }
  }
}
