﻿// Decompiled with JetBrains decompiler
// Type: OpJoinRandomRoomParams
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using ExitGames.Client.Photon;

internal class OpJoinRandomRoomParams
{
  public Hashtable ExpectedCustomRoomProperties;
  public byte ExpectedMaxPlayers;
  public MatchmakingMode MatchingType;
  public TypedLobby TypedLobby;
  public string SqlLobbyFilter;
  public string[] ExpectedUsers;
}
