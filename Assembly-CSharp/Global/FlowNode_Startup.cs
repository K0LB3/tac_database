﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_Startup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

[FlowNode.Pin(1, "Output", FlowNode.PinTypes.Output, 0)]
[FlowNode.NodeType("Event/Startup", 58751)]
public class FlowNode_Startup : FlowNodePersistent
{
  private void Start()
  {
    this.ActivateOutputLinks(1);
    this.enabled = false;
  }

  public override void OnActivate(int pinID)
  {
  }
}
