﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_DestroyObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("Destroy", 32741)]
[FlowNode.Pin(10, "Destroy", FlowNode.PinTypes.Input, 0)]
public class FlowNode_DestroyObject : FlowNode
{
  [FlowNode.DropTarget(typeof (GameObject), false)]
  [FlowNode.ShowInInfo]
  public GameObject Target;

  public override void OnActivate(int pinID)
  {
    if (!((Object) this.Target != (Object) null))
      return;
    Object.Destroy((Object) this.Target.gameObject);
  }
}
