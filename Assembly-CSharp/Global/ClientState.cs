﻿// Decompiled with JetBrains decompiler
// Type: ClientState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

public enum ClientState
{
  Uninitialized,
  PeerCreated,
  Queued,
  Authenticated,
  JoinedLobby,
  DisconnectingFromMasterserver,
  ConnectingToGameserver,
  ConnectedToGameserver,
  Joining,
  Joined,
  Leaving,
  DisconnectingFromGameserver,
  ConnectingToMasterserver,
  QueuedComingFromGameserver,
  Disconnecting,
  Disconnected,
  ConnectedToMaster,
  ConnectingToNameServer,
  ConnectedToNameServer,
  DisconnectingFromNameServer,
  Authenticating,
}
