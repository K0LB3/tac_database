﻿// Decompiled with JetBrains decompiler
// Type: AlchemistGooglePlay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

public static class AlchemistGooglePlay
{
  public const string achievement_blue_flame_guard = "CgkIgsmSkdUXEAIQBw";
  public const string achievement_battler = "CgkIgsmSkdUXEAIQAg";
  public const string achievement_determination = "CgkIgsmSkdUXEAIQCQ";
  public const string achievement_raking_it_in = "CgkIgsmSkdUXEAIQCA";
  public const string achievement_elite_evolver = "CgkIgsmSkdUXEAIQBg";
  public const string achievement_the_adventure_begins = "CgkIgsmSkdUXEAIQAQ";
  public const string achievement_mercenary = "CgkIgsmSkdUXEAIQAw";
  public const string achievement_veteran = "CgkIgsmSkdUXEAIQBA";
  public const string achievement_seasoned_warrior = "CgkIgsmSkdUXEAIQCg";
  public const string achievement_battle_hardened = "CgkIgsmSkdUXEAIQBQ";
}
