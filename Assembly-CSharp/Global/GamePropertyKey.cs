﻿// Decompiled with JetBrains decompiler
// Type: GamePropertyKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

public class GamePropertyKey
{
  public const byte MaxPlayers = 255;
  public const byte IsVisible = 254;
  public const byte IsOpen = 253;
  public const byte PlayerCount = 252;
  public const byte Removed = 251;
  public const byte PropsListedInLobby = 250;
  public const byte CleanupCacheOnLeave = 249;
  public const byte MasterClientId = 248;
  public const byte ExpectedUsers = 247;
}
