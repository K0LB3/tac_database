﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.Data.EntityNotificationType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace Gsc.Network.Data
{
  public enum EntityNotificationType
  {
    Update,
    Remove,
  }
}
