﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.IWebTask`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using Gsc.Tasks;
using System.Collections;

namespace Gsc.Network
{
  public interface IWebTask<TResponse> : IEnumerator, IWebTaskBase, IWebTask, ITask where TResponse : IResponse<TResponse>
  {
    TResponse Response { get; }

    IErrorResponse ErrorResponse { get; }
  }
}
