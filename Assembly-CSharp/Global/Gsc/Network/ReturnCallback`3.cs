﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.ReturnCallback`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace Gsc.Network
{
  public delegate TResult ReturnCallback<TRequest, TResponse, TResult>(TRequest request, TResponse response);
}
