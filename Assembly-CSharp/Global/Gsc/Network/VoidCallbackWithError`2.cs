﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.VoidCallbackWithError`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace Gsc.Network
{
  public delegate void VoidCallbackWithError<TRequest, TResponse>(TRequest request, TResponse response, IErrorResponse error);
}
