﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.IErrorResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using Gsc.DOM;

namespace Gsc.Network
{
  public interface IErrorResponse : IResponse
  {
    IDocument data { get; }

    string ErrorCode { get; }
  }
}
