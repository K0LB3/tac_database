﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Mutable.Value
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using Gsc.Network;
using System.Collections;

namespace Gsc.DOM.Json.Mutable
{
  public struct Value
  {
    public void SetNull()
    {
    }

    public void SetObject()
    {
    }

    public void SetArray()
    {
    }

    public void Set(IRequestObject value)
    {
    }

    public void Set(IEnumerable value)
    {
    }

    public void Set(bool value)
    {
    }

    public void Set(string value)
    {
    }

    public void Set(int value)
    {
    }

    public void Set(uint value)
    {
    }

    public void Set(long value)
    {
    }

    public void Set(ulong value)
    {
    }

    public void Set(float value)
    {
    }

    public void Set(double value)
    {
    }
  }
}
