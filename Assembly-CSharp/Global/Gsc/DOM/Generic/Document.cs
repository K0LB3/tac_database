﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Generic.Document
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace Gsc.DOM.Generic
{
  public class Document : IDisposable, IDocument
  {
    private readonly Value root;

    public Document(Document document, ref Value root)
    {
      this.root = root;
    }

    IValue IDocument.Root
    {
      get
      {
        return (IValue) this.root;
      }
    }

    public Value Root
    {
      get
      {
        return this.root;
      }
    }

    ~Document()
    {
      this.Dispose();
    }

    public void Dispose()
    {
    }
  }
}
