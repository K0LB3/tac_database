﻿// Decompiled with JetBrains decompiler
// Type: NullGraphic2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class NullGraphic2 : Graphic
{
  protected override void Start()
  {
    base.Start();
    this.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
  }

  protected override void OnPopulateMesh(VertexHelper vh)
  {
    base.OnPopulateMesh(vh);
  }
}
