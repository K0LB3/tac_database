﻿// Decompiled with JetBrains decompiler
// Type: LobbyType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

public enum LobbyType : byte
{
  Default = 0,
  SqlLobby = 2,
  AsyncRandomLobby = 3,
}
