﻿// Decompiled with JetBrains decompiler
// Type: FixLineSpacing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Text))]
public class FixLineSpacing : MonoBehaviour
{
  public bool NoBestFit;

  private void OnEnable()
  {
  }

  private void Awake()
  {
    if (!this.enabled)
      return;
    Text component = this.GetComponent<Text>();
    component.lineSpacing *= 2f;
    if (this.NoBestFit)
      component.resizeTextForBestFit = false;
    this.enabled = false;
  }
}
