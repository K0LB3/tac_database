﻿// Decompiled with JetBrains decompiler
// Type: NetworkError
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using GR;
using UnityEngine;

public class NetworkError : MonoSingleton<NetworkError>
{
  protected override void Initialize()
  {
    Object.DontDestroyOnLoad((Object) this);
    Object.DontDestroyOnLoad(Object.Instantiate(Resources.Load("NetworkErrorHandler"), Vector3.zero, Quaternion.identity));
    Debug.Log((object) "NetworkError Initialized");
  }

  protected override void Release()
  {
  }
}
