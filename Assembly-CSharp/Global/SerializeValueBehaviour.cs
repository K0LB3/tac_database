﻿// Decompiled with JetBrains decompiler
// Type: SerializeValueBehaviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SerializeValueBehaviour : MonoBehaviour
{
  [SerializeField]
  private SerializeValueList m_List = new SerializeValueList();

  public SerializeValueList list
  {
    get
    {
      return this.m_List;
    }
  }

  private void Awake()
  {
    this.m_List.Initialize();
  }
}
