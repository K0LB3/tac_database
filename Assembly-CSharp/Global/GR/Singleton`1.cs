﻿// Decompiled with JetBrains decompiler
// Type: GR.Singleton`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

namespace GR
{
  public abstract class Singleton<T> where T : class, new()
  {
    private static T instance_;

    public static T Instance
    {
      get
      {
        if ((object) Singleton<T>.instance_ == null)
          Singleton<T>.instance_ = Activator.CreateInstance<T>();
        return Singleton<T>.instance_;
      }
    }
  }
}
