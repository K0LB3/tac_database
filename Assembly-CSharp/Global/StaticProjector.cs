﻿// Decompiled with JetBrains decompiler
// Type: StaticProjector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (MeshFilter))]
[ExecuteInEditMode]
[AddComponentMenu("Rendering/Static Projector")]
[RequireComponent(typeof (MeshRenderer))]
public class StaticProjector : MonoBehaviour
{
  public float FOVAngle = 45f;
  public float FarPlane = 3f;
  [SerializeField]
  [HideInInspector]
  private Mesh mMesh;

  private void Awake()
  {
    if (!Application.isPlaying)
      return;
    this.enabled = false;
  }
}
