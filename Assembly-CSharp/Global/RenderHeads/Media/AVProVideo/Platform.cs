﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Platform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum Platform
  {
    Windows = 0,
    MacOSX = 1,
    iOS = 2,
    tvOS = 3,
    Android = 4,
    WindowsPhone = 5,
    WindowsUWP = 6,
    WebGL = 7,
    Count = 8,
    Unknown = 100, // 0x00000064
  }
}
