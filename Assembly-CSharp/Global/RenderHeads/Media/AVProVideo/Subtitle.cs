﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Subtitle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public class Subtitle
  {
    public int index;
    public string text;
    public int timeStartMs;
    public int timeEndMs;

    public bool IsBefore(float time)
    {
      if ((double) time > (double) this.timeStartMs)
        return (double) time > (double) this.timeEndMs;
      return false;
    }

    public bool IsTime(float time)
    {
      if ((double) time >= (double) this.timeStartMs)
        return (double) time < (double) this.timeEndMs;
      return false;
    }
  }
}
