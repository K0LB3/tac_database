﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.ErrorCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum ErrorCode
  {
    None = 0,
    LoadFailed = 100, // 0x00000064
    DecodeFailed = 200, // 0x000000C8
  }
}
