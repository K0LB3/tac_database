﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ToggleChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[FlowNode.Pin(1, "enable", FlowNode.PinTypes.Input, 0)]
[AddComponentMenu("")]
[FlowNode.NodeType("Event/ToggleChange", 58751)]
[FlowNode.Pin(2, "disable", FlowNode.PinTypes.Input, 0)]
public class FlowNode_ToggleChange : FlowNode
{
  [FlowNode.DropTarget(typeof (Toggle), true)]
  [FlowNode.ShowInInfo]
  public Toggle Target;

  public override void OnActivate(int pinID)
  {
    if (!((Object) this.Target != (Object) null))
      return;
    if (pinID == 1)
    {
      this.Target.isOn = true;
    }
    else
    {
      if (pinID != 2)
        return;
      this.Target.isOn = false;
    }
  }
}
