﻿// Decompiled with JetBrains decompiler
// Type: PhotonTransformViewScaleModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class PhotonTransformViewScaleModel
{
  public float InterpolateMoveTowardsSpeed = 1f;
  public bool SynchronizeEnabled;
  public PhotonTransformViewScaleModel.InterpolateOptions InterpolateOption;
  public float InterpolateLerpSpeed;

  public enum InterpolateOptions
  {
    Disabled,
    MoveTowards,
    Lerp,
  }
}
