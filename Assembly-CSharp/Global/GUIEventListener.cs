﻿// Decompiled with JetBrains decompiler
// Type: GUIEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIEventListener : MonoBehaviour
{
  public GUIEventListener.GUIEvent Listeners;

  private void OnGUI()
  {
    if (this.Listeners == null)
      return;
    this.Listeners(this.gameObject);
  }

  public delegate void GUIEvent(GameObject go);
}
