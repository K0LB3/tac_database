﻿// Decompiled with JetBrains decompiler
// Type: FastlitLightingSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class FastlitLightingSettings : MonoBehaviour
{
  public bool Occlude = true;
  public bool BakeOcclusion = true;
}
