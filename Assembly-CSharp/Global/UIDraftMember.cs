﻿// Decompiled with JetBrains decompiler
// Type: UIDraftMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4FC8B913-901B-4D78-BEA1-EB7B1073DC23
// Assembly location: D:\User\Desktop\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Expose")]
[RequireComponent(typeof (RectTransform))]
[ExecuteInEditMode]
public class UIDraftMember : MonoBehaviour
{
  [SerializeField]
  private bool mSearchGraphicComponent = true;
  public bool UseGraphic;

  private void Awake()
  {
    if (!Application.isEditor)
      return;
    this.tag = "EditorOnly";
    if (!this.mSearchGraphicComponent)
      return;
    if ((Object) this.GetComponent<Text>() != (Object) null)
      this.UseGraphic = true;
    if ((Object) this.GetComponent<RawImage>() != (Object) null)
      this.UseGraphic = true;
    if ((Object) this.GetComponent<Image>() != (Object) null)
      this.UseGraphic = true;
    this.mSearchGraphicComponent = false;
  }
}
