﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ToggleBlocksRaycasts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("Toggle/BlocksRaycasts", 32741)]
[FlowNode.Pin(10, "Disable", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(11, "Enable", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1, "Output", FlowNode.PinTypes.Output, 0)]
public class FlowNode_ToggleBlocksRaycasts : FlowNodePersistent
{
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;

  public override void OnActivate(int pinID)
  {
    CanvasGroup canvasGroup = !Object.op_Inequality((Object) this.Target, (Object) null) ? (CanvasGroup) null : (CanvasGroup) this.Target.GetComponent<CanvasGroup>();
    switch (pinID)
    {
      case 10:
        if (Object.op_Inequality((Object) canvasGroup, (Object) null))
        {
          canvasGroup.set_blocksRaycasts(false);
          break;
        }
        break;
      case 11:
        if (Object.op_Inequality((Object) canvasGroup, (Object) null))
        {
          canvasGroup.set_blocksRaycasts(true);
          break;
        }
        break;
    }
    this.ActivateOutputLinks(1);
  }
}
