﻿// Decompiled with JetBrains decompiler
// Type: UIDraftMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (RectTransform))]
[ExecuteInEditMode]
[AddComponentMenu("UI/Expose")]
public class UIDraftMember : MonoBehaviour
{
  [SerializeField]
  private bool mSearchGraphicComponent;
  public bool UseGraphic;

  public UIDraftMember()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    if (!Application.get_isEditor())
      return;
    ((Component) this).set_tag("EditorOnly");
    if (!this.mSearchGraphicComponent)
      return;
    if (Object.op_Inequality((Object) ((Component) this).GetComponent<Text>(), (Object) null))
      this.UseGraphic = true;
    if (Object.op_Inequality((Object) ((Component) this).GetComponent<RawImage>(), (Object) null))
      this.UseGraphic = true;
    if (Object.op_Inequality((Object) ((Component) this).GetComponent<Image>(), (Object) null))
      this.UseGraphic = true;
    this.mSearchGraphicComponent = false;
  }
}
