﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ImageArrayChangeIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("UI/ImageArrayChangeIndex", 32741)]
[FlowNode.Pin(0, "Set", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(10, "Out", FlowNode.PinTypes.Output, 10)]
public class FlowNode_ImageArrayChangeIndex : FlowNode
{
  [FlowNode.ShowInInfo]
  public int Index;
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;

  public override void OnActivate(int pinID)
  {
    ImageArray component = (ImageArray) (!Object.op_Inequality((Object) this.Target, (Object) null) ? ((Component) this).get_gameObject() : this.Target).GetComponent<ImageArray>();
    if (Object.op_Inequality((Object) component, (Object) null))
      component.ImageIndex = this.Index;
    this.ActivateOutputLinks(10);
  }
}
