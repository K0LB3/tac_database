﻿// Decompiled with JetBrains decompiler
// Type: SetupNetworkVersion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetupNetworkVersion : MonoBehaviour
{
  [SerializeField]
  private GameObject uiRoot;
  [SerializeField]
  private Toggle toggle;
  [SerializeField]
  private SRPG_InputField versionInputField;
  [SerializeField]
  private Dropdown serverNameDropdown;
  [SerializeField]
  private List<string> serverList;

  public SetupNetworkVersion()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.uiRoot.SetActive(false);
  }
}
