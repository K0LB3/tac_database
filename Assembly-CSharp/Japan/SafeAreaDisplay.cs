﻿// Decompiled with JetBrains decompiler
// Type: SafeAreaDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class SafeAreaDisplay : MonoBehaviour
{
  private float mDeltaTime;
  public Text SafeArea;

  public SafeAreaDisplay()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    Object.Destroy((Object) ((Component) this).get_gameObject());
  }

  private void Update()
  {
    Rect safeArea = SetCanvasBounds.GetSafeArea();
    string empty = string.Empty;
    string str = "00ff00";
    this.SafeArea.set_text(string.Format("<color=#{0}>rect_x = {1}</color>\n", (object) str, (object) ((Rect) ref safeArea).get_x()) + string.Format("<color=#{0}>rect_y = {1}</color>\n", (object) str, (object) ((Rect) ref safeArea).get_y()) + string.Format("<color=#{0}>rect_w = {1}</color>\n", (object) str, (object) ((Rect) ref safeArea).get_width()) + string.Format("<color=#{0}>rect_h = {1}</color>\n", (object) str, (object) ((Rect) ref safeArea).get_height()));
  }
}
