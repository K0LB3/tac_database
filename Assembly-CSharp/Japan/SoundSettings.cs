﻿// Decompiled with JetBrains decompiler
// Type: SoundSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class SoundSettings : ScriptableObject
{
  public string Tap;
  public string OK;
  public string Cancel;
  public string Select;
  public string Buzzer;
  public string Swipe;
  public string ScrollList;
  public string WindowPop;
  public string WindowClose;
  public const float BGMCrossFadeTime = 1f;
  private static SoundSettings mInstance;

  public SoundSettings()
  {
    base.\u002Ector();
  }

  public static SoundSettings Current
  {
    get
    {
      if (Object.op_Equality((Object) SoundSettings.mInstance, (Object) null))
      {
        SoundSettings.mInstance = (SoundSettings) Resources.Load<SoundSettings>(nameof (SoundSettings));
        Object.DontDestroyOnLoad((Object) SoundSettings.mInstance);
      }
      return SoundSettings.mInstance;
    }
  }
}
