﻿// Decompiled with JetBrains decompiler
// Type: AdjustRectScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class AdjustRectScale : MonoBehaviour
{
  [SerializeField]
  private float SetScale;
  private Rect lastSafeArea;
  private Vector3 initScale;
  private float lastSetScale;

  public AdjustRectScale()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    this.initScale = ((Component) component).get_transform().get_localScale();
    this.ApplySafeAreaScale(SetCanvasBounds.GetSafeArea());
  }

  private void ApplySafeAreaScale(Rect area)
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      if ((double) (((Rect) ref area).get_width() / (float) Screen.get_width()) < 1.0)
        ((Transform) component).set_localScale(new Vector3(this.SetScale, this.SetScale, this.SetScale));
      else
        ((Transform) component).set_localScale(this.initScale);
    }
    this.lastSafeArea = area;
    this.lastSetScale = this.SetScale;
  }
}
