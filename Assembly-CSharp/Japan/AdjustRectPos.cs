﻿// Decompiled with JetBrains decompiler
// Type: AdjustRectPos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class AdjustRectPos : MonoBehaviour
{
  [SerializeField]
  private Vector3 SetOffsetPos;
  private Rect lastSafeArea;
  private Vector3 initPos;
  private Vector3 lastSetOffsetPos;

  public AdjustRectPos()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    this.initPos = ((Transform) component).get_localPosition();
    this.ApplySafeAreaPos(SetCanvasBounds.GetSafeArea());
  }

  private void ApplySafeAreaPos(Rect area)
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      if ((double) (((Rect) ref area).get_width() / (float) Screen.get_width()) < 1.0)
        ((Transform) component).set_localPosition(Vector3.op_Addition(this.initPos, this.SetOffsetPos));
      else
        ((Transform) component).set_localPosition(this.initPos);
    }
    this.lastSafeArea = area;
    this.lastSetOffsetPos = this.SetOffsetPos;
  }
}
