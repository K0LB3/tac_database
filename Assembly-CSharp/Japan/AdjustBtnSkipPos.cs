﻿// Decompiled with JetBrains decompiler
// Type: AdjustBtnSkipPos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class AdjustBtnSkipPos : MonoBehaviour
{
  [SerializeField]
  private int OffsetPtx;

  public AdjustBtnSkipPos()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    Rect safeArea = SetCanvasBounds.GetSafeArea();
    if ((double) (((Rect) ref safeArea).get_width() / (float) Screen.get_width()) >= 1.0)
      return;
    Vector2 anchoredPosition = component.get_anchoredPosition();
    ref Vector2 local = ref anchoredPosition;
    local.x = (__Null) (local.x - (double) ((Rect) ref safeArea).get_x() / 3.0 * (double) (2 + this.OffsetPtx));
    component.set_anchoredPosition(anchoredPosition);
  }
}
