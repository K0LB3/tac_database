﻿// Decompiled with JetBrains decompiler
// Type: GrowthPush
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public class GrowthPush
{
  private static GrowthPush instance = new GrowthPush();

  private GrowthPush()
  {
  }

  public static GrowthPush GetInstance()
  {
    return GrowthPush.instance;
  }

  public void Initialize(string applicationId, string credentialId, GrowthPush.Environment environment)
  {
    this.Initialize(applicationId, credentialId, environment, true);
  }

  public void Initialize(string applicationId, string credentialId, GrowthPush.Environment environment, bool adInfoEnable)
  {
  }

  public void RequestDeviceToken(string senderId)
  {
  }

  public void RequestDeviceToken()
  {
  }

  public string GetDeviceToken()
  {
    return (string) null;
  }

  public void SetDeviceToken(string deviceToken)
  {
  }

  public void ClearBadge()
  {
  }

  public void SetTag(string name)
  {
    this.SetTag(name, string.Empty);
  }

  public void SetTag(string name, string value)
  {
  }

  public void TrackEvent(string name)
  {
    this.TrackEvent(name, string.Empty);
  }

  public void TrackEvent(string name, string value)
  {
  }

  public void TrackEvent(string name, string value, string gameObject, string methodName)
  {
  }

  public void RenderMessage(string uuid)
  {
  }

  public void SetDeviceTags()
  {
  }

  public void SetBaseUrl(string baseUrl)
  {
  }

  public enum Environment
  {
    Unknown,
    Development,
    Production,
  }
}
