﻿// Decompiled with JetBrains decompiler
// Type: SwitchDemoFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class SwitchDemoFrame : MonoBehaviour
{
  [SerializeField]
  private GameObject Frame;

  public SwitchDemoFrame()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    if (!Object.op_Inequality((Object) this.Frame, (Object) null))
      return;
    Rect safeArea = SetCanvasBounds.GetSafeArea();
    if ((double) ((Rect) ref safeArea).get_width() >= (double) Screen.get_width())
      return;
    this.Frame.SetActive(true);
  }
}
