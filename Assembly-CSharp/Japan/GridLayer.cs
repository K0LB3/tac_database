﻿// Decompiled with JetBrains decompiler
// Type: GridLayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (MeshRenderer))]
[RequireComponent(typeof (MeshFilter))]
[DisallowMultipleComponent]
public class GridLayer : MonoBehaviour
{
  public int LayerID;
  [Multiline]
  public string Preview;
  private int mXSize;
  private int mYSize;
  private Texture2D mTex;
  private Color32[] mPixels;
  private float mOpacity;
  private float mTransitTime;
  private float mDesiredOpacity;
  private float mCurrentOpacity;

  public GridLayer()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    if (string.IsNullOrEmpty(this.Preview))
      return;
    string[] strArray = this.Preview.Split('\n');
    GridMap<Color32> grid = new GridMap<Color32>(strArray[0].Length, strArray.Length);
    Color32[] color32Array = new Color32[7]
    {
      new Color32((byte) 0, (byte) 0, (byte) 0, (byte) 0),
      new Color32(byte.MaxValue, (byte) 64, (byte) 64, byte.MaxValue),
      new Color32((byte) 64, byte.MaxValue, (byte) 64, byte.MaxValue),
      new Color32((byte) 64, (byte) 64, byte.MaxValue, byte.MaxValue),
      new Color32(byte.MaxValue, byte.MaxValue, (byte) 64, byte.MaxValue),
      new Color32(byte.MaxValue, (byte) 64, byte.MaxValue, byte.MaxValue),
      new Color32((byte) 64, byte.MaxValue, byte.MaxValue, byte.MaxValue)
    };
    for (int y = 0; y < strArray.Length; ++y)
    {
      for (int index = 0; index < strArray[0].Length && index < strArray[y].Length; ++index)
      {
        int result;
        int.TryParse(strArray[y].Substring(index, 1), out result);
        grid.set(index, y, color32Array[result % color32Array.Length]);
      }
    }
    this.UpdateGrid(grid);
  }

  public void UpdateGrid(GridMap<Color32> grid)
  {
    if (grid.w != this.mXSize || grid.h != this.mYSize)
      this.InitTexture(grid.w, grid.h);
    int width = ((Texture) this.mTex).get_width();
    Color32[] mPixels = this.mPixels;
    for (int y = 0; y < grid.h; ++y)
    {
      for (int x = 0; x < grid.w; ++x)
        mPixels[x + 1 + (y + 1) * width] = grid.get(x, y);
    }
    this.mTex.SetPixels32(mPixels);
    this.mTex.Apply();
  }

  private void Awake()
  {
    ((Renderer) ((Component) this).GetComponent<Renderer>()).set_material((Material) Resources.Load<Material>("BG/GridMaterial"));
  }

  private void Update()
  {
    float num = 2f * Time.get_deltaTime();
    if ((double) this.mDesiredOpacity < (double) this.mCurrentOpacity)
      num = -num;
    this.mCurrentOpacity = Mathf.Clamp01(this.mCurrentOpacity + num);
    if ((double) this.mDesiredOpacity > 0.0 || (double) this.mCurrentOpacity > 0.0)
      return;
    ((Component) this).get_gameObject().SetActive(false);
  }

  private void OnWillRenderObject()
  {
    ((Renderer) ((Component) this).GetComponent<Renderer>()).get_material().SetFloat("_opacity", this.mCurrentOpacity);
  }

  private void InitTexture(int w, int h)
  {
    this.mXSize = w;
    this.mYSize = h;
    w += 2;
    h += 2;
    Object.DestroyImmediate((Object) this.mTex);
    if (!Mathf.IsPowerOfTwo(w))
      w = Mathf.NextPowerOfTwo(w);
    if (!Mathf.IsPowerOfTwo(h))
      h = Mathf.NextPowerOfTwo(h);
    this.mTex = new Texture2D(w, h, (TextureFormat) 4, false);
    ((Texture) this.mTex).set_wrapMode((TextureWrapMode) 1);
    ((Texture) this.mTex).set_filterMode((FilterMode) 0);
    Vector2 vector2 = (Vector2) null;
    vector2.x = (__Null) ((double) this.mXSize / (double) ((Texture) this.mTex).get_width() / (double) this.mXSize);
    vector2.y = (__Null) ((double) this.mYSize / (double) ((Texture) this.mTex).get_height() / (double) this.mYSize);
    Material material = ((Renderer) ((Component) this).GetComponent<Renderer>()).get_material();
    material.SetTexture("_indexTex", (Texture) this.mTex);
    material.SetTextureScale("_indexTex", vector2);
    material.SetTextureOffset("_indexTex", vector2);
    this.mPixels = new Color32[((Texture) this.mTex).get_width() * ((Texture) this.mTex).get_height()];
  }

  private void OnDestroy()
  {
    if (Object.op_Inequality((Object) this.mTex, (Object) null))
    {
      Object.DestroyImmediate((Object) this.mTex);
      this.mTex = (Texture2D) null;
    }
    this.mPixels = (Color32[]) null;
  }

  public void Show()
  {
    if ((double) this.mDesiredOpacity >= 1.0)
      return;
    this.mCurrentOpacity = 0.0f;
    this.mDesiredOpacity = 1f;
    ((Component) this).get_gameObject().SetActive(true);
  }

  public void Hide()
  {
    if ((double) this.mDesiredOpacity <= 0.0)
      return;
    this.mDesiredOpacity = 0.0f;
  }

  public void SetMask(bool enable)
  {
    Material material = ((Renderer) ((Component) this).GetComponent<Renderer>()).get_material();
    if (enable)
      material.set_shaderKeywords(new string[1]
      {
        "WITH_MASK"
      });
    else
      material.set_shaderKeywords(new string[1]
      {
        "WITHOUT_MASK"
      });
  }

  public void ChangeMaterial(string path)
  {
    if (string.IsNullOrEmpty(path))
      return;
    Material material = (Material) Resources.Load<Material>(path);
    if (!Object.op_Implicit((Object) material))
      return;
    Renderer component = (Renderer) ((Component) this).GetComponent<Renderer>();
    if (!Object.op_Implicit((Object) component))
      return;
    component.set_material(material);
  }
}
