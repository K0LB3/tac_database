﻿// Decompiled with JetBrains decompiler
// Type: GachaStone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class GachaStone : MonoBehaviour
{
  public Camera TargetCamera;

  public GachaStone()
  {
    base.\u002Ector();
  }

  public string DROP_ID { get; set; }

  private void Start()
  {
    if (!Object.op_Equality((Object) this.TargetCamera, (Object) null))
      return;
    this.TargetCamera = Camera.get_main();
  }

  private void Update()
  {
    ((Component) this).get_transform().LookAt(((Component) this.TargetCamera).get_transform());
  }
}
