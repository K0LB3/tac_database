﻿// Decompiled with JetBrains decompiler
// Type: ServerTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class ServerTime : MonoBehaviour
{
  public ServerTime()
  {
    base.\u002Ector();
  }

  private void OnGUI()
  {
    GUILayout.BeginArea(new Rect((float) (Screen.get_width() / 2 - 100), 0.0f, 200f, 30f));
    GUILayout.Label(string.Format("Time Offset: {0}", (object) (PhotonNetwork.ServerTimestamp - System.Environment.TickCount)), new GUILayoutOption[0]);
    if (GUILayout.Button("fetch", new GUILayoutOption[0]))
      PhotonNetwork.FetchServerTimestamp();
    GUILayout.EndArea();
  }
}
