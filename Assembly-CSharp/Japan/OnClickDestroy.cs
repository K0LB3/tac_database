﻿// Decompiled with JetBrains decompiler
// Type: OnClickDestroy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Photon;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (PhotonView))]
public class OnClickDestroy : MonoBehaviour
{
  public bool DestroyByRpc;

  public void OnClick()
  {
    if (!this.DestroyByRpc)
      PhotonNetwork.Destroy(((Component) this).get_gameObject());
    else
      this.photonView.RPC("DestroyRpc", PhotonTargets.AllBuffered);
  }

  [DebuggerHidden]
  [PunRPC]
  public IEnumerator DestroyRpc()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new OnClickDestroy.\u003CDestroyRpc\u003Ec__Iterator0()
    {
      \u0024this = this
    };
  }
}
