﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_OnPartyChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("Event/OnPartyChange", 58751)]
[FlowNode.Pin(1, "On", FlowNode.PinTypes.Output, 1)]
public class FlowNode_OnPartyChange : FlowNodeEvent<FlowNode_OnPartyChange>
{
}
