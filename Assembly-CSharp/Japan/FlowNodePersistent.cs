﻿// Decompiled with JetBrains decompiler
// Type: FlowNodePersistent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public abstract class FlowNodePersistent : FlowNode
{
  protected override void Awake()
  {
    base.Awake();
    ((Behaviour) this).set_enabled(true);
  }
}
