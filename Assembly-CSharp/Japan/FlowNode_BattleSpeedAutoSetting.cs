﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_BattleSpeedAutoSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using SRPG;
using UnityEngine;
using UnityEngine.UI;

[FlowNode.NodeType("System/BattleSpeedAutoSetting", 32741)]
[FlowNode.Pin(1, "オプション画面での倍速切り替え", FlowNode.PinTypes.Input, 1)]
[FlowNode.Pin(2, "オートプレイON", FlowNode.PinTypes.Input, 2)]
[FlowNode.Pin(3, "オートプレイOFF", FlowNode.PinTypes.Input, 3)]
[FlowNode.Pin(4, "倍速機能ON", FlowNode.PinTypes.Input, 4)]
[FlowNode.Pin(5, "倍速機能OFF", FlowNode.PinTypes.Input, 5)]
public class FlowNode_BattleSpeedAutoSetting : FlowNode
{
  [SerializeField]
  private bool isShowButtonNotVip = true;
  private const int INPUT_CHANGE_BATTLE_SPEED = 1;
  private const int INPUT_AUTO_PLAY_ON = 2;
  private const int INPUT_AUTO_PLAY_OFF = 3;
  private const int INPUT_BATTLE_SPEED_ON = 4;
  private const int INPUT_BATTLE_SPEED_OFF = 5;
  [SerializeField]
  private Toggle battleSpeedToggle;
  [SerializeField]
  private GameObject battleSpeedLockObj;
  [SerializeField]
  private Toggle autoPlayToggle;
  public const string AUTO_TOGGLE_CHANGE_KEY = "AUTO_TOGGLE_CHANGE_KEY";

  protected override void Awake()
  {
    base.Awake();
    BattleSpeedController.BattleTimeConfig = GameUtility.Config_UseBattleSpeed.Value;
    if (!BattleSpeedController.IsShowSpeedButton() && Object.op_Implicit((Object) this.battleSpeedToggle))
      ((Component) this.battleSpeedToggle).get_gameObject().SetActive(false);
    else if (BattleSpeedController.isPremium)
    {
      if (Object.op_Implicit((Object) this.battleSpeedLockObj))
        this.battleSpeedLockObj.SetActive(false);
      ((Selectable) this.battleSpeedToggle).set_interactable(true);
      this.battleSpeedToggle.set_isOn(BattleSpeedController.BattleTimeConfig);
    }
    else
    {
      if (Object.op_Implicit((Object) this.battleSpeedLockObj))
        this.battleSpeedLockObj.SetActive(true);
      if (this.isShowButtonNotVip)
      {
        ((Selectable) this.battleSpeedToggle).set_interactable(false);
        this.battleSpeedToggle.set_isOn(false);
      }
      else
        ((Component) this.battleSpeedToggle).get_gameObject().SetActive(false);
    }
  }

  public override void OnActivate(int pinID)
  {
    switch (pinID)
    {
      case 1:
        if (BattleSpeedController.isAutoOnly && BattleSpeedController.BattleTimeConfig)
          this.autoPlayToggle.set_isOn(true);
        this.battleSpeedToggle.set_isOn(BattleSpeedController.BattleTimeConfig);
        break;
      case 3:
        if (!BattleSpeedController.isAutoOnly)
          break;
        BattleSpeedController.BattleTimeConfig = false;
        this.battleSpeedToggle.set_isOn(false);
        BattleSpeedController.ResetSpeed();
        break;
      case 4:
        if (!BattleSpeedController.isPremium)
          break;
        if (BattleSpeedController.isAutoOnly)
          this.autoPlayToggle.set_isOn(true);
        BattleSpeedController.BattleTimeConfig = true;
        BattleSpeedController.SetUp();
        break;
      case 5:
        BattleSpeedController.BattleTimeConfig = false;
        BattleSpeedController.ResetSpeed();
        break;
    }
  }

  protected override void OnDestroy()
  {
    if (!Object.op_Inequality((Object) MonoSingleton<TimeManager>.Instance, (Object) null))
      return;
    BattleSpeedController.EndBattle();
  }
}
