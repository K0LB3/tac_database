﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ShutdownApp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("System/Shutdown Application")]
[FlowNode.Pin(1, "Shutdown", FlowNode.PinTypes.Input, 0)]
public class FlowNode_ShutdownApp : FlowNode
{
  public override void OnActivate(int pinID)
  {
    if (pinID != 1)
      return;
    Application.Quit();
  }
}
