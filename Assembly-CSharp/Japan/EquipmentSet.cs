﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EquipmentSet : ScriptableObject
{
  public EquipmentSet.EquipmentType Type;
  public bool PrimaryHidden;
  public bool PrimaryForceOverride;
  public GameObject PrimaryHand;
  public List<GameObject> PrimaryHandChangeLists;
  public bool SecondaryHidden;
  public bool SecondaryForceOverride;
  public GameObject SecondaryHand;
  public List<GameObject> SecondaryHandChangeLists;
  public List<GameObject> OptionEquipmentLists;

  public EquipmentSet()
  {
    base.\u002Ector();
  }

  public enum EquipmentType
  {
    Melee,
    Bow,
    Gun,
  }
}
