﻿// Decompiled with JetBrains decompiler
// Type: ExceptionMonitor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using UnityEngine;

[AddComponentMenu("")]
public class ExceptionMonitor : MonoBehaviour
{
  private static ExceptionMonitor mInstnace;
  private bool bMessageDraw;

  public ExceptionMonitor()
  {
    base.\u002Ector();
  }

  public static void Start()
  {
    if (!Object.op_Equality((Object) ExceptionMonitor.mInstnace, (Object) null))
      return;
    ExceptionMonitor.mInstnace = (ExceptionMonitor) new GameObject(nameof (ExceptionMonitor)).AddComponent<ExceptionMonitor>();
  }

  private void Awake()
  {
    Object.DontDestroyOnLoad((Object) ((Component) this).get_gameObject());
    ((Object) this).set_hideFlags((HideFlags) 61);
    // ISSUE: method pointer
    Application.add_logMessageReceived(new Application.LogCallback((object) this, __methodptr(HandleLog)));
  }

  private void HandleLog(string logString, string stackTrace, LogType type)
  {
    if (type != 4 || this.bMessageDraw)
      return;
    if (logString.IndexOf("DllNotFoundException:") >= 0)
      this.MessageBoxDLL();
    else
      this.MessageBoxDefault();
  }

  private void MessageBoxDefault()
  {
    this.bMessageDraw = true;
    EmbedSystemMessage.Create(LocalizedText.Get("embed.APP_EXCEPTION"), (EmbedSystemMessage.SystemMessageEvent) (yes =>
    {
      this.bMessageDraw = false;
      Application.Quit();
    }));
  }

  private void MessageBoxDLL()
  {
    this.bMessageDraw = true;
    EmbedSystemMessageEx embedSystemMessageEx = EmbedSystemMessageEx.Create(LocalizedText.Get("embed.APP_DLL_EXCEPTION"));
    embedSystemMessageEx.AddButton(LocalizedText.Get("embed.APP_DLL_SUPPORT"), false, (EmbedSystemMessageEx.SystemMessageEvent) (ok => Application.OpenURL("https://al.fg-games.co.jp/news/206/")));
    embedSystemMessageEx.AddButton(LocalizedText.Get("embed.APP_DLL_QUIT"), true, (EmbedSystemMessageEx.SystemMessageEvent) (ok =>
    {
      this.bMessageDraw = false;
      Application.Quit();
    }));
  }
}
