﻿// Decompiled with JetBrains decompiler
// Type: GachaVoice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class GachaVoice : MonoBehaviour
{
  public string DirectCharName;
  public int Excites;
  public string Play1CueName;
  public string Play2CueName;
  public string[] Play3Cuename;
  private int excites;
  private string mCharName;
  private string mCueName;
  private MySound.Voice mVoice;

  public GachaVoice()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    if (string.IsNullOrEmpty(this.DirectCharName))
      this.DirectCharName = "uroboros";
    this.mVoice = new MySound.Voice(this.DirectCharName);
    this.excites = 0;
    this.mCharName = this.DirectCharName;
  }

  public void Play1()
  {
    this.SetupCueName(this.Play1CueName);
    this.Play();
  }

  public void Play2()
  {
    this.SetupCueName(this.Play2CueName);
    this.Play();
  }

  public void Play3()
  {
    this.excites = this.Excites <= 0 || this.Excites >= 4 ? 0 : this.Excites - 1;
    this.SetupCueName(this.Play3Cuename[this.excites]);
    this.Play();
  }

  private void Play()
  {
    if (this.mVoice == null)
      return;
    this.mVoice.Play(this.mCueName, 0.0f, false);
  }

  public void Stop()
  {
    if (this.mVoice == null)
      return;
    this.mVoice.StopAll(0.0f);
  }

  public void Discard()
  {
    if (this.mVoice != null)
      this.mVoice.Cleanup();
    this.mVoice = (MySound.Voice) null;
    this.mCharName = (string) null;
  }

  public bool SetupCueName(string cuename)
  {
    if (string.IsNullOrEmpty(cuename) || string.IsNullOrEmpty(this.mCharName))
      return false;
    this.mCueName = MySound.Voice.ReplaceCharNameOfCueName(cuename, this.mCharName);
    return true;
  }
}
