﻿// Decompiled with JetBrains decompiler
// Type: Title
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Drafts/Title")]
public class Title : UIDraft
{
  [UIDraft.AutoGenerated]
  public Button Button_TapStart;
  [UIDraft.AutoGenerated]
  public Button Button_NewGame;
}
