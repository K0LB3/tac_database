﻿// Decompiled with JetBrains decompiler
// Type: SyncSize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[DisallowMultipleComponent]
public class SyncSize : MonoBehaviour
{
  public RectTransform Source;
  public float ExtraW;
  public float ExtraH;
  private float mLastWidth;
  private float mLastHeight;
  private RectTransform mRect;

  public SyncSize()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.mRect = ((Component) this).get_transform() as RectTransform;
    this.Sync();
  }

  private void LateUpdate()
  {
    this.Sync();
  }

  private void Sync()
  {
    if (Object.op_Equality((Object) this.Source, (Object) null) || Object.op_Equality((Object) this.mRect, (Object) null))
      return;
    Rect rect1 = this.Source.get_rect();
    float num1 = ((Rect) ref rect1).get_width() + this.ExtraW;
    Rect rect2 = this.Source.get_rect();
    float num2 = ((Rect) ref rect2).get_height() + this.ExtraH;
    if ((double) num1 == (double) this.mLastWidth && (double) num2 == (double) this.mLastHeight)
      return;
    this.mLastWidth = num1;
    this.mLastHeight = num2;
    Vector2 vector2 = (Vector2) null;
    vector2.x = (__Null) (double) this.mLastWidth;
    vector2.y = (__Null) (double) this.mLastHeight;
    this.mRect.set_sizeDelta(vector2);
  }
}
