﻿// Decompiled with JetBrains decompiler
// Type: TestPositionAsUv1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class TestPositionAsUv1 : PositionAsUV1
{
  protected TestPositionAsUv1()
  {
    base.\u002Ector();
  }

  public virtual void ModifyMesh(VertexHelper vh)
  {
    UIVertex uiVertex = (UIVertex) null;
    for (int index = 0; index < vh.get_currentVertCount(); ++index)
    {
      vh.PopulateUIVertex(ref uiVertex, index);
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      uiVertex.uv1 = (__Null) new Vector2((float) (^(Vector3&) ref uiVertex.position).x, (float) (^(Vector3&) ref uiVertex.position).y);
      vh.SetUIVertex(uiVertex, index);
    }
  }

  public virtual void ModifyMesh(Mesh mesh)
  {
    ((BaseMeshEffect) this).ModifyMesh(mesh);
  }
}
