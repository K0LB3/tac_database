﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_LocalizedText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

[FlowNode.NodeType("System/LocalizedText", 32741)]
[FlowNode.Pin(0, "Load", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(10, "Unload", FlowNode.PinTypes.Input, 0)]
public class FlowNode_LocalizedText : FlowNode
{
  public string tableID;

  public override void OnActivate(int pinID)
  {
    if (string.IsNullOrEmpty(this.tableID))
      return;
    if (pinID == 0)
    {
      LocalizedText.LoadTable(this.tableID, false);
    }
    else
    {
      if (pinID != 1)
        return;
      LocalizedText.UnloadTable(this.tableID);
    }
  }
}
