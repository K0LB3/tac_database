﻿// Decompiled with JetBrains decompiler
// Type: EventQuit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.Events;

public class EventQuit : MonoBehaviour
{
  public EventQuit()
  {
    base.\u002Ector();
  }

  private static EventQuit Instance { get; set; }

  public static EventQuit Find()
  {
    return EventQuit.Instance;
  }

  public UnityAction OnClick { private get; set; }

  public void Quit()
  {
    if (this.OnClick == null)
      return;
    this.OnClick.Invoke();
  }

  private void Awake()
  {
    if (Object.op_Inequality((Object) null, (Object) EventQuit.Instance))
      Object.Destroy((Object) this);
    EventQuit.Instance = this;
  }

  private void OnDestroy()
  {
    if (!Object.op_Equality((Object) this, (Object) EventQuit.Instance))
      return;
    EventQuit.Instance = (EventQuit) null;
  }

  private void Update()
  {
  }
}
