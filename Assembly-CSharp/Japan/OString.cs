﻿// Decompiled with JetBrains decompiler
// Type: OString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using CodeStage.AntiCheat.ObscuredTypes;

public struct OString
{
  private ObscuredString value;

  public OString(string value)
  {
    this.value = (ObscuredString) value;
  }

  public static implicit operator OString(string value)
  {
    return new OString(value);
  }

  public static implicit operator string(OString value)
  {
    return (string) value.value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }
}
