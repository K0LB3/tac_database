﻿// Decompiled with JetBrains decompiler
// Type: KuroObi
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class KuroObi : Graphic
{
  public KuroObi()
  {
    base.\u002Ector();
  }

  public static float CalcObiSize()
  {
    if (!SRPG_CanvasScaler.UseKuroObi)
      return 0.0f;
    float num1 = (float) Screen.get_width() / (float) Screen.get_height();
    float num2 = 1.6f;
    if ((double) num1 >= (double) num2)
      return 0.0f;
    return (float) ((750.0 / (double) (num1 / num2) - 750.0) * 0.5);
  }

  public virtual bool Raycast(Vector2 sp, Camera eventCamera)
  {
    RectTransform transform = ((Component) this).get_transform() as RectTransform;
    Rect rect = transform.get_rect();
    float num = KuroObi.CalcObiSize();
    Vector2 vector2;
    RectTransformUtility.ScreenPointToLocalPointInRectangle(transform, sp, (Camera) null, ref vector2);
    return vector2.y < (double) ((Rect) ref rect).get_yMin() + (double) num || (double) ((Rect) ref rect).get_yMax() - (double) num < vector2.y;
  }

  protected virtual void OnPopulateMesh(VertexHelper vh)
  {
    vh.Clear();
    Color32 color32 = Color32.op_Implicit(this.get_color());
    Rect rect = (((Component) this).get_transform() as RectTransform).get_rect();
    UIVertex uiVertex = (UIVertex) null;
    float num1 = KuroObi.CalcObiSize();
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_yMax()));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMax(), ((Rect) ref rect).get_yMax()));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    float num2 = ((Rect) ref rect).get_yMax() - num1;
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMax(), num2));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMin(), num2));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    float num3 = ((Rect) ref rect).get_yMin() + num1;
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMin(), num3));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMax(), num3));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMax(), ((Rect) ref rect).get_yMin()));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
    uiVertex.position = (__Null) Vector2.op_Implicit(new Vector2(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_yMin()));
    uiVertex.color = (__Null) color32;
    vh.AddVert(uiVertex);
  }
}
