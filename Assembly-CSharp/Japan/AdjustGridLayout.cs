﻿// Decompiled with JetBrains decompiler
// Type: AdjustGridLayout
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class AdjustGridLayout : MonoBehaviour
{
  [SerializeField]
  private float OffsetSize;
  private float lastOffsetSize;
  private float initSize;

  public AdjustGridLayout()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    GridLayoutGroup component = (GridLayoutGroup) ((Component) this).GetComponent<GridLayoutGroup>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    this.initSize = (float) component.get_cellSize().x;
    this.SetGridSizeX();
  }

  private void SetGridSizeX()
  {
    GridLayoutGroup component = (GridLayoutGroup) ((Component) this).GetComponent<GridLayoutGroup>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    Rect safeArea = SetCanvasBounds.GetSafeArea();
    float num = ((Rect) ref safeArea).get_width() / (float) Screen.get_width();
    if ((double) num >= 1.0)
      return;
    Vector2 cellSize = component.get_cellSize();
    cellSize.x = (__Null) ((double) this.initSize * (double) num);
    ref Vector2 local = ref cellSize;
    local.x = (__Null) (local.x + (double) this.OffsetSize);
    component.set_cellSize(cellSize);
    this.lastOffsetSize = this.OffsetSize;
  }
}
