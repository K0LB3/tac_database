﻿// Decompiled with JetBrains decompiler
// Type: ImageSpriteSheet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("UI/ImageSpriteSheet (透明)")]
public class ImageSpriteSheet : Image
{
  [HeaderBar("▼スプライトシートのパス")]
  [SerializeField]
  [StringIsResourcePath(typeof (SpriteSheet))]
  private string m_SpriteSheetPath;
  [SerializeField]
  private string m_DefaultKey;
  private SpriteSheet m_SpriteSheet;

  public ImageSpriteSheet()
  {
    base.\u002Ector();
  }

  protected virtual void Start()
  {
    ((UIBehaviour) this).Start();
    if (!Application.get_isPlaying())
      return;
    if (!string.IsNullOrEmpty(this.m_SpriteSheetPath))
      this.m_SpriteSheet = AssetManager.Load<SpriteSheet>(this.m_SpriteSheetPath);
    this.SetSprite(this.m_DefaultKey);
  }

  public string DefaultKey
  {
    get
    {
      return this.m_DefaultKey;
    }
  }

  public void SetSprite(string key)
  {
    this.set_sprite(this.GetSprite(key));
  }

  public Sprite GetSprite(string key)
  {
    if (Object.op_Inequality((Object) this.m_SpriteSheet, (Object) null))
      return this.m_SpriteSheet.GetSprite(key);
    return (Sprite) null;
  }

  protected virtual void OnPopulateMesh(VertexHelper toFill)
  {
    if (Object.op_Inequality((Object) this.get_sprite(), (Object) null))
      base.OnPopulateMesh(toFill);
    else
      toFill.Clear();
  }
}
