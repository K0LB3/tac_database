﻿// Decompiled with JetBrains decompiler
// Type: SRPG_CanvasScaler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("Layout/Canvas Scaler (SRPG)")]
[ExecuteInEditMode]
public class SRPG_CanvasScaler : CanvasScaler
{
  public static bool UseKuroObi;
  public const float MinScreenWidth = 1200f;
  public const float MinScreenHeight = 750f;

  public SRPG_CanvasScaler()
  {
    base.\u002Ector();
  }

  protected virtual void Awake()
  {
    ((UIBehaviour) this).Awake();
    this.set_uiScaleMode((CanvasScaler.ScaleMode) 1);
    if (SRPG_CanvasScaler.UseKuroObi)
    {
      this.set_screenMatchMode((CanvasScaler.ScreenMatchMode) 1);
    }
    else
    {
      this.set_screenMatchMode((CanvasScaler.ScreenMatchMode) 0);
      this.set_matchWidthOrHeight(1f);
    }
    this.set_referenceResolution(new Vector2(1200f, 750f));
  }
}
