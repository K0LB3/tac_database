﻿// Decompiled with JetBrains decompiler
// Type: GpsGiftIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using SRPG;
using UnityEngine;
using UnityEngine.UI;

public class GpsGiftIcon : MonoBehaviour
{
  private Image m_Image;

  public GpsGiftIcon()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.m_Image = (Image) ((Component) this).get_gameObject().GetComponent<Image>();
    if (Object.op_Inequality((Object) this.m_Image, (Object) null))
      ((Behaviour) this.m_Image).set_enabled(false);
    ((Component) this).get_gameObject().SetActive(false);
  }

  private void Update()
  {
    if (Object.op_Equality((Object) this.m_Image, (Object) null))
      return;
    GameManager instance = MonoSingleton<GameManager>.Instance;
    if (!Object.op_Inequality((Object) instance, (Object) null) || instance.Player == null)
      return;
    if (instance.Player.ValidGpsGift)
      ((Behaviour) this.m_Image).set_enabled(true);
    else
      ((Behaviour) this.m_Image).set_enabled(false);
  }
}
