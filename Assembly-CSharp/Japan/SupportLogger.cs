﻿// Decompiled with JetBrains decompiler
// Type: SupportLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class SupportLogger : MonoBehaviour
{
  public bool LogTrafficStats;

  public SupportLogger()
  {
    base.\u002Ector();
  }

  public void Start()
  {
    if (!Object.op_Equality((Object) GameObject.Find("PunSupportLogger"), (Object) null))
      return;
    GameObject gameObject = new GameObject("PunSupportLogger");
    Object.DontDestroyOnLoad((Object) gameObject);
    ((SupportLogging) gameObject.AddComponent<SupportLogging>()).LogTrafficStats = this.LogTrafficStats;
  }
}
