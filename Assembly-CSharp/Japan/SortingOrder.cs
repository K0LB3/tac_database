﻿// Decompiled with JetBrains decompiler
// Type: SortingOrder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("Rendering/SortingOrder")]
[RequireComponent(typeof (Renderer))]
public class SortingOrder : MonoBehaviour
{
  [SerializeField]
  private int mSortingOrder;

  public SortingOrder()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    ((Behaviour) this).set_enabled(false);
  }

  private void OnValidate()
  {
    ((Renderer) ((Component) this).GetComponent<Renderer>()).set_sortingOrder(this.mSortingOrder);
  }
}
