﻿// Decompiled with JetBrains decompiler
// Type: NonInteractableMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof (Selectable))]
public class NonInteractableMessage : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
  public string Caption;
  public string Message;

  public NonInteractableMessage()
  {
    base.\u002Ector();
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    if (((Selectable) ((Component) this).GetComponent<Selectable>()).get_interactable())
      return;
    UIUtility.NegativeSystemMessage(LocalizedText.Get(this.Caption), LocalizedText.Get(this.Message), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
  }
}
