﻿// Decompiled with JetBrains decompiler
// Type: SceneAssetBundle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class SceneAssetBundle : MonoBehaviour
{
  public int Hash;

  public SceneAssetBundle()
  {
    base.\u002Ector();
  }

  private void LateUpdate()
  {
    this.CheckChildren();
  }

  private void CheckChildren()
  {
    if (((Component) this).get_transform().get_childCount() > 0)
      return;
    Object.Destroy((Object) ((Component) this).get_gameObject());
  }
}
