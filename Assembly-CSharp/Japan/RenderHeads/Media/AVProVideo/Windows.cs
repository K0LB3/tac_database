﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Windows
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public static class Windows
  {
    public const string AudioDeviceOutputName_Vive = "HTC VIVE USB Audio";
    public const string AudioDeviceOutputName_Rift = "Rift Audio";

    public enum VideoApi
    {
      MediaFoundation,
      DirectShow,
    }
  }
}
