﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.ErrorCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum ErrorCode
  {
    None = 0,
    LoadFailed = 100, // 0x00000064
    DecodeFailed = 200, // 0x000000C8
  }
}
