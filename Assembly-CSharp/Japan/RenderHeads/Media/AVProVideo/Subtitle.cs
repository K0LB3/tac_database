﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Subtitle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public class Subtitle
  {
    public int index;
    public string text;
    public int timeStartMs;
    public int timeEndMs;

    public bool IsBefore(float time)
    {
      if ((double) time > (double) this.timeStartMs)
        return (double) time > (double) this.timeEndMs;
      return false;
    }

    public bool IsTime(float time)
    {
      if ((double) time >= (double) this.timeStartMs)
        return (double) time < (double) this.timeEndMs;
      return false;
    }
  }
}
