﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Platform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum Platform
  {
    Windows = 0,
    MacOSX = 1,
    iOS = 2,
    tvOS = 3,
    Android = 4,
    WindowsPhone = 5,
    WindowsUWP = 6,
    WebGL = 7,
    Count = 8,
    Unknown = 100, // 0x00000064
  }
}
