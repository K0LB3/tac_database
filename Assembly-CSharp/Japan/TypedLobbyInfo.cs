﻿// Decompiled with JetBrains decompiler
// Type: TypedLobbyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public class TypedLobbyInfo : TypedLobby
{
  public int PlayerCount;
  public int RoomCount;

  public override string ToString()
  {
    return string.Format("TypedLobbyInfo '{0}'[{1}] rooms: {2} players: {3}", (object) this.Name, (object) this.Type, (object) this.RoomCount, (object) this.PlayerCount);
  }
}
