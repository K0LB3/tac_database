﻿// Decompiled with JetBrains decompiler
// Type: CustomAuthenticationType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public enum CustomAuthenticationType : byte
{
  Custom = 0,
  Steam = 1,
  Facebook = 2,
  Oculus = 3,
  PlayStation = 4,
  Xbox = 5,
  None = 255, // 0xFF
}
