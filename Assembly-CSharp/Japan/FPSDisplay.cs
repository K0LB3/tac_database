﻿// Decompiled with JetBrains decompiler
// Type: FPSDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour
{
  private float mDeltaTime;
  public Text FPS;

  public FPSDisplay()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    Object.Destroy((Object) ((Component) this).get_gameObject());
  }

  private void Update()
  {
    this.mDeltaTime += (float) (((double) Time.get_unscaledDeltaTime() - (double) this.mDeltaTime) * 0.100000001490116);
    string empty = string.Empty;
    float num1 = this.mDeltaTime * 1000f;
    string str1 = "00ff00";
    if ((double) num1 >= 50.0)
      str1 = "ff0000";
    else if ((double) num1 >= 33.0)
      str1 = "ffff00";
    string str2 = string.Format("<color=#{0}>{1:0.00} ms</color>", (object) str1, (object) num1) + string.Format("\nAssets:{0}/{1}", (object) AssetManager.GetLoadedAssetNames().Length, (object) AssetManager.GetOpenedAssetBundleNames().Length);
    float num2 = 1048576f;
    long monoUsedSizeLong = Profiler.GetMonoUsedSizeLong();
    long monoHeapSizeLong = Profiler.GetMonoHeapSizeLong();
    long allocatedMemoryLong = Profiler.GetTotalAllocatedMemoryLong();
    long reservedMemoryLong = Profiler.GetTotalReservedMemoryLong();
    string str3 = monoHeapSizeLong + reservedMemoryLong <= 314572800L ? (monoHeapSizeLong + reservedMemoryLong <= 262144000L ? "00ff00" : "ffff00") : "ff0000";
    this.FPS.set_text(str2 + string.Format("\n<color=#{2}>Mono:{0}/{1}MB</color>", (object) ((float) monoUsedSizeLong / num2).ToString("F2"), (object) ((float) monoHeapSizeLong / num2).ToString("F2"), (object) str3) + string.Format("\n<color=#{2}>Unity:{0}/{1}MB</color>", (object) ((float) allocatedMemoryLong / num2).ToString("F2"), (object) ((float) reservedMemoryLong / num2).ToString("F2"), (object) str3) + string.Format("\n<color=#{1}>Texture:</color><color=#{2}>{0}</color>", (object) AssetManager.Format.ToPath().Replace("/", string.Empty), (object) "00ff00", (object) "ff0000"));
  }
}
