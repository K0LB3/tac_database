﻿// Decompiled with JetBrains decompiler
// Type: LoginNewsChecker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using UnityEngine;

public class LoginNewsChecker : MonoBehaviour
{
  public LoginNewsChecker()
  {
    base.\u002Ector();
  }

  private void Update()
  {
    if (!LoginNewsInfo.IsChangePubInfo())
      return;
    HomeWindow objectOfType = (HomeWindow) Object.FindObjectOfType<HomeWindow>();
    if (!Object.op_Inequality((Object) objectOfType, (Object) null))
      return;
    objectOfType.ChangeNewsState();
  }
}
