﻿// Decompiled with JetBrains decompiler
// Type: OpJoinRandomRoomParams
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using ExitGames.Client.Photon;

internal class OpJoinRandomRoomParams
{
  public Hashtable ExpectedCustomRoomProperties;
  public byte ExpectedMaxPlayers;
  public MatchmakingMode MatchingType;
  public TypedLobby TypedLobby;
  public string SqlLobbyFilter;
  public string[] ExpectedUsers;
}
