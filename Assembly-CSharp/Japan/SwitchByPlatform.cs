﻿// Decompiled with JetBrains decompiler
// Type: SwitchByPlatform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class SwitchByPlatform : MonoBehaviour
{
  [SerializeField]
  public RuntimePlatform[] hides;

  public SwitchByPlatform()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    foreach (int hide in this.hides)
    {
      if (Application.get_platform() == (RuntimePlatform) hide)
        ((Component) this).get_gameObject().SetActive(false);
    }
  }
}
