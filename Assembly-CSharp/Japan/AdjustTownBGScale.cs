﻿// Decompiled with JetBrains decompiler
// Type: AdjustTownBGScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class AdjustTownBGScale : MonoBehaviour
{
  [SerializeField]
  private float RevisionScaleValue;
  private bool m_set_scale_safe_area;
  private Vector3 m_init_scale;
  private Rect m_safe_area;

  public AdjustTownBGScale()
  {
    base.\u002Ector();
  }

  public float RevisionScale
  {
    get
    {
      return this.RevisionScaleValue;
    }
  }

  private void Start()
  {
    this.m_init_scale = ((Component) this).get_transform().get_localScale();
    this.m_safe_area = SetCanvasBounds.GetSafeArea();
    if ((double) ((Rect) ref this.m_safe_area).get_width() >= (double) Screen.get_width() || this.m_set_scale_safe_area)
      return;
    this.SetScale();
  }

  private void SetScale()
  {
    this.m_set_scale_safe_area = !this.m_set_scale_safe_area;
    if (this.m_set_scale_safe_area)
    {
      float num = (float) (1.0 + (1.0 - (double) ((Rect) ref this.m_safe_area).get_width() / (double) Screen.get_width())) + this.RevisionScaleValue;
      Vector3 vector3;
      ((Vector3) ref vector3).\u002Ector(num, num, 1f);
      ((Component) this).get_transform().set_localScale(vector3);
    }
    else
      ((Component) this).get_transform().set_localScale(this.m_init_scale);
  }
}
