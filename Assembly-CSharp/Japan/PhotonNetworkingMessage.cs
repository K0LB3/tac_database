﻿// Decompiled with JetBrains decompiler
// Type: PhotonNetworkingMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public enum PhotonNetworkingMessage
{
  OnConnectedToPhoton,
  OnLeftRoom,
  OnMasterClientSwitched,
  OnPhotonCreateRoomFailed,
  OnPhotonJoinRoomFailed,
  OnCreatedRoom,
  OnJoinedLobby,
  OnLeftLobby,
  OnDisconnectedFromPhoton,
  OnConnectionFail,
  OnFailedToConnectToPhoton,
  OnReceivedRoomListUpdate,
  OnJoinedRoom,
  OnPhotonPlayerConnected,
  OnPhotonPlayerDisconnected,
  OnPhotonRandomJoinFailed,
  OnConnectedToMaster,
  OnPhotonSerializeView,
  OnPhotonInstantiate,
  OnPhotonMaxCccuReached,
  OnPhotonCustomRoomPropertiesChanged,
  OnPhotonPlayerPropertiesChanged,
  OnUpdatedFriendList,
  OnCustomAuthenticationFailed,
  OnCustomAuthenticationResponse,
  OnWebRpcResponse,
  OnOwnershipRequest,
  OnLobbyStatisticsUpdate,
  OnPhotonPlayerActivityChanged,
  OnOwnershipTransfered,
}
