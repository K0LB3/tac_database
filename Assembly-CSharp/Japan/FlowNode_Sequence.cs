﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_Sequence
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("Sequence", 32741)]
[FlowNode.Pin(0, "Input", FlowNode.PinTypes.Input, 0)]
public class FlowNode_Sequence : FlowNode
{
  [SerializeField]
  private int m_Num = 1;
  [SerializeField]
  [HideInInspector]
  private FlowNode.Pin[] m_Pins = new FlowNode.Pin[0];

  public override void OnActivate(int pinID)
  {
    for (int index = 0; index < this.OutputLinks.Length; ++index)
      this.ActivateOutputLinks(this.OutputLinks[index].SrcPinID);
  }

  public override FlowNode.Pin[] GetDynamicPins()
  {
    return this.m_Pins;
  }
}
