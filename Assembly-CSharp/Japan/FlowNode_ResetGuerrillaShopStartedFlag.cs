﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ResetGuerrillaShopStartedFlag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using SRPG;

[FlowNode.NodeType("System/ResetGuerrillaShopStartedFlag", 32741)]
[FlowNode.Pin(0, "Reset", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1000, "Exit", FlowNode.PinTypes.Output, 1000)]
public class FlowNode_ResetGuerrillaShopStartedFlag : FlowNode
{
  public override void OnActivate(int pinID)
  {
    if (pinID != 0)
      return;
    MonoSingleton<GameManager>.Instance.Player.IsGuerrillaShopStarted = false;
    this.ActivateOutputLinks(1000);
  }
}
