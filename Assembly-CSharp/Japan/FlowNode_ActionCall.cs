﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ActionCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using UnityEngine;

[FlowNode.NodeType("UI/ActionCall", 32741)]
[FlowNode.Pin(10, "Input", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1, "Output", FlowNode.PinTypes.Output, 1)]
public class FlowNode_ActionCall : FlowNode
{
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;
  public ActionCall.EventType EventType;
  public SerializeValueList Value;

  public override void OnActivate(int pinID)
  {
    ActionCall component = (ActionCall) (!Object.op_Inequality((Object) this.Target, (Object) null) ? ((Component) this).get_gameObject() : this.Target).GetComponent<ActionCall>();
    if (Object.op_Inequality((Object) component, (Object) null))
      component.Invoke(this.EventType, this.Value);
    this.ActivateOutputLinks(1);
  }
}
