﻿// Decompiled with JetBrains decompiler
// Type: AdjustEventImageScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class AdjustEventImageScale : MonoBehaviour
{
  [SerializeField]
  private float RevisionScaleValue;

  public AdjustEventImageScale()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.SetRect();
  }

  private void SetRect()
  {
    Rect safeArea = SetCanvasBounds.GetSafeArea();
    if ((double) ((Rect) ref safeArea).get_width() >= (double) Screen.get_width())
      return;
    RectTransform component = (RectTransform) ((Component) this).get_gameObject().GetComponent<RectTransform>();
    float num = (float) (1.0 + (1.0 - (double) ((Rect) ref safeArea).get_width() / (double) Screen.get_width())) + this.RevisionScaleValue;
    Vector2 vector2;
    ((Vector2) ref vector2).\u002Ector((float) component.get_sizeDelta().x * num, (float) component.get_sizeDelta().y * num);
    component.set_sizeDelta(vector2);
  }
}
