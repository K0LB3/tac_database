﻿// Decompiled with JetBrains decompiler
// Type: MyMsgInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Globalization;
using System.Text.RegularExpressions;

public static class MyMsgInput
{
  public static bool isLegal(string name)
  {
    return string.IsNullOrEmpty(name) || new StringInfo(name).LengthInTextElements >= name.Length && !new Regex("^[0-9０-９\\-]+$").IsMatch(name) && !new Regex("^\\s+$").IsMatch(name);
  }
}
