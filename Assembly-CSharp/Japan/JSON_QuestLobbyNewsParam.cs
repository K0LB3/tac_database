﻿// Decompiled with JetBrains decompiler
// Type: JSON_QuestLobbyNewsParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

[Serializable]
public class JSON_QuestLobbyNewsParam
{
  public int category;
  public string begin_at;
  public string end_at;
  public int show_type;
}
