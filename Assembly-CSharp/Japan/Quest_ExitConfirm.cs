﻿// Decompiled with JetBrains decompiler
// Type: Quest_ExitConfirm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Drafts/Quest_ExitConfirm")]
public class Quest_ExitConfirm : UIDraft
{
  [UIDraft.AutoGenerated]
  public Text Text_Message;
  [UIDraft.AutoGenerated]
  public Button Btn_No;
  [UIDraft.AutoGenerated]
  public Button Btn_Yes;
}
