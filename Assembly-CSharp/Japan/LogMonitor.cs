﻿// Decompiled with JetBrains decompiler
// Type: LogMonitor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

[AddComponentMenu("")]
public class LogMonitor : MonoBehaviour
{
  public static LogMonitor mInstnace;
  private List<LogMonitor.Log> mLogs;
  private int mLogCount;
  private GUIStyle mBackgroundStyle;
  private GUIStyle mErrorStyle;
  private GUIStyle mExceptionStyle;
  private GUIStyle mStackTraceStyle;
  private string mStackTrace;
  private LogMonitor.GUICallback mCallback;
  private bool mDisp;
  private bool mSending;

  public LogMonitor()
  {
    base.\u002Ector();
  }

  public static void Start()
  {
    if (!GameUtility.IsDebugBuild || !UnityEngine.Object.op_Equality((UnityEngine.Object) LogMonitor.mInstnace, (UnityEngine.Object) null))
      return;
    LogMonitor.mInstnace = (LogMonitor) new GameObject(nameof (LogMonitor)).AddComponent<LogMonitor>();
  }

  public static LogMonitor Instance
  {
    get
    {
      return LogMonitor.mInstnace;
    }
  }

  public bool isDisp
  {
    get
    {
      return this.mDisp;
    }
  }

  public bool IsSend
  {
    get
    {
      return this.mSending;
    }
  }

  private void Awake()
  {
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) ((Component) this).get_gameObject());
    ((UnityEngine.Object) this).set_hideFlags((HideFlags) 61);
    this.SetDisp(true);
  }

  public void SetDisp(bool value)
  {
    this.mDisp = value;
  }

  private void OnEnable()
  {
    // ISSUE: method pointer
    GameUtility.RegisterLogCallback(new Application.LogCallback((object) this, __methodptr(HandleLog)));
  }

  private void OnDisable()
  {
    // ISSUE: method pointer
    GameUtility.UnregisterLogCallback(new Application.LogCallback((object) this, __methodptr(HandleLog)));
  }

  private void OnGUICallback()
  {
    if (this.mLogs.Count <= 0 || !this.mDisp)
      return;
    if (this.mErrorStyle == null)
    {
      this.mErrorStyle = new GUIStyle(GUI.get_skin().get_label());
      this.mErrorStyle.set_contentOffset(Vector2.op_Multiply(Vector2.get_right(), 5f));
      this.mErrorStyle.set_margin(new RectOffset(0, 0, 0, 0));
      this.mErrorStyle.set_fontSize(10);
      this.mErrorStyle.set_normal(new GUIStyleState());
      this.mErrorStyle.get_normal().set_textColor(Color.get_white());
      this.mErrorStyle.get_normal().set_background(new Texture2D(1, 1));
      this.mErrorStyle.get_normal().get_background().SetPixel(0, 0, new Color(0.2f, 0.2f, 0.2f));
      this.mErrorStyle.get_normal().get_background().Apply();
      this.mErrorStyle.set_hover(new GUIStyleState());
      this.mErrorStyle.get_hover().set_textColor(Color.get_white());
      this.mErrorStyle.get_hover().set_background(new Texture2D(1, 1));
      this.mErrorStyle.get_hover().get_background().SetPixel(0, 0, new Color(0.3f, 0.3f, 0.3f));
      this.mErrorStyle.get_hover().get_background().Apply();
    }
    if (this.mStackTraceStyle == null)
    {
      this.mStackTraceStyle = new GUIStyle(GUI.get_skin().get_label());
      this.mStackTraceStyle.set_contentOffset(Vector2.op_Multiply(Vector2.get_right(), 5f));
      this.mStackTraceStyle.set_margin(new RectOffset(0, 0, 0, 0));
      this.mStackTraceStyle.set_fontSize(10);
      this.mStackTraceStyle.set_normal(new GUIStyleState());
      this.mStackTraceStyle.get_normal().set_textColor(Color.get_white());
      this.mStackTraceStyle.get_normal().set_background(new Texture2D(1, 1));
      this.mStackTraceStyle.get_normal().get_background().SetPixel(0, 0, new Color(0.0f, 0.0f, 1f));
      this.mStackTraceStyle.get_normal().get_background().Apply();
    }
    if (this.mExceptionStyle == null)
    {
      this.mExceptionStyle = new GUIStyle(GUI.get_skin().get_label());
      this.mExceptionStyle.set_contentOffset(Vector2.op_Multiply(Vector2.get_right(), 5f));
      this.mExceptionStyle.set_margin(new RectOffset(0, 0, 0, 0));
      this.mExceptionStyle.set_fontSize(10);
      this.mExceptionStyle.set_normal(new GUIStyleState());
      this.mExceptionStyle.get_normal().set_textColor(Color.get_yellow());
      this.mExceptionStyle.get_normal().set_background(new Texture2D(1, 1));
      this.mExceptionStyle.get_normal().get_background().SetPixel(0, 0, new Color(0.5f, 0.0f, 0.0f));
      this.mExceptionStyle.get_normal().get_background().Apply();
      this.mExceptionStyle.set_hover(new GUIStyleState());
      this.mExceptionStyle.get_hover().set_textColor(Color.get_yellow());
      this.mExceptionStyle.get_hover().set_background(new Texture2D(1, 1));
      this.mExceptionStyle.get_hover().get_background().SetPixel(0, 0, new Color(0.6f, 0.0f, 0.0f));
      this.mExceptionStyle.get_hover().get_background().Apply();
    }
    if (this.mBackgroundStyle == null)
    {
      this.mBackgroundStyle = new GUIStyle(GUI.get_skin().get_label());
      this.mBackgroundStyle.set_stretchWidth(true);
      this.mBackgroundStyle.set_stretchHeight(true);
      this.mBackgroundStyle.get_normal().set_background(new Texture2D(1, 1));
      this.mBackgroundStyle.get_normal().get_background().SetPixel(0, 0, Color.get_black());
      this.mBackgroundStyle.get_normal().get_background().Apply();
      this.mBackgroundStyle.set_margin(new RectOffset(0, 0, 0, 0));
      this.mBackgroundStyle.set_padding(this.mBackgroundStyle.get_margin());
    }
    GUI.Box(new Rect(0.0f, 0.0f, (float) Screen.get_width(), 30f), string.Empty, this.mBackgroundStyle);
    if (GUI.Button(new Rect((float) (Screen.get_width() - 30), 0.0f, 30f, 30f), "X"))
    {
      this.Clear();
    }
    else
    {
      GUILayout.BeginVertical(new GUILayoutOption[0]);
      for (int index = 0; index < this.mLogs.Count; ++index)
      {
        if (GUILayout.Button("#" + (object) this.mLogs[index].index + " " + this.mLogs[index].logString, this.mLogs[index].type != null ? this.mExceptionStyle : this.mErrorStyle, new GUILayoutOption[1]
        {
          GUILayout.Width((float) (Screen.get_width() - 30))
        }))
        {
          this.mStackTrace = this.mLogs[index].stackTrace;
          this.mSending = false;
        }
      }
      if (!string.IsNullOrEmpty(this.mStackTrace))
      {
        GUILayout.Box(string.Empty, this.mBackgroundStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(4f)
        });
        GUILayout.Label(this.mStackTrace, this.mStackTraceStyle, new GUILayoutOption[1]
        {
          GUILayout.Width((float) (Screen.get_width() - 30))
        });
      }
      GUILayout.Box(string.Empty, this.mBackgroundStyle, new GUILayoutOption[1]
      {
        GUILayout.Height(8f)
      });
      GUILayout.BeginHorizontal(new GUILayoutOption[0]);
      for (int index = 0; index < this.mLogs.Count; ++index)
      {
        if (GUILayout.Button("#" + (object) this.mLogs[index].index, new GUILayoutOption[2]
        {
          GUILayout.Height(40f),
          GUILayout.Width(40f)
        }))
          this.mStackTrace = this.mLogs[index].stackTrace;
      }
      if (!string.IsNullOrEmpty(this.mStackTrace))
      {
        GUI.set_enabled(!this.mSending);
        if (GUILayout.Button("HipChatに\nスクリーンショット送信", new GUILayoutOption[2]
        {
          GUILayout.Width(160f),
          GUILayout.Height(40f)
        }))
          this.SendHipChat();
        GUI.set_enabled(true);
      }
      GUILayout.EndHorizontal();
      GUILayout.EndVertical();
    }
  }

  private void HandleLog(string logString, string stackTrace, LogType type)
  {
    if (type != 4 && type != null || type == null && logString.StartsWith("Asynchronous Background loading is only supported in Unity Pro."))
      return;
    LogMonitor.Log log;
    if (this.mLogs.Count > 5)
    {
      log = this.mLogs[0];
      this.mLogs.RemoveAt(0);
    }
    else
      log = new LogMonitor.Log();
    log.type = type;
    log.logString = logString;
    log.stackTrace = stackTrace;
    log.index = ++this.mLogCount;
    this.mLogs.Add(log);
    if (!UnityEngine.Object.op_Equality((UnityEngine.Object) this.mCallback, (UnityEngine.Object) null))
      return;
    this.mCallback = (LogMonitor.GUICallback) new GameObject("callback", new System.Type[2]
    {
      typeof (GameObject),
      typeof (LogMonitor.GUICallback)
    }).GetComponent<LogMonitor.GUICallback>();
    this.mCallback.OnGUIListener = new LogMonitor.GUICallback.GUIEvent(this.OnGUICallback);
    ((Component) this.mCallback).get_transform().SetParent(((Component) this).get_transform(), false);
  }

  public void Clear()
  {
    this.mLogs.Clear();
    this.mStackTrace = (string) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) this.mCallback).get_gameObject());
    this.mCallback = (LogMonitor.GUICallback) null;
  }

  public bool SendHipChat()
  {
    if (this.mSending)
      return false;
    DateTime localTime = DateTime.UtcNow.ToLocalTime();
    this.StartCoroutine(this.PostHipchat(string.Empty + AppPath.persistentDataPath + "/" + (object) localTime.Year + "-" + (object) localTime.Month + "-" + (object) localTime.Day + "-" + (object) localTime.Hour + "-" + (object) localTime.Minute + "-" + (object) localTime.Second + "-" + (object) localTime.Millisecond + ".PNG"));
    return true;
  }

  [DebuggerHidden]
  private IEnumerator PostHipchat(string filename)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    LogMonitor.\u003CPostHipchat\u003Ec__Iterator0 hipchatCIterator0 = new LogMonitor.\u003CPostHipchat\u003Ec__Iterator0();
    return (IEnumerator) hipchatCIterator0;
  }

  private void SaveScreeShot(string filename)
  {
    Texture2D texture2D = new Texture2D(Screen.get_width(), Screen.get_height(), (TextureFormat) 3, false);
    texture2D.ReadPixels(new Rect(0.0f, 0.0f, (float) Screen.get_width(), (float) Screen.get_height()), 0, 0);
    texture2D.Apply();
    byte[] png = texture2D.EncodeToPNG();
    UnityEngine.Object.Destroy((UnityEngine.Object) texture2D);
    File.WriteAllBytes(filename, png);
  }

  public void DirectLog(string logString)
  {
    this.HandleLog(logString, (string) null, (LogType) 0);
  }

  private class Log
  {
    public int index;
    public string logString;
    public string stackTrace;
    public LogType type;
  }

  [Serializable]
  public class ErrorInfo
  {
    public string appver;
    public string dlcver;
    public string netver;
    public string host;
    public string code;
    public string uid;
    public string name;
    public string use_info;
  }

  [Serializable]
  public class NotificationMessage
  {
    public string message;
  }

  [AddComponentMenu("")]
  public class GUICallback : MonoBehaviour
  {
    public LogMonitor.GUICallback.GUIEvent OnGUIListener;

    public GUICallback()
    {
      base.\u002Ector();
    }

    private void OnGUI()
    {
      this.OnGUIListener();
    }

    public delegate void GUIEvent();
  }
}
