﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.ReportProgress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace GooglePlayGames
{
  internal delegate void ReportProgress(string id, double progress, Action<bool> callback);
}
