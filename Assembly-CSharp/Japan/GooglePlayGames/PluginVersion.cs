﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.PluginVersion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace GooglePlayGames
{
  public class PluginVersion
  {
    public const string VersionKeyCPP = "00911";
    public const string VersionKeyU5 = "00915";
    public const int VersionInt = 2342;
    public const string VersionString = "0.9.26";
    public const string VersionKey = "00926";
    public const int MinGmsCoreVersionCode = 8115000;
  }
}
