﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.Quests.QuestState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi.Quests
{
  public enum QuestState
  {
    Upcoming = 1,
    Open = 2,
    Accepted = 3,
    Completed = 4,
    Expired = 5,
    Failed = 6,
  }
}
