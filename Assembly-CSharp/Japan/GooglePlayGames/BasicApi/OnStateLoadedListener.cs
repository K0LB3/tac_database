﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.OnStateLoadedListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi
{
  public interface OnStateLoadedListener
  {
    void OnStateLoaded(bool success, int slot, byte[] data);

    byte[] OnStateConflict(int slot, byte[] localData, byte[] serverData);

    void OnStateSaved(bool success, int slot);
  }
}
