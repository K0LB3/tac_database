﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.UIStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi
{
  public enum UIStatus
  {
    LeftRoom = -18, // -0x00000012
    UiBusy = -12, // -0x0000000C
    UserClosedUI = -6,
    Timeout = -5,
    VersionUpdateRequired = -4,
    NotAuthorized = -3,
    InternalError = -2,
    Valid = 1,
  }
}
