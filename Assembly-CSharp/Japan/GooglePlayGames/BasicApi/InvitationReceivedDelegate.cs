﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.InvitationReceivedDelegate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GooglePlayGames.BasicApi.Multiplayer;

namespace GooglePlayGames.BasicApi
{
  public delegate void InvitationReceivedDelegate(Invitation invitation, bool shouldAutoAccept);
}
