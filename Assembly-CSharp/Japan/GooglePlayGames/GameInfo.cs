﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.GameInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace GooglePlayGames
{
  public static class GameInfo
  {
    private const string UnescapedApplicationId = "APP_ID";
    private const string UnescapedIosClientId = "IOS_CLIENTID";
    private const string UnescapedWebClientId = "WEB_CLIENTID";
    private const string UnescapedNearbyServiceId = "NEARBY_SERVICE_ID";
    public const string ApplicationId = "__APP_ID__";
    public const string IosClientId = "__IOS_CLIENTID__";
    public const string WebClientId = "__WEB_CLIENTID__";
    public const string NearbyConnectionServiceId = "__NEARBY_SERVICE_ID__";

    public static bool ApplicationIdInitialized()
    {
      if (!string.IsNullOrEmpty("__APP_ID__"))
        return !"__APP_ID__".Equals(GameInfo.ToEscapedToken("APP_ID"));
      return false;
    }

    public static bool IosClientIdInitialized()
    {
      if (!string.IsNullOrEmpty("__IOS_CLIENTID__"))
        return !"__IOS_CLIENTID__".Equals(GameInfo.ToEscapedToken("IOS_CLIENTID"));
      return false;
    }

    public static bool WebClientIdInitialized()
    {
      if (!string.IsNullOrEmpty("__WEB_CLIENTID__"))
        return !"__WEB_CLIENTID__".Equals(GameInfo.ToEscapedToken("WEB_CLIENTID"));
      return false;
    }

    public static bool NearbyConnectionsInitialized()
    {
      if (!string.IsNullOrEmpty("__NEARBY_SERVICE_ID__"))
        return !"__NEARBY_SERVICE_ID__".Equals(GameInfo.ToEscapedToken("NEARBY_SERVICE_ID"));
      return false;
    }

    private static string ToEscapedToken(string token)
    {
      return string.Format("__{0}__", (object) token);
    }
  }
}
