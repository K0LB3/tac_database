﻿// Decompiled with JetBrains decompiler
// Type: EventCoinListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class EventCoinListItem : MonoBehaviour
{
  [SerializeField]
  private GameObject button;

  public EventCoinListItem()
  {
    base.\u002Ector();
  }

  public ListItemEvents listItemEvents
  {
    get
    {
      return (ListItemEvents) ((Component) this).GetComponent<ListItemEvents>();
    }
  }

  public GameObject Button
  {
    get
    {
      return this.button;
    }
  }

  public void Set(bool isPeriod, bool isRead, long post_at, long read)
  {
    if (isRead)
      this.button.get_gameObject().SetActive(false);
    else
      this.button.get_gameObject().SetActive(true);
  }
}
