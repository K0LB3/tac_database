﻿// Decompiled with JetBrains decompiler
// Type: StaticLightVolume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class StaticLightVolume : MonoBehaviour
{
  public static List<StaticLightVolume> Volumes = new List<StaticLightVolume>();
  [NonSerialized]
  public Bounds Bounds;
  [Range(0.0f, 1f)]
  public float AmbientLitToDirectLit;
  [Range(0.0f, 1f)]
  public float AmbientLitToIndirectLit;
  [Range(0.0f, 1f)]
  public float PointLitToDirectLit;
  [Range(0.0f, 1f)]
  public float PointLitToIndirectLit;
  [Range(0.1f, 4f)]
  public float DirectLightingScale;
  [Range(0.1f, 4f)]
  public float IndirectLightingScale;
  [SerializeField]
  [HideInInspector]
  private StaticLightVolume.LightProbe[] mVoxel;
  [Range(1f, 16f)]
  public int XSize;
  [Range(1f, 16f)]
  public int YSize;
  [Range(1f, 16f)]
  public int ZSize;
  private const float GizmoColorR = 0.2f;
  private const float GizmoColorG = 0.6f;
  private const float GizmoColorB = 0.6f;

  public StaticLightVolume()
  {
    base.\u002Ector();
  }

  public static StaticLightVolume FindVolume(Vector3 pos)
  {
    for (int index = StaticLightVolume.Volumes.Count - 1; index >= 0; --index)
    {
      if (((Behaviour) StaticLightVolume.Volumes[index]).get_isActiveAndEnabled() && ((Bounds) ref StaticLightVolume.Volumes[index].Bounds).Contains(pos))
        return StaticLightVolume.Volumes[index];
    }
    return (StaticLightVolume) null;
  }

  private Bounds CalcBounds()
  {
    Transform transform = ((Component) this).get_transform();
    return new Bounds(transform.get_position(), transform.get_localScale());
  }

  private void OnEnable()
  {
    StaticLightVolume.Volumes.Add(this);
    this.Bounds = this.CalcBounds();
  }

  private void OnDisable()
  {
    StaticLightVolume.Volumes.Remove(this);
  }

  public void CalcLightColor(Vector3 position, out Color directLit, out Color indirectLit)
  {
    position = Vector3.Max(position, ((Bounds) ref this.Bounds).get_min());
    position = Vector3.Min(position, ((Bounds) ref this.Bounds).get_max());
    float num1 = (float) ((Bounds) ref this.Bounds).get_size().x / (float) this.XSize;
    float num2 = (float) ((Bounds) ref this.Bounds).get_size().y / (float) this.YSize;
    float num3 = (float) ((Bounds) ref this.Bounds).get_size().z / (float) this.ZSize;
    Vector3 vector3 = Vector3.op_Subtraction(Vector3.op_Subtraction(position, ((Bounds) ref this.Bounds).get_min()), Vector3.op_Multiply(Vector3.get_one(), 0.5f));
    int num4 = Mathf.Clamp(Mathf.FloorToInt((float) vector3.x / num1), 0, this.XSize - 1);
    int num5 = Mathf.Clamp(Mathf.FloorToInt((float) vector3.y / num2), 0, this.YSize - 1);
    int num6 = Mathf.Clamp(Mathf.FloorToInt((float) vector3.z / num3), 0, this.ZSize - 1);
    int num7 = Mathf.Min(num4 + 1, this.XSize - 1);
    int num8 = Mathf.Min(num5 + 1, this.YSize - 1);
    int num9 = Mathf.Min(num6 + 1, this.ZSize - 1);
    float num10 = (float) vector3.x % num1 / num1;
    float num11 = (float) vector3.y % num2 / num2;
    float num12 = (float) vector3.z % num3 / num3;
    int num13 = this.XSize * this.YSize;
    int index1 = num4 + this.XSize * num5 + num13 * num6;
    int index2 = num7 + this.XSize * num8 + num13 * num6;
    int index3 = num4 + this.XSize * num5 + num13 * num6;
    int index4 = num7 + this.XSize * num8 + num13 * num6;
    int index5 = num4 + this.XSize * num5 + num13 * num9;
    int index6 = num7 + this.XSize * num8 + num13 * num9;
    int index7 = num4 + this.XSize * num5 + num13 * num9;
    int index8 = num7 + this.XSize * num8 + num13 * num9;
    directLit = Color.Lerp(Color.Lerp(Color.Lerp(Color32.op_Implicit(this.mVoxel[index1].DirectLightColor), Color32.op_Implicit(this.mVoxel[index2].DirectLightColor), num10), Color.Lerp(Color32.op_Implicit(this.mVoxel[index3].DirectLightColor), Color32.op_Implicit(this.mVoxel[index4].DirectLightColor), num10), num11), Color.Lerp(Color.Lerp(Color32.op_Implicit(this.mVoxel[index5].DirectLightColor), Color32.op_Implicit(this.mVoxel[index6].DirectLightColor), num10), Color.Lerp(Color32.op_Implicit(this.mVoxel[index7].DirectLightColor), Color32.op_Implicit(this.mVoxel[index8].DirectLightColor), num10), num11), num12);
    indirectLit = Color.Lerp(Color.Lerp(Color.Lerp(Color32.op_Implicit(this.mVoxel[index1].IndirectLightColor), Color32.op_Implicit(this.mVoxel[index2].IndirectLightColor), num10), Color.Lerp(Color32.op_Implicit(this.mVoxel[index3].IndirectLightColor), Color32.op_Implicit(this.mVoxel[index4].IndirectLightColor), num10), num11), Color.Lerp(Color.Lerp(Color32.op_Implicit(this.mVoxel[index5].IndirectLightColor), Color32.op_Implicit(this.mVoxel[index6].IndirectLightColor), num10), Color.Lerp(Color32.op_Implicit(this.mVoxel[index7].IndirectLightColor), Color32.op_Implicit(this.mVoxel[index8].IndirectLightColor), num10), num11), num12);
  }

  private void OnValidate()
  {
    int length = this.XSize * this.YSize * this.ZSize;
    if (this.mVoxel.Length == length)
      return;
    this.mVoxel = new StaticLightVolume.LightProbe[length];
    for (int index1 = 0; index1 < this.XSize; ++index1)
    {
      for (int index2 = 0; index2 < this.YSize; ++index2)
      {
        for (int index3 = 0; index3 < this.ZSize; ++index3)
        {
          int index4 = index1 + index2 * this.XSize + index3 * this.XSize * this.YSize;
          this.mVoxel[index4] = new StaticLightVolume.LightProbe();
          this.mVoxel[index4].DirectLightColor = Color32.op_Implicit(Color.get_black());
          this.mVoxel[index4].IndirectLightColor = Color32.op_Implicit(Color.get_black());
        }
      }
    }
  }

  private Vector3 CalcCenter(int x, int y, int z)
  {
    return new Vector3(Mathf.Lerp((float) ((Bounds) ref this.Bounds).get_min().x, (float) ((Bounds) ref this.Bounds).get_max().x, ((float) x + 0.5f) / (float) this.XSize), Mathf.Lerp((float) ((Bounds) ref this.Bounds).get_min().y, (float) ((Bounds) ref this.Bounds).get_max().y, ((float) y + 0.5f) / (float) this.YSize), Mathf.Lerp((float) ((Bounds) ref this.Bounds).get_min().z, (float) ((Bounds) ref this.Bounds).get_max().z, ((float) z + 0.5f) / (float) this.ZSize));
  }

  public void Bake()
  {
    Light[] objectsOfType = (Light[]) Object.FindObjectsOfType<Light>();
    Light[] all1 = Array.FindAll<Light>(objectsOfType, (Predicate<Light>) (lit => lit.get_type() == 1));
    Light[] all2 = Array.FindAll<Light>(objectsOfType, (Predicate<Light>) (lit => lit.get_type() == 2));
    Color color1 = Color.get_black();
    foreach (Light light in all1)
      color1 = Color.op_Addition(color1, Color.op_Multiply(light.get_color(), light.get_intensity()));
    Color color2 = Color.op_Multiply(color1, this.DirectLightingScale);
    color2.a = (__Null) 1.0;
    foreach (ColorBlendVolume volume in ColorBlendVolume.Volumes)
      volume.UpdateBounds();
    for (int x = 0; x < this.XSize; ++x)
    {
      for (int y = 0; y < this.YSize; ++y)
      {
        for (int z = 0; z < this.ZSize; ++z)
        {
          Vector3 vector3_1 = this.CalcCenter(x, y, z);
          Color color3;
          ((Color) ref color3).\u002Ector(0.0f, 0.0f, 0.0f);
          Color color4;
          ((Color) ref color4).\u002Ector(0.0f, 0.0f, 0.0f);
          int index1 = x + y * this.XSize + z * this.XSize * this.YSize;
          AmbientLightSettings volume1 = AmbientLightSettings.FindVolume(vector3_1);
          color3 = Color.op_Addition(color2, Color.op_Multiply(volume1.AmbientLightColor, this.AmbientLitToDirectLit));
          for (int index2 = 0; index2 < all2.Length; ++index2)
          {
            Vector3 vector3_2 = Vector3.op_Subtraction(vector3_1, ((Component) all2[index2]).get_transform().get_position());
            float num = Mathf.Clamp01((float) (1.0 - (double) ((Vector3) ref vector3_2).get_magnitude() / (double) all2[index2].get_range())) * all2[index2].get_intensity();
            ref Color local1 = ref color3;
            local1.r = (__Null) (local1.r + all2[index2].get_color().r * (double) num * (double) this.PointLitToDirectLit);
            ref Color local2 = ref color3;
            local2.g = (__Null) (local2.g + all2[index2].get_color().g * (double) num * (double) this.PointLitToDirectLit);
            ref Color local3 = ref color3;
            local3.b = (__Null) (local3.b + all2[index2].get_color().b * (double) num * (double) this.PointLitToDirectLit);
            ref Color local4 = ref color4;
            local4.r = (__Null) (local4.r + all2[index2].get_color().r * (double) num * (double) this.PointLitToIndirectLit);
            ref Color local5 = ref color4;
            local5.g = (__Null) (local5.g + all2[index2].get_color().g * (double) num * (double) this.PointLitToIndirectLit);
            ref Color local6 = ref color4;
            local6.b = (__Null) (local6.b + all2[index2].get_color().b * (double) num * (double) this.PointLitToIndirectLit);
          }
          color4 = Color.op_Multiply(color4, this.IndirectLightingScale);
          color4 = Color.op_Addition(color4, Color.op_Multiply(volume1.AmbientLightColor, this.AmbientLitToIndirectLit));
          ColorBlendVolume volume2 = ColorBlendVolume.FindVolume(vector3_1);
          if (Object.op_Inequality((Object) volume2, (Object) null))
          {
            color3 = Color.op_Multiply(color3, Color32.op_Implicit(volume2.Color));
            color4 = Color.op_Multiply(color4, Color32.op_Implicit(volume2.Color));
          }
          color3.a = (__Null) 1.0;
          color4.a = (__Null) 1.0;
          this.mVoxel[index1].DirectLightColor = Color32.op_Implicit(color3);
          this.mVoxel[index1].IndirectLightColor = Color32.op_Implicit(color4);
        }
      }
    }
  }

  [Serializable]
  public struct LightProbe
  {
    public Color32 DirectLightColor;
    public Color32 IndirectLightColor;
  }
}
