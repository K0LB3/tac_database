﻿// Decompiled with JetBrains decompiler
// Type: UnityWeakReference`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;
using UnityEngine;

internal class UnityWeakReference<T> : WeakReference where T : Object
{
  public UnityWeakReference(T target)
    : base((object) target)
  {
  }

  public override bool IsAlive
  {
    get
    {
      return Object.op_Inequality((Object) (object) this.Target, (Object) null);
    }
  }

  public T Target
  {
    get
    {
      return base.Target as T;
    }
    set
    {
      this.Target = (object) value;
    }
  }

  public bool TryGetTarget(out T target)
  {
    target = base.Target as T;
    return Object.op_Inequality((Object) (object) target, (Object) null);
  }
}
