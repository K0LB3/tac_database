﻿// Decompiled with JetBrains decompiler
// Type: MaterialEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class MaterialEffect : MonoBehaviour
{
  public Material Material;

  public MaterialEffect()
  {
    base.\u002Ector();
  }

  private void OnRenderImage(RenderTexture src, RenderTexture dest)
  {
    if (Object.op_Inequality((Object) this.Material, (Object) null))
      Graphics.Blit((Texture) src, dest, this.Material);
    else
      Graphics.Blit((Texture) src, dest);
  }
}
