﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_SetText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("UI/SetText", 32741)]
[FlowNode.Pin(1, "Set", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(100, "Out", FlowNode.PinTypes.Output, 0)]
public class FlowNode_SetText : FlowNode
{
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (UnityEngine.UI.Text), true)]
  public UnityEngine.UI.Text Target;
  public string Text;

  public override void OnActivate(int pinID)
  {
    if (pinID != 1)
      return;
    if (Object.op_Inequality((Object) this.Target, (Object) null))
      this.Target.set_text(this.Text);
    this.ActivateOutputLinks(100);
  }
}
