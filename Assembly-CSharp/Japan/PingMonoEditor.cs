﻿// Decompiled with JetBrains decompiler
// Type: PingMonoEditor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using ExitGames.Client.Photon;
using System;
using System.Net.Sockets;
using UnityEngine;

public class PingMonoEditor : PhotonPing
{
  private Socket sock;

  public PingMonoEditor()
  {
    base.\u002Ector();
  }

  public virtual bool StartPing(string ip)
  {
    this.Init();
    try
    {
      this.sock = !ip.Contains(".") ? new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp) : new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
      this.sock.ReceiveTimeout = 5000;
      this.sock.Connect(ip, 5055);
      this.PingBytes[this.PingBytes.Length - 1] = (object) this.PingId;
      this.sock.Send((byte[]) this.PingBytes);
      this.PingBytes[this.PingBytes.Length - 1] = (object) (int) (byte) (this.PingId - 1);
    }
    catch (Exception ex)
    {
      this.sock = (Socket) null;
      Console.WriteLine((object) ex);
    }
    return false;
  }

  public virtual bool Done()
  {
    if (this.GotResult != null || this.sock == null)
      return true;
    if (this.sock.Available <= 0)
      return false;
    int num = this.sock.Receive((byte[]) this.PingBytes, SocketFlags.None);
    if ((int) (byte) this.PingBytes[this.PingBytes.Length - 1] != this.PingId || num != this.PingLength)
      Debug.Log((object) "ReplyMatch is false! ");
    this.Successful = num != this.PingBytes.Length ? (__Null) 0 : (__Null) ((int) (byte) this.PingBytes[this.PingBytes.Length - 1] == this.PingId ? 1 : 0);
    this.GotResult = (__Null) 1;
    return true;
  }

  public virtual void Dispose()
  {
    try
    {
      this.sock.Close();
    }
    catch
    {
    }
    this.sock = (Socket) null;
  }
}
