﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.IDevice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace Gsc.Auth
{
  public interface IDevice
  {
    bool initialized { get; }

    bool hasError { get; }

    string Platform { get; }
  }
}
