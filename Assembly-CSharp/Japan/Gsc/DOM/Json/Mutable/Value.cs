﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Mutable.Value
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Gsc.Network;
using System.Collections;
using System.Runtime.InteropServices;

namespace Gsc.DOM.Json.Mutable
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  public struct Value
  {
    public void SetNull()
    {
    }

    public void SetObject()
    {
    }

    public void SetArray()
    {
    }

    public void Set(IRequestObject value)
    {
    }

    public void Set(IEnumerable value)
    {
    }

    public void Set(bool value)
    {
    }

    public void Set(string value)
    {
    }

    public void Set(int value)
    {
    }

    public void Set(uint value)
    {
    }

    public void Set(long value)
    {
    }

    public void Set(ulong value)
    {
    }

    public void Set(float value)
    {
    }

    public void Set(double value)
    {
    }
  }
}
