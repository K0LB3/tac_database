﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Member
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace Gsc.DOM.Json
{
  public struct Member : IMember
  {
    private readonly string name;
    private readonly Value value;

    public Member(string name, Value value)
    {
      this.name = name;
      this.value = value;
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public Value Value
    {
      get
      {
        return this.value;
      }
    }

    IValue IMember.Value
    {
      get
      {
        return (IValue) this.value;
      }
    }
  }
}
