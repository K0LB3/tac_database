﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.IPurchaseFlowImpl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace Gsc.Purchase
{
  public interface IPurchaseFlowImpl
  {
    void Init(string[] productIds);

    void UpdateProducts(string[] productIds);

    void Resume();

    bool Confirmed();

    bool Purchase(ProductInfo product);

    void Consume(string transactionId);
  }
}
