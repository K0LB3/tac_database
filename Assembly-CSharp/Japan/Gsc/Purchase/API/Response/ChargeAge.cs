﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.API.Response.ChargeAge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Gsc.DOM;
using Gsc.Network;
using Gsc.Purchase.API.App;

namespace Gsc.Purchase.API.Response
{
  public class ChargeAge : GenericResponse<ChargeAge>
  {
    public ChargeAge(WebInternalResponse response)
    {
      using (IDocument document = this.Parse(response))
        this.Age = document.Root["age"].ToInt();
    }

    public int Age { get; private set; }
  }
}
