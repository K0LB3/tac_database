﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.API.Response.Fulfillment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Gsc.DOM;
using Gsc.Network;
using Gsc.Purchase.API.App;

namespace Gsc.Purchase.API.Response
{
  public class Fulfillment : GenericResponse<Fulfillment>
  {
    public Fulfillment(WebInternalResponse response)
    {
      using (IDocument document = this.Parse(response))
        this.Result = new FulfillmentResult(document.Root.GetObject());
    }

    public FulfillmentResult Result { get; private set; }
  }
}
