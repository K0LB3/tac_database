﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.IWebTask`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Gsc.Tasks;
using System.Collections;

namespace Gsc.Network
{
  public interface IWebTask<TRequest, TResponse> : IWebTask<TResponse>, IWebTask, IWebTaskBase, ITask, IEnumerator where TRequest : IRequest<TRequest, TResponse> where TResponse : IResponse<TResponse>
  {
    TRequest Request { get; }
  }
}
