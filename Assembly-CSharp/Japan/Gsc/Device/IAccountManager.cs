﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Device.IAccountManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace Gsc.Device
{
  public interface IAccountManager
  {
    string GetSecretKey(string name);

    string GetDeviceId(string name);

    void SetKeyPair(string name, string secretKey, string deviceId);

    void SetDeviceId(string name, string deviceId);

    void Remove(string name);

    void Reset();
  }
}
