﻿// Decompiled with JetBrains decompiler
// Type: Gsc.App.NetworkHelper.BlockRequest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Gsc.Network;

namespace Gsc.App.NetworkHelper
{
  public static class BlockRequest
  {
    public static BlockRequest<TRequest, TResponse> Create<TRequest, TResponse>(IRequest<TRequest, TResponse> request) where TRequest : IRequest<TRequest, TResponse> where TResponse : IResponse<TResponse>
    {
      return new BlockRequest<TRequest, TResponse>(WebInternalTask.Create<TRequest, TResponse>(request));
    }
  }
}
