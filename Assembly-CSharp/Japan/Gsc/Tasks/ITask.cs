﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Tasks.ITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections;

namespace Gsc.Tasks
{
  public interface ITask
  {
    bool isDone { get; }

    void OnStart();

    IEnumerator Run();

    void OnFinish();
  }
}
