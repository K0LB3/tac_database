﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Core.NativeRootObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace Gsc.Core
{
  public class NativeRootObject : MonoBehaviour
  {
    private static NativeRootObject _instance;

    public NativeRootObject()
    {
      base.\u002Ector();
    }

    public static NativeRootObject Instance
    {
      get
      {
        if (Object.op_Equality((Object) NativeRootObject._instance, (Object) null))
        {
          GameObject gameObject = new GameObject("GSCC.NativeRootObject");
          ((Object) gameObject).set_hideFlags((HideFlags) 61);
          Object.DontDestroyOnLoad((Object) gameObject);
          gameObject.AddComponent<NativeRootObject>();
        }
        return NativeRootObject._instance;
      }
    }

    private void Awake()
    {
      NativeRootObject._instance = this;
    }
  }
}
