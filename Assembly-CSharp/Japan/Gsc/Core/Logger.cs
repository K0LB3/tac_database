﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Core.Logger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using Gsc.Network;
using System;
using System.Threading;
using UnityEngine;

namespace Gsc.Core
{
  public class Logger
  {
    private static bool initialized;

    public static event Application.LogCallback Callback
    {
      add
      {
        Application.LogCallback comparand = Logger.Callback;
        Application.LogCallback logCallback;
        do
        {
          logCallback = comparand;
          comparand = Interlocked.CompareExchange<Application.LogCallback>(ref Logger.Callback, (Application.LogCallback) Delegate.Combine((Delegate) logCallback, (Delegate) value), comparand);
        }
        while (comparand != logCallback);
      }
      remove
      {
        Application.LogCallback comparand = Logger.Callback;
        Application.LogCallback logCallback;
        do
        {
          logCallback = comparand;
          comparand = Interlocked.CompareExchange<Application.LogCallback>(ref Logger.Callback, (Application.LogCallback) Delegate.Remove((Delegate) logCallback, (Delegate) value), comparand);
        }
        while (comparand != logCallback);
      }
    }

    public static void Init()
    {
      if (Logger.initialized)
        return;
      // ISSUE: reference to a compiler-generated field
      if (Logger.\u003C\u003Ef__mg\u0024cache0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: method pointer
        Logger.\u003C\u003Ef__mg\u0024cache0 = new Application.LogCallback((object) null, __methodptr(_HandleLog));
      }
      // ISSUE: reference to a compiler-generated field
      Application.remove_logMessageReceived(Logger.\u003C\u003Ef__mg\u0024cache0);
      // ISSUE: reference to a compiler-generated field
      if (Logger.\u003C\u003Ef__mg\u0024cache1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: method pointer
        Logger.\u003C\u003Ef__mg\u0024cache1 = new Application.LogCallback((object) null, __methodptr(_HandleLog));
      }
      // ISSUE: reference to a compiler-generated field
      Application.add_logMessageReceived(Logger.\u003C\u003Ef__mg\u0024cache1);
      Logger.initialized = true;
    }

    public static void HandleLog(string logMessage, string stackTrace, LogType logType)
    {
      if (logType != 1 && logType != 4 && logType != null)
        return;
      UnityErrorLogSender.Instance.Send(logMessage, stackTrace, logType);
    }

    private static void _HandleLog(string logMessage, string stackTrace, LogType logType)
    {
      Logger.HandleLog(logMessage, stackTrace, logType);
      // ISSUE: reference to a compiler-generated field
      if (Logger.Callback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      Logger.Callback.Invoke(logMessage, stackTrace, logType);
    }
  }
}
