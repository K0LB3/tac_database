﻿// Decompiled with JetBrains decompiler
// Type: GemParticleHitEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class GemParticleHitEffect : MonoBehaviour
{
  private Vector3 mStartPosition;
  public static bool IsEnable;
  public GameObject EffectPrefab;

  public GemParticleHitEffect()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.mStartPosition = ((Component) this).get_transform().get_position();
  }

  private void Update()
  {
    if (!GemParticleHitEffect.IsEnable || Object.op_Equality((Object) this.EffectPrefab, (Object) null))
      return;
    GemParticle component = (GemParticle) ((Component) this).get_gameObject().GetComponent<GemParticle>();
    if (!Object.op_Inequality((Object) component, (Object) null) || !Object.op_Inequality((Object) component.TargetObject, (Object) null))
      return;
    Vector3 vector3_1 = Vector3.op_Addition(component.TargetObject.get_position(), component.TargetOffset);
    Vector3 vector3_2 = Vector3.op_Subtraction(vector3_1, this.mStartPosition);
    float magnitude = ((Vector3) ref vector3_2).get_magnitude();
    Vector3 vector3_3 = Vector3.op_Subtraction(vector3_1, ((Component) this).get_transform().get_position());
    if (0.200000002980232 <= (double) ((Vector3) ref vector3_3).get_magnitude() / (double) magnitude)
      return;
    GameUtility.RequireComponent<OneShotParticle>((GameObject) Object.Instantiate<GameObject>((M0) this.EffectPrefab, vector3_1, Quaternion.get_identity()));
    GemParticleHitEffect.IsEnable = false;
  }

  private void OnDisable()
  {
    GemParticleHitEffect.IsEnable = false;
    Object.Destroy((Object) this);
  }
}
