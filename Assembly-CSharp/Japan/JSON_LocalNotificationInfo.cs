﻿// Decompiled with JetBrains decompiler
// Type: JSON_LocalNotificationInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public class JSON_LocalNotificationInfo
{
  public int pk;
  public JSON_LocalNotificationInfo.Fields fields;

  public class Fields
  {
    public int id;
    public string trophy_iname;
    public int push_flg;
    public string push_word;
  }
}
