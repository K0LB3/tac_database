﻿// Decompiled with JetBrains decompiler
// Type: FixLineSpacing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Text))]
public class FixLineSpacing : MonoBehaviour
{
  public bool NoBestFit;

  public FixLineSpacing()
  {
    base.\u002Ector();
  }

  private void OnEnable()
  {
  }

  private void Awake()
  {
    if (!((Behaviour) this).get_enabled())
      return;
    Text component = (Text) ((Component) this).GetComponent<Text>();
    Text text = component;
    text.set_lineSpacing(text.get_lineSpacing() * 2f);
    if (this.NoBestFit)
      component.set_resizeTextForBestFit(false);
    ((Behaviour) this).set_enabled(false);
  }
}
