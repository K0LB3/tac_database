﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_AnimatorSetNormalizedTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[FlowNode.NodeType("Animator/Set Normalized Time", 32741)]
[FlowNode.Pin(10, "Input", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1, "Output", FlowNode.PinTypes.Output, 1)]
public class FlowNode_AnimatorSetNormalizedTime : FlowNode
{
  [FlowNode.ShowInInfo]
  public float NormalizedTime;
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;
  public bool UpdateAnimator;

  public override void OnActivate(int pinID)
  {
    Animator component = (Animator) (!Object.op_Inequality((Object) this.Target, (Object) null) ? ((Component) this).get_gameObject() : this.Target).GetComponent<Animator>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      AnimatorStateInfo animatorStateInfo = component.GetCurrentAnimatorStateInfo(0);
      component.Play(((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash(), 0, this.NormalizedTime);
      if (this.UpdateAnimator)
        component.Update(0.0f);
    }
    this.ActivateOutputLinks(1);
  }
}
