﻿// Decompiled with JetBrains decompiler
// Type: ProjectorShadow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("Scripts/ProjectorShadow")]
public class ProjectorShadow : MonoBehaviour
{
  public ProjectorShadow()
  {
    base.\u002Ector();
  }

  private void Start()
  {
  }

  public void Initialize()
  {
  }

  public void Release()
  {
  }

  public void Update()
  {
  }

  private void SetZOffset(float factor, float unit)
  {
    Projector component = (Projector) ((Component) this).GetComponent<Projector>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    Material material = component.get_material();
    material.SetFloat("_offsetFactor", factor);
    material.SetFloat("_offsetUnits", unit);
  }
}
