﻿// Decompiled with JetBrains decompiler
// Type: WindowsAPI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class WindowsAPI
{
  public static readonly int MONITOR_DEFAULTTOPRIMARY = 1;
  public static readonly int MONITOR_DEFAULTTONEAREST = 2;
  public static readonly int MONITOR_DEFAULTTONULL;

  [DllImport("user32.dll", CharSet = CharSet.Auto)]
  public static extern IntPtr FindWindow(string className, string windowName);

  [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
  public static extern int SetWindowLong32(HandleRef hWnd, int nIndex, int dwNewLong);

  [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
  public static extern IntPtr SetWindowLongPtr64(HandleRef hWnd, int nIndex, IntPtr dwNewLong);

  [DllImport("user32.dll", EntryPoint = "DefWindowProcA")]
  public static extern IntPtr DefWindowProc(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);

  [DllImport("user32.dll", CharSet = CharSet.Auto)]
  public static extern IntPtr CallWindowProc(IntPtr lpPrevWndFunc, IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);

  [DllImport("user32.dll", CharSet = CharSet.Auto)]
  private static extern int GetWindowRect(IntPtr hWnd, out WindowsAPI.RECT rect);

  [DllImport("user32.dll", CharSet = CharSet.Auto)]
  private static extern bool GetClientRect(IntPtr hWnd, out WindowsAPI.RECT rect);

  [DllImport("user32.dll", CharSet = CharSet.Auto)]
  private static extern int MoveWindow(IntPtr hWnd, int x, int y, int nWidth, int nHeight, int bRepaint);

  [DllImport("user32.dll", CharSet = CharSet.Auto)]
  public static extern IntPtr MonitorFromWindow(IntPtr hwnd, IntPtr dwFlags);

  [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
  public static extern bool GetMonitorInfo(IntPtr hMonitor, ref WindowsAPI.MONITORINFOEX moniterInfoEX);

  public static int MoveWindow(IntPtr hWnd, Rect rect, bool repaint)
  {
    return WindowsAPI.MoveWindow(hWnd, (int) ((Rect) ref rect).get_x(), (int) ((Rect) ref rect).get_y(), (int) ((Rect) ref rect).get_width(), (int) ((Rect) ref rect).get_height(), !repaint ? 0 : 1);
  }

  public static WindowsAPI.MonitorInfoEx GetMonitorInfo(IntPtr hWnd)
  {
    IntPtr hMonitor = WindowsAPI.MonitorFromWindow(hWnd, new IntPtr(WindowsAPI.MONITOR_DEFAULTTONEAREST));
    WindowsAPI.MONITORINFOEX moniterInfoEX = new WindowsAPI.MONITORINFOEX();
    moniterInfoEX.cbSize = Marshal.SizeOf((object) moniterInfoEX);
    WindowsAPI.GetMonitorInfo(hMonitor, ref moniterInfoEX);
    WindowsAPI.MonitorInfoEx monitorInfoEx = new WindowsAPI.MonitorInfoEx();
    monitorInfoEx.workAreaRect = (Rect) null;
    ((Rect) ref monitorInfoEx.workAreaRect).set_xMin((float) moniterInfoEX.rcWork.left);
    ((Rect) ref monitorInfoEx.workAreaRect).set_xMax((float) moniterInfoEX.rcWork.right);
    ((Rect) ref monitorInfoEx.workAreaRect).set_yMin((float) moniterInfoEX.rcWork.top);
    ((Rect) ref monitorInfoEx.workAreaRect).set_yMax((float) moniterInfoEX.rcWork.bottom);
    monitorInfoEx.monitorRect = (Rect) null;
    ((Rect) ref monitorInfoEx.monitorRect).set_xMin((float) moniterInfoEX.rcMonitor.left);
    ((Rect) ref monitorInfoEx.monitorRect).set_xMax((float) moniterInfoEX.rcMonitor.right);
    ((Rect) ref monitorInfoEx.monitorRect).set_yMin((float) moniterInfoEX.rcMonitor.top);
    ((Rect) ref monitorInfoEx.monitorRect).set_yMax((float) moniterInfoEX.rcMonitor.bottom);
    monitorInfoEx.Device = moniterInfoEX.szDevice;
    return monitorInfoEx;
  }

  public static Rect GetWindowRect(IntPtr hWnd)
  {
    WindowsAPI.RECT rect1 = new WindowsAPI.RECT();
    WindowsAPI.GetWindowRect(hWnd, out rect1);
    Rect rect2 = (Rect) null;
    ((Rect) ref rect2).set_xMin((float) rect1.left);
    ((Rect) ref rect2).set_xMax((float) rect1.right);
    ((Rect) ref rect2).set_yMin((float) rect1.top);
    ((Rect) ref rect2).set_yMax((float) rect1.bottom);
    return rect2;
  }

  public static Rect GetClientRect(IntPtr hWnd)
  {
    WindowsAPI.RECT rect1 = new WindowsAPI.RECT();
    WindowsAPI.GetClientRect(hWnd, out rect1);
    Rect rect2 = (Rect) null;
    ((Rect) ref rect2).set_xMin((float) rect1.left);
    ((Rect) ref rect2).set_xMax((float) rect1.right);
    ((Rect) ref rect2).set_yMin((float) rect1.top);
    ((Rect) ref rect2).set_yMax((float) rect1.bottom);
    return rect2;
  }

  public static IntPtr SetWindowLongPtr(HandleRef hWnd, int nIndex, IntPtr dwNewLong)
  {
    if (IntPtr.Size == 8)
      return WindowsAPI.SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
    return new IntPtr(WindowsAPI.SetWindowLong32(hWnd, nIndex, dwNewLong.ToInt32()));
  }

  public delegate IntPtr WndProcDelegate(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

  public struct RECT
  {
    public int left;
    public int top;
    public int right;
    public int bottom;
  }

  [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
  public struct MONITORINFOEX
  {
    public int cbSize;
    public WindowsAPI.RECT rcMonitor;
    public WindowsAPI.RECT rcWork;
    public uint dwFlags;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
    public string szDevice;
  }

  public class MonitorInfoEx
  {
    public Rect monitorRect;
    public Rect workAreaRect;
    public int flag;
    public string Device;
  }
}
