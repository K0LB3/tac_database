﻿// Decompiled with JetBrains decompiler
// Type: LocalNotificationInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public class LocalNotificationInfo
{
  public int id;
  public int push_flg;
  public string trophy_iname;
  public string push_word;

  public bool Deserialize(JSON_LocalNotificationInfo json)
  {
    if (json == null)
      return false;
    this.id = json.fields.id;
    this.trophy_iname = json.fields.trophy_iname;
    this.push_flg = json.fields.push_flg;
    this.push_word = json.fields.push_word;
    return true;
  }
}
