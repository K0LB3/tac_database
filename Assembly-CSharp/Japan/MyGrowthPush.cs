﻿// Decompiled with JetBrains decompiler
// Type: MyGrowthPush
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using UnityEngine;

public class MyGrowthPush : MonoBehaviour
{
  public string applicationId;
  public string credentialId;
  public string senderId;

  public MyGrowthPush()
  {
    base.\u002Ector();
  }

  public GrowthPush.Environment getEnvironment()
  {
    return Debug.get_isDebugBuild() ? GrowthPush.Environment.Development : GrowthPush.Environment.Production;
  }

  private void Start()
  {
    Debug.Log((object) "[GrowthBeat] - Start");
  }

  private void OnDestroy()
  {
    Debug.Log((object) "[GrowthBeat] - Destroy");
  }

  private void Update()
  {
  }

  private void Awake()
  {
    GrowthPush.GetInstance().Initialize(this.applicationId, this.credentialId, this.getEnvironment());
    GrowthPush.GetInstance().RequestDeviceToken(this.senderId);
    GrowthPush.GetInstance().ClearBadge();
    GrowthPush.GetInstance().TrackEvent("Launch");
    GrowthPush.GetInstance().SetDeviceTags();
    GrowthPush.GetInstance().GetDeviceToken();
  }

  public static void registCustomerId(string cuid)
  {
    DebugMenu.Log("PUSH", "GrowthPush Set Tag > customid = " + cuid);
    GrowthPush.GetInstance().SetTag("customid", cuid);
  }
}
