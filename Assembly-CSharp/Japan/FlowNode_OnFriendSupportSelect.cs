﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_OnFriendSupportSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("Event/OnFriendSupportSelect", 58751)]
[FlowNode.Pin(1, "Selected", FlowNode.PinTypes.Output, 0)]
public class FlowNode_OnFriendSupportSelect : FlowNodePersistent
{
  public void Selected()
  {
    this.ActivateOutputLinks(1);
  }
}
