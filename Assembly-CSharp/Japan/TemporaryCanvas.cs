﻿// Decompiled with JetBrains decompiler
// Type: TemporaryCanvas
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class TemporaryCanvas : MonoBehaviour
{
  public GameObject Instance;

  public TemporaryCanvas()
  {
    base.\u002Ector();
  }

  private void OnApplicationQuit()
  {
    this.Instance = (GameObject) null;
  }

  private void Update()
  {
    if (!Object.op_Equality((Object) this.Instance, (Object) null))
      return;
    Object.Destroy((Object) ((Component) this).get_gameObject());
  }
}
