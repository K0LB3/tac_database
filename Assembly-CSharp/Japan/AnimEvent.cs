﻿// Decompiled with JetBrains decompiler
// Type: AnimEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class AnimEvent : ScriptableObject
{
  public float Start;
  public float End;

  public AnimEvent()
  {
    base.\u002Ector();
  }

  public virtual void OnStart(GameObject go)
  {
  }

  public virtual void OnTick(GameObject go, float ratio)
  {
  }

  public virtual void OnEnd(GameObject go)
  {
  }

  public virtual void UpdatePreview(GameObject go, float time)
  {
  }
}
