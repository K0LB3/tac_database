﻿// Decompiled with JetBrains decompiler
// Type: DestructTimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class DestructTimer : MonoBehaviour
{
  public float Timer;

  public DestructTimer()
  {
    base.\u002Ector();
  }

  private void Update()
  {
    this.Timer -= Time.get_deltaTime();
    if ((double) this.Timer > 0.0)
      return;
    Object.Destroy((Object) ((Component) this).get_gameObject());
  }
}
