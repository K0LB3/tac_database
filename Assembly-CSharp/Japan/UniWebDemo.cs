﻿// Decompiled with JetBrains decompiler
// Type: UniWebDemo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class UniWebDemo : MonoBehaviour
{
  public GameObject cubePrefab;
  public TextMesh tipTextMesh;

  public UniWebDemo()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    Debug.LogWarning((object) "UniWebView only works on iOS/Android/WP8. Please switch to these platforms in Build Settings.");
  }
}
