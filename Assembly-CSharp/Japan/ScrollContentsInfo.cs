﻿// Decompiled with JetBrains decompiler
// Type: ScrollContentsInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class ScrollContentsInfo : MonoBehaviour
{
  protected float mStartPosX;
  protected float mEndPosX;
  protected float mStartPosY;
  protected float mEndPosY;

  public ScrollContentsInfo()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.mStartPosX = 0.0f;
    this.mEndPosX = 0.0f;
    this.mStartPosY = 0.0f;
    this.mEndPosY = 0.0f;
  }

  public virtual Vector2 SetRangePos(Vector2 position)
  {
    return Vector2.get_zero();
  }

  public virtual bool CheckRangePos(float pos)
  {
    return false;
  }

  public virtual float GetNearIconPos(float pos)
  {
    return pos;
  }
}
