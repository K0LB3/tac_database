﻿// Decompiled with JetBrains decompiler
// Type: QuestList_Item
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Drafts/QuestList_Item")]
public class QuestList_Item : UIDraft
{
  [UIDraft.AutoGenerated]
  public Image Clear;
  [UIDraft.AutoGenerated]
  public Text Text_QuestName;
  [UIDraft.AutoGenerated]
  public Text Text_PointNum;
  [UIDraft.AutoGenerated]
  public Text Text_Point;
}
