﻿// Decompiled with JetBrains decompiler
// Type: QuitOnEscapeOrBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class QuitOnEscapeOrBack : MonoBehaviour
{
  public QuitOnEscapeOrBack()
  {
    base.\u002Ector();
  }

  private void Update()
  {
    if (!Input.GetKeyDown((KeyCode) 27))
      return;
    Application.Quit();
  }
}
