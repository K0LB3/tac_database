﻿// Decompiled with JetBrains decompiler
// Type: TobiraEnhanceIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using SRPG;
using UnityEngine;

public class TobiraEnhanceIcon : MonoBehaviour
{
  [SerializeField]
  private GameObject mUseSubPieceIcon;

  public TobiraEnhanceIcon()
  {
    base.\u002Ector();
  }

  public void ShowUseSubPieceIcon()
  {
    if (!Object.op_Inequality((Object) this.mUseSubPieceIcon, (Object) null))
      return;
    this.mUseSubPieceIcon.SetActive(UnitTobiraEnhanceWindow.IsUseSubPiese);
  }
}
