﻿// Decompiled with JetBrains decompiler
// Type: MailIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class MailIcon : MonoBehaviour
{
  public GameObject ItemIconTemplate;
  public GameObject CoinIconTemplate;
  public GameObject GoldIconTemplate;
  public GameObject ArenaCoinIconTemplate;
  public GameObject MultiCoinIconTemplate;
  public GameObject EventCoinIconTemplate;
  public GameObject KakeraCoinIconTemplate;
  public GameObject SetIconTemplate;
  public GameObject ArtifactIconTemplate;
  public GameObject ConceptCardIconTemplate;
  public GameObject CurrentIcon;

  public MailIcon()
  {
    base.\u002Ector();
  }
}
