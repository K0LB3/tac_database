﻿// Decompiled with JetBrains decompiler
// Type: ChargeIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class ChargeIcon : MonoBehaviour
{
  public GameObject ChargeIconPrefab;
  private GameObject mChargeIcon;

  public ChargeIcon()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    if (!Object.op_Implicit((Object) this.ChargeIconPrefab))
      return;
    this.mChargeIcon = (GameObject) Object.Instantiate<GameObject>((M0) this.ChargeIconPrefab, ((Component) this).get_transform().get_position(), ((Component) this).get_transform().get_rotation());
    if (!Object.op_Inequality((Object) this.mChargeIcon, (Object) null))
      return;
    this.mChargeIcon.get_transform().SetParent(((Component) this).get_transform());
    this.mChargeIcon.SetActive(false);
  }

  public void Open()
  {
    if (!Object.op_Inequality((Object) this.mChargeIcon, (Object) null) || this.mChargeIcon.get_activeSelf())
      return;
    this.mChargeIcon.SetActive(true);
  }

  public void Close()
  {
    if (!Object.op_Inequality((Object) this.mChargeIcon, (Object) null) || !this.mChargeIcon.get_activeSelf())
      return;
    this.mChargeIcon.SetActive(false);
  }
}
