﻿// Decompiled with JetBrains decompiler
// Type: EventBackGround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EventBackGround : MonoBehaviour
{
  public static List<EventBackGround> Instances = new List<EventBackGround>();
  public bool mClose;

  public EventBackGround()
  {
    base.\u002Ector();
  }

  public static EventBackGround Find()
  {
    foreach (EventBackGround instance in EventBackGround.Instances)
    {
      if (Object.op_Inequality((Object) instance, (Object) null))
        return instance;
    }
    return (EventBackGround) null;
  }

  public static void DiscardAll()
  {
    foreach (EventBackGround instance in EventBackGround.Instances)
    {
      if (!((Component) instance).get_gameObject().get_activeInHierarchy())
        Object.Destroy((Object) ((Component) instance).get_gameObject());
    }
    EventBackGround.Instances.Clear();
  }

  private void Awake()
  {
    EventBackGround.Instances.Add(this);
  }

  private void OnDestroy()
  {
    EventBackGround.Instances.Remove(this);
  }

  public void Open()
  {
    ((Component) this).get_gameObject().SetActive(true);
    this.mClose = false;
  }

  public void Close()
  {
    ((Component) this).get_gameObject().SetActive(false);
    this.mClose = true;
  }
}
