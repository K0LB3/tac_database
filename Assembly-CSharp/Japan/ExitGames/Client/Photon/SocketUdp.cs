﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.SocketUdp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;
using System.Net;
using System.Net.Sockets;
using System.Security;
using System.Threading;

namespace ExitGames.Client.Photon
{
  internal class SocketUdp : IPhotonSocket, IDisposable
  {
    private Socket sock;
    private readonly object syncer;

    public SocketUdp(PeerBase npeer)
    {
      base.\u002Ector(npeer);
      if (this.ReportDebugOfLevel((DebugLevel) 5))
        this.get_Listener().DebugReturn((DebugLevel) 5, "CSharpSocket: UDP, Unity3d.");
      this.set_Protocol((ConnectionProtocol) 0);
      this.PollReceive = (__Null) 0;
    }

    public void Dispose()
    {
      this.set_State((PhotonSocketState) 3);
      if (this.sock != null)
      {
        try
        {
          if (this.sock.Connected)
            this.sock.Close();
        }
        catch (Exception ex)
        {
          this.EnqueueDebugReturn((DebugLevel) 3, "Exception in Dispose(): " + (object) ex);
        }
      }
      this.sock = (Socket) null;
      this.set_State((PhotonSocketState) 0);
    }

    public virtual bool Connect()
    {
      lock (this.syncer)
      {
        if (!base.Connect())
          return false;
        this.set_State((PhotonSocketState) 1);
        new Thread(new ThreadStart(this.DnsAndConnect))
        {
          Name = "photon dns thread",
          IsBackground = true
        }.Start();
        return true;
      }
    }

    public virtual bool Disconnect()
    {
      if (this.ReportDebugOfLevel((DebugLevel) 3))
        this.EnqueueDebugReturn((DebugLevel) 3, "CSharpSocket.Disconnect()");
      this.set_State((PhotonSocketState) 3);
      lock (this.syncer)
      {
        if (this.sock != null)
        {
          try
          {
            this.sock.Close();
          }
          catch (Exception ex)
          {
            this.EnqueueDebugReturn((DebugLevel) 3, "Exception in Disconnect(): " + (object) ex);
          }
          this.sock = (Socket) null;
        }
      }
      this.set_State((PhotonSocketState) 0);
      return true;
    }

    public virtual PhotonSocketError Send(byte[] data, int length)
    {
      lock (this.syncer)
      {
        if (this.sock != null)
        {
          if (this.sock.Connected)
          {
            try
            {
              this.sock.Send(data, 0, length, SocketFlags.None);
              goto label_9;
            }
            catch (Exception ex)
            {
              if (this.ReportDebugOfLevel((DebugLevel) 1))
                this.EnqueueDebugReturn((DebugLevel) 1, "Cannot send to: " + this.get_ServerAddress() + ". " + ex.Message);
              return (PhotonSocketError) 3;
            }
          }
        }
        return (PhotonSocketError) 1;
      }
label_9:
      return (PhotonSocketError) 0;
    }

    public virtual PhotonSocketError Receive(out byte[] data)
    {
      data = (byte[]) null;
      return (PhotonSocketError) 2;
    }

    internal void DnsAndConnect()
    {
      IPAddress address = (IPAddress) null;
      try
      {
        lock (this.syncer)
        {
          address = IPhotonSocket.GetIpAddress(this.get_ServerAddress());
          if (address == null)
            throw new ArgumentException("Invalid IPAddress. Address: " + this.get_ServerAddress());
          if (address.AddressFamily != AddressFamily.InterNetwork && address.AddressFamily != AddressFamily.InterNetworkV6)
            throw new ArgumentException("AddressFamily '" + (object) address.AddressFamily + "' not supported. Address: " + this.get_ServerAddress());
          this.sock = new Socket(address.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
          this.sock.Connect(address, this.get_ServerPort());
          this.set_AddressResolvedAsIpv6(address.AddressFamily == AddressFamily.InterNetworkV6);
          this.set_State((PhotonSocketState) 2);
          ((PeerBase) this.peerBase).OnConnect();
        }
      }
      catch (SecurityException ex)
      {
        if (this.ReportDebugOfLevel((DebugLevel) 1))
          this.get_Listener().DebugReturn((DebugLevel) 1, "Connect() to '" + this.get_ServerAddress() + "' (" + (address != null ? address.AddressFamily.ToString() : string.Empty) + ") failed: " + ex.ToString());
        this.HandleException((StatusCode) 1022);
        return;
      }
      catch (Exception ex)
      {
        if (this.ReportDebugOfLevel((DebugLevel) 1))
          this.get_Listener().DebugReturn((DebugLevel) 1, "Connect() to '" + this.get_ServerAddress() + "' (" + (address != null ? address.AddressFamily.ToString() : string.Empty) + ") failed: " + ex.ToString());
        this.HandleException((StatusCode) 1023);
        return;
      }
      new Thread(new ThreadStart(this.ReceiveLoop))
      {
        Name = "photon receive thread",
        IsBackground = true
      }.Start();
    }

    public void ReceiveLoop()
    {
      byte[] buffer = new byte[this.get_MTU()];
      while (this.get_State() == 2)
      {
        try
        {
          int num = this.sock.Receive(buffer);
          this.HandleReceivedDatagram(buffer, num, true);
        }
        catch (Exception ex)
        {
          if (this.get_State() != 3)
          {
            if (this.get_State() != null)
            {
              if (this.ReportDebugOfLevel((DebugLevel) 1))
                this.EnqueueDebugReturn((DebugLevel) 1, "Receive issue. State: " + (object) this.get_State() + ". Server: '" + this.get_ServerAddress() + "' Exception: " + (object) ex);
              this.HandleException((StatusCode) 1039);
            }
          }
        }
      }
      base.Disconnect();
    }
  }
}
