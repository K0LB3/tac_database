﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatEventCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ChatEventCode
  {
    public const byte ChatMessages = 0;
    public const byte Users = 1;
    public const byte PrivateMessage = 2;
    public const byte FriendsList = 3;
    public const byte StatusUpdate = 4;
    public const byte Subscribe = 5;
    public const byte Unsubscribe = 6;
  }
}
