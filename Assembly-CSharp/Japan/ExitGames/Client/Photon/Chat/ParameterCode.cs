﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ParameterCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ParameterCode
  {
    public const byte ApplicationId = 224;
    public const byte Secret = 221;
    public const byte AppVersion = 220;
    public const byte ClientAuthenticationType = 217;
    public const byte ClientAuthenticationParams = 216;
    public const byte ClientAuthenticationData = 214;
    public const byte Region = 210;
    public const byte Address = 230;
    public const byte UserId = 225;
  }
}
