﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatUserStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace ExitGames.Client.Photon.Chat
{
  public static class ChatUserStatus
  {
    public const int Offline = 0;
    public const int Invisible = 1;
    public const int Online = 2;
    public const int Away = 3;
    public const int DND = 4;
    public const int LFG = 5;
    public const int Playing = 6;
  }
}
