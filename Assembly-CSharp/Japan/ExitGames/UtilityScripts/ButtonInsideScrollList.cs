﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.UtilityScripts.ButtonInsideScrollList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ExitGames.UtilityScripts
{
  public class ButtonInsideScrollList : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
  {
    private ScrollRect scrollRect;

    public ButtonInsideScrollList()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.scrollRect = (ScrollRect) ((Component) this).GetComponentInParent<ScrollRect>();
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
      if (!Object.op_Inequality((Object) this.scrollRect, (Object) null))
        return;
      this.scrollRect.StopMovement();
      ((Behaviour) this.scrollRect).set_enabled(false);
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
      if (!Object.op_Inequality((Object) this.scrollRect, (Object) null) || ((Behaviour) this.scrollRect).get_enabled())
        return;
      ((Behaviour) this.scrollRect).set_enabled(true);
    }
  }
}
