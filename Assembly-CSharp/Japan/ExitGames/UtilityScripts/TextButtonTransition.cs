﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.UtilityScripts.TextButtonTransition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ExitGames.UtilityScripts
{
  [RequireComponent(typeof (Text))]
  public class TextButtonTransition : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
  {
    private Text _text;
    public Color NormalColor;
    public Color HoverColor;

    public TextButtonTransition()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
      this._text = (Text) ((Component) this).GetComponent<Text>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
      ((Graphic) this._text).set_color(this.HoverColor);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
      ((Graphic) this._text).set_color(this.NormalColor);
    }
  }
}
