﻿// Decompiled with JetBrains decompiler
// Type: Photon.MonoBehaviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace Photon
{
  public class MonoBehaviour : MonoBehaviour
  {
    private PhotonView pvCache;

    public MonoBehaviour()
    {
      base.\u002Ector();
    }

    public PhotonView photonView
    {
      get
      {
        if (Object.op_Equality((Object) this.pvCache, (Object) null))
          this.pvCache = PhotonView.Get((Component) this);
        return this.pvCache;
      }
    }
  }
}
