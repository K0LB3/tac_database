﻿// Decompiled with JetBrains decompiler
// Type: QuestLobbyNewsUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class QuestLobbyNewsUI : MonoBehaviour
{
  [SerializeField]
  private QuestLobbyNews.QuestLobbyCategory mCategory;
  [SerializeField]
  private GameObject mBadgeRoot;
  [SerializeField]
  private Text mText;

  public QuestLobbyNewsUI()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    if (!Object.op_Implicit((Object) this.mText) || !Object.op_Implicit((Object) this.mBadgeRoot))
      return;
    this.mBadgeRoot.SetActive(false);
    QuestLobbyNews questLobbyNews = QuestLobbyNews.FindQuestLobbyNews(this.mCategory);
    if (questLobbyNews == null || !questLobbyNews.isShow())
      return;
    this.mBadgeRoot.SetActive(true);
    this.mText.set_text(questLobbyNews.GetShowText());
  }
}
