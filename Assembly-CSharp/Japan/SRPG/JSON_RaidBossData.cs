﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidBossData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidBossData
  {
    public string uid;
    public string name;
    public int area_id;
    public JSON_RaidBossInfo boss_info;
    public int sos_status;
    public JSON_RaidSOSMember[] sos_member;
  }
}
