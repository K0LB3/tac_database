﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_VersusEndEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_VersusEndEnd : Json_PlayerDataAll
  {
    public JSON_QuestProgress[] quests;
    public int wincnt;
    public int win_bonus;
    public int key;
    public int rankup;
    public int floor;
    public int arravied;
  }
}
