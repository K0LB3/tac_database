﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildEntryCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_GuildEntryCondition
  {
    public int lower_level;
    public int is_auto_approval;
    public string recruit_comment;
  }
}
