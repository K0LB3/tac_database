﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SelecteConceptCardMaterial
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class SelecteConceptCardMaterial
  {
    public OLong mUniqueID;
    public ConceptCardData mSelectedData;
    public int mSelectNum;

    public string iname
    {
      get
      {
        if (this.mSelectedData == null)
          return (string) null;
        return this.mSelectedData.Param.iname;
      }
    }
  }
}
