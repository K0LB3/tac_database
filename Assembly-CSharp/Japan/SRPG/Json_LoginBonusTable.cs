﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_LoginBonusTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_LoginBonusTable
  {
    public int count;
    public string type;
    public string prefab;
    public string[] bonus_units;
    public int lastday;
    public Json_LoginBonus[] bonuses;
    public Json_PremiumLoginBonus[] premium_bonuses;
  }
}
