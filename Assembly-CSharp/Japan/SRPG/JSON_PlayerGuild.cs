﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_PlayerGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_PlayerGuild
  {
    public int gid;
    public string guild_name;
    public int role_id;
    public int invest_point;
    public long applied_at;
    public long joined_at;
    public long leaved_at;
    public long invest_at;
  }
}
