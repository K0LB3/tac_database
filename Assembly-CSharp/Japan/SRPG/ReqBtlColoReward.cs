﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqBtlColoReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqBtlColoReward : WebAPI
  {
    public ReqBtlColoReward(Network.ResponseCallback response)
    {
      this.name = "btl/colo/reward";
      this.body = WebAPI.GetRequestString(WebAPI.GetStringBuilder().ToString());
      this.callback = response;
    }
  }
}
