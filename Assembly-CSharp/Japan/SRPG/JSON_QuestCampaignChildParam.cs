﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestCampaignChildParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_QuestCampaignChildParam
  {
    public string iname;
    public int scope;
    public int quest_type;
    public int quest_mode;
    public string quest_id;
    public string unit;
    public int drop_rate;
    public int drop_num;
    public int exp_player;
    public int exp_unit;
    public int ap_rate;
  }
}
