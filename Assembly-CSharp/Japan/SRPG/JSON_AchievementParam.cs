﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AchievementParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_AchievementParam
  {
    public int pk;
    public JSON_AchievementParam.Fields fields;

    public class Fields
    {
      public int id;
      public string iname;
      public string ios;
      public string googleplay;
    }
  }
}
