﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqAppGuardAuthentication
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqAppGuardAuthentication : WebAPI
  {
    public ReqAppGuardAuthentication(string uniqueClientID, Network.ResponseCallback response)
    {
      this.name = "appguard/auth";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"unique_client_id\":");
      stringBuilder.Append("\"");
      stringBuilder.Append(uniqueClientID);
      stringBuilder.Append("\"");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
