﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitSubSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class UnitSubSetting
  {
    public eMapUnitCtCalcType startCtCalc;
    public OInt startCtVal;

    public UnitSubSetting()
    {
    }

    public UnitSubSetting(JSON_MapPartySubCT json)
    {
      this.startCtCalc = (eMapUnitCtCalcType) json.ct_calc;
      this.startCtVal = (OInt) json.ct_val;
    }
  }
}
