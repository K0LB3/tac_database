﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqHikkoshiExec
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqHikkoshiExec : WebAPI
  {
    public ReqHikkoshiExec(string token, Network.ResponseCallback response)
    {
      this.name = "hikkoshi/exec";
      this.body = WebAPI.GetRequestString("\"token\":\"" + token + "\"");
      this.callback = response;
    }
  }
}
