﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AchievementBridge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class AchievementBridge : MonoBehaviour
  {
    public AchievementBridge()
    {
      base.\u002Ector();
    }

    public void OnClick()
    {
      if (GameCenterManager.IsAuth())
      {
        GameManager instanceDirect = MonoSingleton<GameManager>.GetInstanceDirect();
        if (Object.op_Inequality((Object) instanceDirect, (Object) null))
          instanceDirect.Player.UpdateAchievementTrophyStates();
        GameCenterManager.ShowAchievement();
      }
      else
        GameCenterManager.ReAuth();
    }
  }
}
