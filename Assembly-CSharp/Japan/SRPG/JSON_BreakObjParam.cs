﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BreakObjParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_BreakObjParam
  {
    public string iname;
    public string name;
    public string expr;
    public string unit_id;
    public int clash_type;
    public int ai_type;
    public int side_type;
    public int ray_type;
    public int is_ui;
    public string rest_hps;
    public int clock;
    public int appear_dir;
  }
}
