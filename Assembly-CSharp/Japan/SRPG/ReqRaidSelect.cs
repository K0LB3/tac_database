﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidSelect : WebAPI
  {
    public ReqRaidSelect(Network.ResponseCallback response)
    {
      this.name = "raidboss/select";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public JSON_RaidBossData raidboss_current;
    }
  }
}
