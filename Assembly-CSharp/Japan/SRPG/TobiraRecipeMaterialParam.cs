﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TobiraRecipeMaterialParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class TobiraRecipeMaterialParam
  {
    private string mIname;
    private int mNum;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public int Num
    {
      get
      {
        return this.mNum;
      }
    }

    public void Deserialize(JSON_TobiraRecipeMaterialParam json)
    {
      if (json == null)
        return;
      this.mIname = json.iname;
      this.mNum = json.num;
    }
  }
}
