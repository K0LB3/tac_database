﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildLeave
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildLeave : WebAPI
  {
    public ReqGuildLeave(string new_guild_master_uid, Network.ResponseCallback response)
    {
      this.name = "guild/leave";
      this.body = WebAPI.GetRequestString((string) null);
      if (!string.IsNullOrEmpty(new_guild_master_uid))
      {
        StringBuilder stringBuilder = WebAPI.GetStringBuilder();
        stringBuilder.Append("\"target_uid\":\"");
        stringBuilder.Append(new_guild_master_uid);
        stringBuilder.Append("\"");
        this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      }
      this.callback = response;
    }
  }
}
