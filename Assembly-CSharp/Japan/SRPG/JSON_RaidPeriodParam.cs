﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidPeriodParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidPeriodParam : JSON_RaidMasterParam
  {
    public int id;
    public int max_bp;
    public string add_bp_time;
    public int bp_by_coin;
    public int rescue_member_max;
    public string rescure_send_interval;
    public int complete_reward_id;
    public int round_buff_max;
    public string begin_at;
    public string end_at;
  }
}
