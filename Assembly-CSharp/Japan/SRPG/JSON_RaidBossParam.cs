﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidBossParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidBossParam : JSON_RaidMasterParam
  {
    public int id;
    public int stamp_index;
    public int period_id;
    public int weight;
    public string name;
    public int hp;
    public string unit_iname;
    public string quest_iname;
    public string time_limit;
    public int battle_reward_id;
    public int beat_reward_id;
    public int damage_ratio_reward_id;
    public string buff_id;
    public int is_boss;
  }
}
