﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StringIsTextIDPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class StringIsTextIDPopup : PropertyAttribute
  {
    public bool ContainsVoiceID;

    public StringIsTextIDPopup(bool containsVoiceID = false)
    {
      this.\u002Ector();
      this.ContainsVoiceID = containsVoiceID;
    }
  }
}
