﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRescueList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Open Popup", FlowNode.PinTypes.Output, 101)]
  public class RaidRescueList : MonoBehaviour, IFlowInterface
  {
    public const int PIN_INPUT_INIT = 1;
    public const int PIN_OUTPUT_OPEN_POPUP = 101;
    [SerializeField]
    private Transform mItemParent;
    [SerializeField]
    private RaidRescueListItem mItem;
    [SerializeField]
    private GameObject mNoRequest;

    public RaidRescueList()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.mNoRequest.SetActive(false);
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Init();
    }

    private void Init()
    {
      if (Object.op_Equality((Object) this.mItemParent, (Object) null) || Object.op_Equality((Object) this.mItem, (Object) null))
        return;
      if (RaidManager.Instance.RaidRescueMemberList == null || RaidManager.Instance.RaidRescueMemberList.Count <= 0)
      {
        this.mNoRequest.SetActive(true);
      }
      else
      {
        for (int index = 0; index < RaidManager.Instance.RaidRescueMemberList.Count; ++index)
        {
          RaidRescueListItem raidRescueListItem = (RaidRescueListItem) Object.Instantiate<RaidRescueListItem>((M0) this.mItem, this.mItemParent);
          raidRescueListItem.Setup(index, RaidManager.Instance.RaidRescueMemberList[index]);
          ((Component) raidRescueListItem).get_gameObject().SetActive(true);
        }
        this.mNoRequest.SetActive(false);
      }
    }

    public void ListClick(RaidRescueListItem item)
    {
      RaidManager.Instance.SetRescueIndex(item.Index);
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
    }
  }
}
