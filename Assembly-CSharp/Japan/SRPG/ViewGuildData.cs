﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ViewGuildData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ViewGuildData
  {
    public int id;
    public string name;
    public string award_id;
    public int level;
    public int count;
    public int max_count;

    public void Deserialize(JSON_ViewGuild json)
    {
      this.id = json.id;
      this.name = json.name;
      this.award_id = json.award_id;
      this.level = json.level;
      this.count = json.count;
      this.max_count = json.max_count;
    }
  }
}
