﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_PremiumLoginBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class Json_PremiumLoginBonus
  {
    public string icon;
    public Json_PremiumLoginBonusItem[] item;
    public int coin;
    public int gold;
    public Json_PremiumLoginBonus premium;
  }
}
