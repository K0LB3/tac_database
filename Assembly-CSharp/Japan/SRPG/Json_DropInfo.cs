﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_DropInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_DropInfo
  {
    public string iname = string.Empty;
    public string iname_origin = string.Empty;
    public string type = string.Empty;
    public int rare = -1;
    public string get_unit = string.Empty;
    public int num;
    public int is_new;
    public int is_gift;
  }
}
