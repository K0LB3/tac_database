﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_WeatherParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_WeatherParam
  {
    public string iname;
    public string name;
    public string expr;
    public string icon;
    public string effect;
    public string[] buff_ids;
    public string[] cond_ids;
  }
}
