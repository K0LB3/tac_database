﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqBuyGold
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqBuyGold : WebAPI
  {
    public ReqBuyGold(int coin, Network.ResponseCallback response)
    {
      this.name = "shop/gold/buy";
      this.body = WebAPI.GetRequestString("\"coin\":" + coin.ToString());
      this.callback = response;
    }
  }
}
