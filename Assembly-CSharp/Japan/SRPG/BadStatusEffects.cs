﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BadStatusEffects
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public static class BadStatusEffects
  {
    public static List<BadStatusEffects.Desc> Effects;
    public static GameObject CurseEffect;
    public static string CurseEffectAttachTarget;
    public static string CurseEffectAttachTargetBigUnit;

    [DebuggerHidden]
    public static IEnumerator LoadEffects(QuestAssets assets)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new BadStatusEffects.\u003CLoadEffects\u003Ec__Iterator0()
      {
        assets = assets
      };
    }

    [DebuggerHidden]
    public static IEnumerator LoadBigUnitEffects()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      BadStatusEffects.\u003CLoadBigUnitEffects\u003Ec__Iterator1 effectsCIterator1 = new BadStatusEffects.\u003CLoadBigUnitEffects\u003Ec__Iterator1();
      return (IEnumerator) effectsCIterator1;
    }

    public static void UnloadEffects()
    {
      BadStatusEffects.Effects = (List<BadStatusEffects.Desc>) null;
      BadStatusEffects.CurseEffect = (GameObject) null;
    }

    public class Desc
    {
      public EUnitCondition Key;
      public GameObject Effect;
      public string NameEffectBigUnit;
      public GameObject EffectBigUnit;
      public string AttachTarget;
      public string AttachTargetBigUnit;
      public Color BlendColor;
      public string AnimationName;
    }
  }
}
