﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_DynamicTransformUnitParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_DynamicTransformUnitParam
  {
    public string iname;
    public string name;
    public string tr_unit_id;
    public int turn;
    public string upper_to_abid;
    public string lower_to_abid;
    public string react_to_abid;
    public int is_no_wa;
    public int is_no_va;
    public int is_no_item;
    public string ct_eff;
    public int ct_dis_ms;
    public int ct_app_ms;
    public int is_tr_hpf;
    public int is_cc_hpf;
  }
}
