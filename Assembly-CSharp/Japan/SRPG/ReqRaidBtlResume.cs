﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidBtlResume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidBtlResume : WebAPI
  {
    public ReqRaidBtlResume(long btlid, Network.ResponseCallback response)
    {
      this.name = "raidboss/btl/resume";
      this.body = WebAPI.GetRequestString<ReqRaidBtlResume.RequestParam>(new ReqRaidBtlResume.RequestParam()
      {
        btlid = btlid
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public long btlid;
    }
  }
}
