﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PulldownItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class PulldownItem : MonoBehaviour
  {
    public Text Text;
    public Graphic Graphic;
    public int Value;
    public Image Overray;

    public PulldownItem()
    {
      base.\u002Ector();
    }

    public virtual void OnStatusChanged(bool enabled)
    {
    }
  }
}
