﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AIAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class AIAction
  {
    public OString skill;
    public OInt type;
    public OInt turn;
    public OBool notBlock;
    public eAIActionNoExecAct noExecAct;
    public int nextActIdx;
    public eAIActionNextTurnAct nextTurnAct;
    public int turnActIdx;
    public SkillLockCondition cond;
  }
}
