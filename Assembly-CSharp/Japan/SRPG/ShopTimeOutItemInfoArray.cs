﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ShopTimeOutItemInfoArray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class ShopTimeOutItemInfoArray
  {
    public ShopTimeOutItemInfo[] Infos;

    public ShopTimeOutItemInfoArray()
    {
    }

    public ShopTimeOutItemInfoArray(ShopTimeOutItemInfo[] infos)
    {
      this.Infos = infos;
    }
  }
}
