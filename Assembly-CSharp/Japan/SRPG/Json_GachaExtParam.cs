﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_GachaExtParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_GachaExtParam
  {
    public Json_GachaStepParam step;
    public Json_GachaLimitParam limit;
    public Json_GachaLimitCntParam limit_cnt;
    public long next_reset_time;
    public Json_GachaRedrawParam redraw;
  }
}
