﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqFriendFavoriteRemove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqFriendFavoriteRemove : WebAPI
  {
    public ReqFriendFavoriteRemove(string fuid, Network.ResponseCallback response)
    {
      this.name = "friend/favorite/remove";
      this.body = WebAPI.GetRequestString("\"fuid\":\"" + fuid + "\"");
      this.callback = response;
    }
  }
}
