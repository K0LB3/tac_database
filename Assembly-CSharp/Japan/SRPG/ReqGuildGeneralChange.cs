﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildGeneralChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildGeneralChange : WebAPI
  {
    public ReqGuildGeneralChange(string target_user_uid, Network.ResponseCallback response)
    {
      this.name = "guild/appoint/general";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"target_uid\":\"");
      stringBuilder.Append(target_user_uid);
      stringBuilder.Append("\"");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
