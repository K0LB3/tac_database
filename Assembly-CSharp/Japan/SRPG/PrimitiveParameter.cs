﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PrimitiveParameter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class PrimitiveParameter : MonoBehaviour, IGameParameter
  {
    [SerializeField]
    private PrimitiveParameter.PrimitiveType Type;
    [SerializeField]
    private Text TargetText;

    public PrimitiveParameter()
    {
      base.\u002Ector();
    }

    public void UpdateValue()
    {
      switch (this.Type)
      {
        case PrimitiveParameter.PrimitiveType.Int:
          this.SetText(DataSource.FindDataOfClass<int>(((Component) this).get_gameObject(), 0).ToString());
          break;
        case PrimitiveParameter.PrimitiveType.Long:
          this.SetText(DataSource.FindDataOfClass<long>(((Component) this).get_gameObject(), 0L).ToString());
          break;
        case PrimitiveParameter.PrimitiveType.Float:
          this.SetText(DataSource.FindDataOfClass<float>(((Component) this).get_gameObject(), 0.0f).ToString());
          break;
        case PrimitiveParameter.PrimitiveType.Double:
          this.SetText(DataSource.FindDataOfClass<double>(((Component) this).get_gameObject(), 0.0).ToString());
          break;
        case PrimitiveParameter.PrimitiveType.String:
          this.SetText(DataSource.FindDataOfClass<string>(((Component) this).get_gameObject(), (string) null));
          break;
      }
    }

    private void SetText(string text)
    {
      if (!Object.op_Inequality((Object) this.TargetText, (Object) null))
        return;
      this.TargetText.set_text(text);
    }

    public enum PrimitiveType
    {
      Int,
      Long,
      Float,
      Double,
      String,
    }
  }
}
