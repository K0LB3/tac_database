﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ShopItemDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_ShopItemDesc
  {
    public string iname;
    public int num;
    public string itype;
    public int maxnum;
    public int boughtnum;
    public int has_count;

    public bool IsItem
    {
      get
      {
        return this.itype == "item";
      }
    }

    public bool IsArtifact
    {
      get
      {
        return this.itype == "artifact";
      }
    }

    public bool IsConceptCard
    {
      get
      {
        return this.itype == "concept_card";
      }
    }
  }
}
