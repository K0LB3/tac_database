﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestClearUnlockUnitDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_QuestClearUnlockUnitDataParam
  {
    public string iname;
    public string uid;
    public int add;
    public int type;
    public string new_id;
    public string old_id;
    public string parent_id;
    public int ulv;
    public string aid;
    public int alv;
    public string[] qids;
    public int qcnd;
  }
}
