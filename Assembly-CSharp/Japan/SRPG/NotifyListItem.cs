﻿// Decompiled with JetBrains decompiler
// Type: SRPG.NotifyListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class NotifyListItem : MonoBehaviour
  {
    public Text Message;
    [NonSerialized]
    public float Lifetime;
    [NonSerialized]
    public float Height;

    public NotifyListItem()
    {
      base.\u002Ector();
    }
  }
}
