﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CustomEnum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class CustomEnum : PropertyAttribute
  {
    public System.Type EnumType;
    public int DefaultValue;

    public CustomEnum(System.Type enumType, int defaultValue)
    {
      this.\u002Ector();
      this.EnumType = enumType;
      this.DefaultValue = defaultValue;
    }
  }
}
