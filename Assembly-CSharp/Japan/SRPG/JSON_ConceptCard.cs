﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ConceptCard
  {
    public long iid;
    public string iname;
    public int exp;
    public int trust;
    public int fav;
    public int trust_bonus;
    public int plus;
  }
}
