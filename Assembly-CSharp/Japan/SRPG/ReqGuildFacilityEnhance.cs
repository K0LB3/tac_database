﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildFacilityEnhance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildFacilityEnhance : WebAPI
  {
    public ReqGuildFacilityEnhance(string facility_iname, EnhanceMaterial[] materials, Network.ResponseCallback response)
    {
      this.name = "guild/facility/invest";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"facility_iname\":\"");
      stringBuilder.Append(facility_iname);
      stringBuilder.Append("\"");
      stringBuilder.Append(",");
      stringBuilder.Append("\"mats\":[");
      for (int index = 0; index < materials.Length; ++index)
      {
        stringBuilder.Append("{");
        stringBuilder.Append("\"iname\":\"");
        stringBuilder.Append(materials[index].item.Param.iname);
        stringBuilder.Append("\"");
        stringBuilder.Append(",");
        stringBuilder.Append("\"num\":");
        stringBuilder.Append(materials[index].num);
        stringBuilder.Append("}");
        if (index < materials.Length - 1)
          stringBuilder.Append(",");
      }
      stringBuilder.Append("]");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
