﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidBeatRankingWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "初期化", FlowNode.PinTypes.Input, 1)]
  public class RaidBeatRankingWindow : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_INIT = 1;
    [SerializeField]
    private GameObject mBeatItem;
    [SerializeField]
    private Transform mBeatParent;
    [SerializeField]
    private GameObject mBeatSelf;
    [SerializeField]
    private GameObject mRescueItem;
    [SerializeField]
    private Transform mRescueParent;
    [SerializeField]
    private GameObject mRescueSelf;
    private static RaidBeatRankingWindow mInstance;
    private RaidRankingList mRankingBeat;
    private RaidRankingList mRankingRescue;
    private List<GameObject> mBeatList;
    private List<GameObject> mRescueList;

    public RaidBeatRankingWindow()
    {
      base.\u002Ector();
    }

    public static RaidBeatRankingWindow Instance
    {
      get
      {
        return RaidBeatRankingWindow.mInstance;
      }
    }

    private void Awake()
    {
      RaidBeatRankingWindow.mInstance = this;
      this.mBeatItem.SetActive(false);
      this.mRescueItem.SetActive(false);
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Init();
    }

    public void Setup(ReqRaidRankingBeat.Response json)
    {
      if (json == null)
        return;
      if (json.beat != null)
      {
        this.mRankingBeat = new RaidRankingList();
        if (!this.mRankingBeat.Deserialize(json.beat))
          return;
      }
      if (json.rescue == null)
        return;
      this.mRankingRescue = new RaidRankingList();
      if (!this.mRankingRescue.Deserialize(json.rescue))
        ;
    }

    private void Init()
    {
      this.Init_List(this.mRankingBeat, this.mBeatList, this.mBeatItem, this.mBeatParent, this.mBeatSelf);
      this.Init_List(this.mRankingRescue, this.mRescueList, this.mRescueItem, this.mRescueParent, this.mRescueSelf);
    }

    private void Init_List(RaidRankingList rankingList, List<GameObject> currentList, GameObject item, Transform parent, GameObject self)
    {
      for (int index = 0; index < currentList.Count; ++index)
        Object.Destroy((Object) currentList[index]);
      currentList.Clear();
      if (rankingList == null || Object.op_Equality((Object) item, (Object) null) || (Object.op_Equality((Object) parent, (Object) null) || Object.op_Equality((Object) self, (Object) null)))
        return;
      DataSource.Bind<RaidRankingData>(self, rankingList.MyInfo);
      if (!string.IsNullOrEmpty(rankingList.MyInfo.SelectedAward))
      {
        AwardParam awardParam = MonoSingleton<GameManager>.Instance.GetAwardParam(rankingList.MyInfo.SelectedAward);
        DataSource.Bind<AwardParam>(self, awardParam);
      }
      if (rankingList.MyInfo.Unit != null)
        DataSource.Bind<UnitData>(self, rankingList.MyInfo.Unit);
      GameParameter.UpdateAll(self);
      for (int index = 0; index < rankingList.Ranking.Count; ++index)
      {
        GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) item, parent);
        DataSource.Bind<RaidRankingData>(gameObject, rankingList.Ranking[index]);
        if (!string.IsNullOrEmpty(rankingList.Ranking[index].SelectedAward))
        {
          AwardParam awardParam = MonoSingleton<GameManager>.Instance.GetAwardParam(rankingList.Ranking[index].SelectedAward);
          DataSource.Bind<AwardParam>(gameObject, awardParam);
        }
        if (rankingList.Ranking[index].Unit != null)
          DataSource.Bind<UnitData>(gameObject, rankingList.Ranking[index].Unit);
        gameObject.SetActive(true);
      }
    }
  }
}
