﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BattleUnitDetailTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class BattleUnitDetailTag : MonoBehaviour
  {
    public Text TextValue;

    public BattleUnitDetailTag()
    {
      base.\u002Ector();
    }

    public void SetTag(string tag)
    {
      if (tag == null)
        tag = string.Empty;
      if (!Object.op_Implicit((Object) this.TextValue))
        return;
      this.TextValue.set_text(tag);
    }
  }
}
