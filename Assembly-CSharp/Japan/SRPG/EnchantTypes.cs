﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EnchantTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum EnchantTypes
  {
    Poison,
    Paralysed,
    Stun,
    Sleep,
    Charm,
    Stone,
    Blind,
    DisableSkill,
    DisableMove,
    DisableAttack,
    Zombie,
    DeathSentence,
    Berserk,
    Knockback,
    ResistBuff,
    ResistDebuff,
    Stop,
    Fast,
    Slow,
    AutoHeal,
    Donsoku,
    Rage,
    GoodSleep,
    AutoJewel,
    DisableHeal,
    SingleAttack,
    AreaAttack,
    DecCT,
    IncCT,
    ESA_Fire,
    ESA_Water,
    ESA_Wind,
    ESA_Thunder,
    ESA_Shine,
    ESA_Dark,
    MaxDamageHp,
    MaxDamageMp,
    SideAttack,
    BackAttack,
  }
}
