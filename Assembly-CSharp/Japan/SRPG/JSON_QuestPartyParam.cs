﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestPartyParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_QuestPartyParam
  {
    public string iname;
    public int type_1;
    public int type_2;
    public int type_3;
    public int type_4;
    public int support_type;
    public int subtype_1;
    public int subtype_2;
    public string unit_1;
    public string unit_2;
    public string unit_3;
    public string unit_4;
    public string subunit_1;
    public string subunit_2;
    public int l_npc_rare;
  }
}
