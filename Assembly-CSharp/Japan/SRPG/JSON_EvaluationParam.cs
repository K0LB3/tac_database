﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_EvaluationParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_EvaluationParam
  {
    public string iname;
    public int val;
    public int hp;
    public int mp;
    public int atk;
    public int def;
    public int mag;
    public int mnd;
    public int dex;
    public int spd;
    public int cri;
    public int luk;
  }
}
