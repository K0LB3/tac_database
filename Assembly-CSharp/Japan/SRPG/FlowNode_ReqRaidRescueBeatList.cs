﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqRaidRescueBeatList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace SRPG
{
  [FlowNode.NodeType("System/ReqRaid/Rescue/BeatList", 32741)]
  public class FlowNode_ReqRaidRescueBeatList : FlowNode_ReqRaidBase
  {
    public override WebAPI GenerateWebAPI()
    {
      return (WebAPI) new ReqRaidBeatList(new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback));
    }

    public override bool Success(WWWResult www)
    {
      WebAPI.JSON_BodyResponse<ReqRaidBeatList.Response> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<ReqRaidBeatList.Response>>(www.text);
      DebugUtility.Assert(jsonObject != null, "res == null");
      try
      {
        if (jsonObject.body == null)
          throw new Exception("Response is NULL : /raidboss/rescue/beatlist");
        if (UnityEngine.Object.op_Equality((UnityEngine.Object) RaidStampRallyWindow.Instance, (UnityEngine.Object) null))
          throw new Exception("RaidStampRallyWindow not exists : /raidboss/rescue/beatlist");
        RaidStampRallyWindow.Instance.SetParam(jsonObject.body);
        List<RaidBossParam> raidBossAll = RaidStampRallyWindow.GetRaidBossAll(RaidManager.Instance.RaidPeriodId);
        for (int index = 0; index < raidBossAll.Count; ++index)
          AssetManager.PrepareAssets(AssetPath.UnitIconMedium(MonoSingleton<GameManager>.Instance.GetUnitParam(raidBossAll[index].UnitIName), (string) null));
        this.StartCoroutine(this.DownloadUnitImage());
      }
      catch (Exception ex)
      {
        DebugUtility.LogException(ex);
        return false;
      }
      return false;
    }

    [DebuggerHidden]
    private IEnumerator DownloadUnitImage()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FlowNode_ReqRaidRescueBeatList.\u003CDownloadUnitImage\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
