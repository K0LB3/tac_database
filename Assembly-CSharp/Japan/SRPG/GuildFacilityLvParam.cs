﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildFacilityLvParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class GuildFacilityLvParam
  {
    public int lv;
    public int base_camp;

    public bool Deserialize(JSON_GuildFacilityLvParam json)
    {
      this.lv = json.lv;
      this.base_camp = json.base_camp;
      return true;
    }
  }
}
