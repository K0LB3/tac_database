﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RankMatchClassListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class RankMatchClassListItem : ListItemEvents
  {
    public GameObject RewardUnit;
    public GameObject RewardItem;
    public GameObject RewardCard;
    public GameObject RewardArtifact;
    public GameObject RewardAward;
    public GameObject RewardGold;
    public GameObject RewardCoin;
    public Transform RewardList;
  }
}
