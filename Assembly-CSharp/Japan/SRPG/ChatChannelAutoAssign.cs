﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatChannelAutoAssign
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ChatChannelAutoAssign
  {
    public int channel;

    public void Deserialize(JSON_ChatChannelAutoAssign json)
    {
      if (json == null)
        return;
      this.channel = json.channel;
    }
  }
}
