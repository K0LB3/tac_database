﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ResetScrollPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.NodeType("UI/ResetScrollPosition", 32741)]
  [FlowNode.Pin(0, "Input", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "Output", FlowNode.PinTypes.Output, 10)]
  public class FlowNode_ResetScrollPosition : FlowNode
  {
    [SerializeField]
    private ScrollRect ScrollParent;
    [SerializeField]
    private Transform ResetTransform;
    private float mDecelerationRate;

    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      this.ResetScrollPosition();
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }

    private void ResetScrollPosition()
    {
      if (Object.op_Equality((Object) this.ScrollParent, (Object) null))
        return;
      this.mDecelerationRate = this.ScrollParent.get_decelerationRate();
      this.ScrollParent.set_decelerationRate(0.0f);
      RectTransform resetTransform = this.ResetTransform as RectTransform;
      resetTransform.set_anchoredPosition(new Vector2((float) resetTransform.get_anchoredPosition().x, 0.0f));
      this.StartCoroutine(this.RefreshScrollRect());
    }

    [DebuggerHidden]
    private IEnumerator RefreshScrollRect()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FlowNode_ResetScrollPosition.\u003CRefreshScrollRect\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
