﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqConvertPiece
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqConvertPiece : WebAPI
  {
    public ReqConvertPiece(string consume, string unit, int pieceNum, Network.ResponseCallback response)
    {
      this.name = "shop/piece/convert";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"convert_piece\":{");
      stringBuilder.Append("\"consume_piece_iname\":\"");
      stringBuilder.Append(consume);
      stringBuilder.Append("\",");
      stringBuilder.Append("\"convert_piece_iname\":\"");
      stringBuilder.Append(unit);
      stringBuilder.Append("\",");
      stringBuilder.Append("\"convert_piece_num\":");
      stringBuilder.Append(pieceNum);
      stringBuilder.Append("}");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
