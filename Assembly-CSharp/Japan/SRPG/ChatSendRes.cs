﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatSendRes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ChatSendRes
  {
    public byte is_success;

    public bool IsSuccess
    {
      get
      {
        return this.is_success == (byte) 1;
      }
    }

    public void Deserialize(JSON_ChatSendRes json)
    {
      if (json == null)
        return;
      this.is_success = json.is_success;
    }
  }
}
