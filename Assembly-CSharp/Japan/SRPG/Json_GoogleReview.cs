﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_GoogleReview
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_GoogleReview
  {
    public Json_PlayerData player;
    public Json_Unit[] units;
    public Json_Item[] items;
    public Json_Mail[] mails;
  }
}
