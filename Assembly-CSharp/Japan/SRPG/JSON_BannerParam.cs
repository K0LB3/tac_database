﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BannerParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_BannerParam
  {
    public string iname;
    public string type;
    public string sval;
    public string banr;
    public string banr_sprite;
    public string begin_at;
    public string end_at;
    public int priority;
    public string message;
    public int is_not_home;
  }
}
