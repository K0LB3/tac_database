﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_SectionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_SectionParam
  {
    public string iname;
    public string name;
    public string expr;
    public long start;
    public long end;
    public int hide;
    public string home;
    public string unit;
    public string item;
    public string shop;
    public string inn;
    public string bar;
    public string bgm;
    public int story_part;
    public string release_key_quest;
    public string message_sys_id;
  }
}
