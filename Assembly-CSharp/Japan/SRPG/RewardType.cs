﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum RewardType
  {
    Item = 0,
    Gold = 1,
    Coin = 2,
    Artifact = 3,
    Unit = 4,
    Award = 5,
    ConceptCard = 6,
    Nothing = 100, // 0x00000064
  }
}
