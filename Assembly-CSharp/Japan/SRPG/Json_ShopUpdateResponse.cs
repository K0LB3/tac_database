﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ShopUpdateResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_ShopUpdateResponse
  {
    public Json_Currencies currencies;
    public Json_ShopItem[] shopitems;
    public int relcnt;
    public int concept_count;
  }
}
