﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_CustomTargetParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_CustomTargetParam
  {
    public string iname;
    public string name;
    public string[] units;
    public string[] jobs;
    public string[] unit_groups;
    public string[] job_groups;
    public string first_job;
    public string second_job;
    public string third_job;
    public int sex;
    public int birth_id;
    public int fire;
    public int water;
    public int wind;
    public int thunder;
    public int shine;
    public int dark;
  }
}
