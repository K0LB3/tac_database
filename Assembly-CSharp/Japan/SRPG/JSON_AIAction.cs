﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AIAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_AIAction
  {
    public string skill;
    public int type;
    public int turn;
    public int notBlock;
    public int noExecAct;
    public int nextActIdx;
    public int nextTurnAct;
    public int turnActIdx;
    public JSON_SkillLockCondition cond;
  }
}
