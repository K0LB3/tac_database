﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_JobRankParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_JobRankParam
  {
    public int chcost;
    public string chitm1;
    public string chitm2;
    public string chitm3;
    public int chnum1;
    public int chnum2;
    public int chnum3;
    public int cost;
    public string eqid1;
    public string eqid2;
    public string eqid3;
    public string eqid4;
    public string eqid5;
    public string eqid6;
    public string learn1;
    public string learn2;
    public string learn3;
  }
}
