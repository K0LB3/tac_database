﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_InfinitySpawnGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_InfinitySpawnGroup
  {
    public int tag;
    public int deck;
    public int interval;
    public int spawn_max;
    public int spawn_unit_num;
    public int is_spawn_at_start;
    public int is_skip_empty_at_start;
  }
}
