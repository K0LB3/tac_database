﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqBtlComReset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqBtlComReset : WebAPI
  {
    public ReqBtlComReset(string iname, Network.ResponseCallback response)
    {
      this.name = "btl/com/reset";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"iname\":\"");
      stringBuilder.Append(iname);
      stringBuilder.Append("\"");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
