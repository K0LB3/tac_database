﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ChatStampParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class JSON_ChatStampParam
  {
    public int pk;
    public JSON_ChatStampParam.Fields fields;

    public class Fields
    {
      public int id;
      public string img_id;
      public string iname;
      public int is_private;
    }
  }
}
