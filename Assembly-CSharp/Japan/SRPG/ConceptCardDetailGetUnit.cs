﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardDetailGetUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardDetailGetUnit : ConceptCardDetailBase
  {
    [SerializeField]
    private RawImage UnitIcon;
    [SerializeField]
    private Text UnitName;
    [SerializeField]
    private ButtonEvent UnitDetailBtn;

    public override void Refresh()
    {
      if (this.mConceptCardData == null)
        return;
      string firstGetUnit = this.mConceptCardData.Param.first_get_unit;
      if (string.IsNullOrEmpty(firstGetUnit))
        return;
      UnitParam unitParam = this.GM.GetUnitParam(firstGetUnit);
      if (unitParam == null)
        return;
      if (Object.op_Inequality((Object) this.UnitIcon, (Object) null))
        MonoSingleton<GameManager>.Instance.ApplyTextureAsync(this.UnitIcon, unitParam == null ? (string) null : AssetPath.UnitSkinIconSmall(unitParam, (ArtifactParam) null, (string) null));
      if (Object.op_Inequality((Object) this.UnitName, (Object) null))
        this.UnitName.set_text(unitParam.name);
      if (!Object.op_Inequality((Object) this.UnitDetailBtn, (Object) null))
        return;
      this.UnitDetailBtn.GetEvent("CONCEPT_CARD_DETAIL_BTN_UNIT_DETAIL")?.valueList.SetField("select_unit", unitParam.iname);
    }
  }
}
