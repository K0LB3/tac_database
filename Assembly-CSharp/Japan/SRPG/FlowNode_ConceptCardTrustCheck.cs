﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ConceptCardTrustCheck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("UI/ConceptCardTrustCheck", 32741)]
  [FlowNode.Pin(10, "入力", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(1000, "トラスト値の最大値以上", FlowNode.PinTypes.Output, 1000)]
  [FlowNode.Pin(1001, "トラスト値の最大値より少ない", FlowNode.PinTypes.Output, 1001)]
  public class FlowNode_ConceptCardTrustCheck : FlowNode
  {
    private const int INPUT_CHECK = 10;
    private const int OUTPUT_HIGH = 1000;
    private const int OUTPUT_LOW = 1001;

    public override void OnActivate(int pinID)
    {
      if (pinID != 10)
        return;
      ConceptCardManager instance = ConceptCardManager.Instance;
      if (Object.op_Equality((Object) instance, (Object) null))
        return;
      if (ConceptCardManager.CalcTotalTrust(instance.SelectedConceptCardData, instance.SelectedMaterials) > (int) MonoSingleton<GameManager>.Instance.MasterParam.FixParam.CardTrustMax)
        this.ActivateOutputLinks(1000);
      else
        this.ActivateOutputLinks(1001);
    }
  }
}
