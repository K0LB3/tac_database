﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConvertUnitPieceExcludeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ConvertUnitPieceExcludeParam
  {
    public int id;
    public string unit_piece_iname;

    public void Deserialize(JSON_ConvertUnitPieceExcludeParam json)
    {
      if (json == null)
        return;
      this.id = json.id;
      this.unit_piece_iname = json.unit_piece_iname;
    }
  }
}
