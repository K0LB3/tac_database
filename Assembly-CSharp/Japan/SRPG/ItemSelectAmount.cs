﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ItemSelectAmount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ItemSelectAmount : MonoBehaviour, IGameParameter
  {
    public ItemSelectAmount()
    {
      base.\u002Ector();
    }

    public void UpdateValue()
    {
      ItemSelectListItemData dataOfClass = DataSource.FindDataOfClass<ItemSelectListItemData>(((Component) this).get_gameObject(), (ItemSelectListItemData) null);
      Text component = (Text) ((Component) this).get_gameObject().GetComponent<Text>();
      if (!Object.op_Inequality((Object) component, (Object) null) || dataOfClass == null)
        return;
      component.set_text(dataOfClass.num.ToString());
    }
  }
}
