﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TargetMarker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class TargetMarker : MonoBehaviour
  {
    private Transform m_Transform;

    public TargetMarker()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.m_Transform = (Transform) ((Component) this).GetComponent<Transform>();
    }

    private void LateUpdate()
    {
      SceneBattle instance = SceneBattle.Instance;
      Vector3 zero = Vector3.get_zero();
      if (Object.op_Inequality((Object) instance, (Object) null) && instance.isUpView)
      {
        ref Vector3 local = ref zero;
        local.y = (__Null) (local.y + 0.649999976158142);
      }
      this.m_Transform.set_localPosition(zero);
    }
  }
}
