﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TobiraParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_TobiraParam
  {
    public string unit_iname;
    public int enable;
    public int category;
    public string recipe_id;
    public string skill_iname;
    public JSON_TobiraLearnAbilityParam[] learn_abils;
    public string overwrite_ls_iname;
    public int priority;
  }
}
