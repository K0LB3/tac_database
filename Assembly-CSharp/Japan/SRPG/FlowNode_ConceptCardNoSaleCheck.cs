﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ConceptCardNoSaleCheck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("UI/ConceptCardNoSaleCheck", 32741)]
  [FlowNode.Pin(10, "入力", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(1000, "非売品が存在する", FlowNode.PinTypes.Output, 1000)]
  [FlowNode.Pin(1001, "非売品は無い", FlowNode.PinTypes.Output, 1001)]
  public class FlowNode_ConceptCardNoSaleCheck : FlowNode
  {
    private const int INPUT_CHECK = 10;
    private const int OUTPUT_EXIST = 1000;
    private const int OUTPUT_NO_EXIST = 1001;

    public override void OnActivate(int pinID)
    {
      if (pinID != 10)
        return;
      ConceptCardManager instance = ConceptCardManager.Instance;
      if (Object.op_Equality((Object) instance, (Object) null))
        return;
      foreach (ConceptCardData conceptCardData in instance.SelectedMaterials.GetList())
      {
        if (conceptCardData.Param.not_sale)
        {
          this.ActivateOutputLinks(1000);
          return;
        }
      }
      this.ActivateOutputLinks(1001);
    }
  }
}
