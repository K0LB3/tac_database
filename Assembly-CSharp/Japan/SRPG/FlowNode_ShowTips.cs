﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ShowTips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Tips/ShowTips", 32741)]
  public class FlowNode_ShowTips : FlowNode_GUI
  {
    private const int PIN_ID_IN = 1;
    [SerializeField]
    private string Tips;

    protected override void OnCreatePinActive()
    {
      GlobalVars.SelectTips = this.Tips;
      base.OnCreatePinActive();
    }
  }
}
