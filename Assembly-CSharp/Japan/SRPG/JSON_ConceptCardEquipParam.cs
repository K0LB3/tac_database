﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCardEquipParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ConceptCardEquipParam
  {
    public string cnds_iname;
    public string card_skill;
    public string add_card_skill_buff_awake;
    public string add_card_skill_buff_lvmax;
    public string abil_iname;
    public string abil_iname_lvmax;
    public string statusup_skill;
    public string skin;
  }
}
