﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidRewardRescueComplete
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidRewardRescueComplete : WebAPI
  {
    public ReqRaidRewardRescueComplete(Network.ResponseCallback response)
    {
      this.name = "raidboss/reward/rescue_complete";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public Json_PlayerData player;
      public Json_Item[] items;
      public Json_Unit[] units;
      public JSON_ConceptCard[] cards;
      public Json_Gift[] reward;
      private int round;
    }
  }
}
