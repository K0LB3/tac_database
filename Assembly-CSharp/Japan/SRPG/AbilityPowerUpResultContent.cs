﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AbilityPowerUpResultContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class AbilityPowerUpResultContent : MonoBehaviour
  {
    public AbilityPowerUpResultContent()
    {
      base.\u002Ector();
    }

    public void SetData(AbilityPowerUpResultContent.Param param)
    {
      DataSource.Bind<AbilityParam>(((Component) this).get_gameObject(), param.data);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    public class Param
    {
      public AbilityParam data;
    }
  }
}
