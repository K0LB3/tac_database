﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class RaidArea : MonoBehaviour
  {
    [SerializeField]
    private GameObject mStageRaidIcon;
    [SerializeField]
    private GameObject mStageRaidBossIcon;
    [SerializeField]
    private List<Transform> mStageList;
    [SerializeField]
    private Transform mBossStage;
    private List<RaidBossIcon> mRaidBossIconList;
    private bool mFinishDownload;
    private bool mIsAreaCleared;
    private bool mSelectingRandomRaid;

    public RaidArea()
    {
      base.\u002Ector();
    }

    public bool FinishDownload
    {
      get
      {
        return this.mFinishDownload;
      }
    }

    public bool IsAreaCleared
    {
      get
      {
        return this.mIsAreaCleared;
      }
    }

    public void FinishSelectingRandomRaid()
    {
      this.mSelectingRandomRaid = false;
    }

    public void Initialize()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.mStageRaidIcon, (UnityEngine.Object) null) || this.mStageList == null)
        return;
      if (this.mRaidBossIconList != null)
      {
        for (int index = 0; index < this.mRaidBossIconList.Count; ++index)
          UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) this.mRaidBossIconList[index]).get_gameObject());
        this.mRaidBossIconList.Clear();
      }
      this.mRaidBossIconList = new List<RaidBossIcon>();
      for (int i = 0; i < this.mStageList.Count; ++i)
      {
        GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.mStageRaidIcon);
        gameObject.get_transform().SetParent(this.mStageList[i], false);
        RaidBossInfo raidBossInfo = RaidManager.Instance.BeatedRaidBossList.Find((Predicate<RaidBossInfo>) (brb => brb.No == i + 1));
        RaidBossIcon component = (RaidBossIcon) gameObject.GetComponent<RaidBossIcon>();
        component.Setup(raidBossInfo, i + 1);
        this.mRaidBossIconList.Add(component);
        if (RaidManager.Instance.CurrentRaidBossData != null && RaidManager.Instance.CurrentRaidBossData.RaidBossInfo.No == i + 1)
          raidBossInfo = RaidManager.Instance.CurrentRaidBossData.RaidBossInfo;
        if (raidBossInfo != null)
        {
          RaidBossParam raidBoss = MonoSingleton<GameManager>.Instance.MasterParam.GetRaidBoss(raidBossInfo.BossId);
          if (raidBoss != null)
          {
            UnitParam unitParam = MonoSingleton<GameManager>.Instance.GetUnitParam(raidBoss.UnitIName);
            if (unitParam != null)
            {
              AssetManager.PrepareAssets(AssetPath.UnitIconMedium(unitParam, (string) null));
              AssetManager.PrepareAssets(AssetPath.UnitImage(unitParam, (string) null));
            }
          }
        }
      }
      GameObject gameObject1 = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.mStageRaidBossIcon);
      gameObject1.get_transform().SetParent(this.mBossStage, false);
      RaidBossInfo info = RaidManager.Instance.BeatedRaidBossList.Find((Predicate<RaidBossInfo>) (brb => brb.No == 0));
      if (info != null)
        this.mIsAreaCleared = true;
      RaidBossIcon component1 = (RaidBossIcon) gameObject1.GetComponent<RaidBossIcon>();
      component1.Setup(info, 0);
      this.mRaidBossIconList.Add(component1);
      RaidAreaParam raidArea = MonoSingleton<GameManager>.Instance.MasterParam.GetRaidArea(RaidManager.Instance.CurrentRaidAreaId);
      if (raidArea != null)
      {
        RaidBossParam raidBoss = MonoSingleton<GameManager>.Instance.MasterParam.GetRaidBoss(raidArea.AreaBossId);
        if (raidBoss != null)
        {
          UnitParam unitParam = MonoSingleton<GameManager>.Instance.GetUnitParam(raidBoss.UnitIName);
          if (unitParam != null)
          {
            AssetManager.PrepareAssets(AssetPath.UnitIconMedium(unitParam, (string) null));
            AssetManager.PrepareAssets(AssetPath.UnitImage(unitParam, (string) null));
          }
        }
      }
      this.mFinishDownload = false;
      this.StartCoroutine(this.DownloadUnitImage());
    }

    public void StartSelectRaid()
    {
      this.mSelectingRandomRaid = true;
      this.StartCoroutine(this._StartSelectRaid());
    }

    [DebuggerHidden]
    private IEnumerator _StartSelectRaid()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new RaidArea.\u003C_StartSelectRaid\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    [DebuggerHidden]
    private IEnumerator DownloadUnitImage()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new RaidArea.\u003CDownloadUnitImage\u003Ec__Iterator1()
      {
        \u0024this = this
      };
    }
  }
}
