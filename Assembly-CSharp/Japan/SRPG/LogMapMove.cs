﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LogMapMove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class LogMapMove : BattleLog
  {
    public Unit self;
    public int ex;
    public int ey;
    public EUnitDirection dir;
    public bool auto;
  }
}
