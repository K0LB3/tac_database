﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RankingQuestScheduleParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RankingQuestScheduleParam
  {
    public int id;
    public string begin_at;
    public string end_at;
    public string reward_begin_at;
    public string reward_end_at;
    public string visible_begin_at;
    public string visible_end_at;
  }
}
