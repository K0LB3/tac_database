﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TrophyCategoryParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_TrophyCategoryParam
  {
    public string iname;
    public int hash_code;
    public int category;
    public int is_not_pull;
    public int day_reset;
    public int bgnr;
    public string begin_at;
    public string end_at;
    public string linked_quest;
    public string banner;
  }
}
