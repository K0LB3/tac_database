﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EBirth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum EBirth
  {
    Unknown = 0,
    Envy = 1,
    Wrath = 2,
    Sloth = 3,
    Lust = 4,
    Gluttony = 5,
    Greed = 6,
    Saga = 7,
    Wadatsumi = 8,
    Desert = 9,
    Other = 100, // 0x00000064
  }
}
