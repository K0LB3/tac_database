﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TobiraLearnAbilityParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_TobiraLearnAbilityParam
  {
    public string abil_iname;
    public int learn_lv;
    public int add_type;
    public string abil_overwrite;
  }
}
