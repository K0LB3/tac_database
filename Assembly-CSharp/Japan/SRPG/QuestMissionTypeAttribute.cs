﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestMissionTypeAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class QuestMissionTypeAttribute : Attribute
  {
    private QuestMissionValueType m_ValueType;

    public QuestMissionTypeAttribute(QuestMissionValueType valueType)
    {
      this.m_ValueType = valueType;
    }

    public QuestMissionValueType ValueType
    {
      get
      {
        return this.m_ValueType;
      }
    }
  }
}
