﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqRaidRescueSend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  [FlowNode.NodeType("System/ReqRaid/Rescue/Send", 32741)]
  public class FlowNode_ReqRaidRescueSend : FlowNode_ReqRaidBase
  {
    public override WebAPI GenerateWebAPI()
    {
      RaidBossData currentRaidBossData = RaidManager.Instance.CurrentRaidBossData;
      return (WebAPI) new ReqRaidRescueSend(currentRaidBossData.AreaId, currentRaidBossData.RaidBossInfo.BossId, currentRaidBossData.RaidBossInfo.Round, new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback));
    }

    public override bool Success(WWWResult www)
    {
      RaidManager.Instance.CurrentRaidBossData.SOSDone();
      return true;
    }
  }
}
