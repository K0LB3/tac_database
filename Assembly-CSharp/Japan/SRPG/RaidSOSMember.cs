﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidSOSMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class RaidSOSMember
  {
    private string mFUID;
    private string mName;
    private int mLv;
    private UnitData mUnit;
    private RaidRescueMemberType mMemberType;
    private long mLastBattleTime;

    public string FUID
    {
      get
      {
        return this.mFUID;
      }
    }

    public string Name
    {
      get
      {
        return this.mName;
      }
    }

    public int Lv
    {
      get
      {
        return this.mLv;
      }
    }

    public UnitData Unit
    {
      get
      {
        return this.mUnit;
      }
    }

    public RaidRescueMemberType MemberType
    {
      get
      {
        return this.mMemberType;
      }
    }

    public long LastBattleTime
    {
      get
      {
        return this.mLastBattleTime;
      }
    }

    public bool Deserialize(JSON_RaidSOSMember json)
    {
      this.mFUID = json.fuid;
      this.mName = json.name;
      this.mLv = json.lv;
      this.mMemberType = (RaidRescueMemberType) json.member_type;
      this.mLastBattleTime = json.last_battle_time;
      if (json.unit != null)
      {
        this.mUnit = new UnitData();
        this.mUnit.Deserialize(json.unit);
      }
      return true;
    }
  }
}
