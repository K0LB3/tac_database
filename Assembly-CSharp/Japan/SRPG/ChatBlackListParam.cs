﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatBlackListParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ChatBlackListParam
  {
    public string name;
    public string uid;
    public long blocked_at;
    public long lastlogin;
    public string icon;
    public string skin_iname;
    public string job_iname;
    public int exp;
    public Json_Unit unit;
  }
}
