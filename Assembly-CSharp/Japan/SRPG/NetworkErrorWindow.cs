﻿// Decompiled with JetBrains decompiler
// Type: SRPG.NetworkErrorWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class NetworkErrorWindow : MonoBehaviour
  {
    [SerializeField]
    private Text Title;
    [SerializeField]
    private Text StatusCode;
    [SerializeField]
    private Text Message;
    [SerializeField]
    private Button m_Button;

    public NetworkErrorWindow()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
    }

    private void Start()
    {
      if (Object.op_Inequality((Object) this.Title, (Object) null))
        this.Title.set_text(LocalizedText.Get("embed.CONN_ERR"));
      if (Object.op_Inequality((Object) this.StatusCode, (Object) null))
      {
        if (GameUtility.IsDebugBuild)
        {
          this.StatusCode.set_text(LocalizedText.Get("embed.CONN_ERRCODE", new object[1]
          {
            (object) Network.ErrCode.ToString()
          }));
          ((Component) this.StatusCode).get_gameObject().SetActive(true);
        }
        else
          ((Component) this.StatusCode).get_gameObject().SetActive(false);
      }
      if (Object.op_Inequality((Object) this.Message, (Object) null))
      {
        if (string.IsNullOrEmpty(Network.ErrMsg))
          this.Message.set_text(LocalizedText.Get("embed.APP_REBOOT", new object[1]
          {
            (object) Network.ErrCode.ToString()
          }));
        else
          this.Message.set_text(Network.ErrMsg);
      }
      if (!Object.op_Inequality((Object) this.m_Button, (Object) null))
        return;
      // ISSUE: method pointer
      ((UnityEvent) this.m_Button.get_onClick()).AddListener(new UnityAction((object) this, __methodptr(OnClick)));
    }

    private void OnClick()
    {
      if (Network.ErrCode != Network.EErrCode.Authorize)
        return;
      MonoSingleton<GameManager>.Instance.ResetAuth();
    }

    public void OpenMaintenanceSite()
    {
      Application.OpenURL(Network.SiteHost);
    }

    public void OpenVersionUpSite()
    {
      Application.OpenURL(Network.SiteHost);
    }

    public void OpenStore()
    {
    }
  }
}
