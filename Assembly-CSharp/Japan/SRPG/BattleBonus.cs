﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BattleBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum BattleBonus
  {
    EffectRange,
    EffectScope,
    EffectHeight,
    HitRate,
    AvoidRate,
    CriticalRate,
    SlashAttack,
    PierceAttack,
    BlowAttack,
    ShotAttack,
    MagicAttack,
    ReactionAttack,
    JumpAttack,
    GainJewel,
    UsedJewelRate,
    ActionCount,
    GutsRate,
    AutoJewel,
    ChargeTimeRate,
    CastTimeRate,
    BuffTurn,
    DebuffTurn,
    CombinationRange,
    HpCostRate,
    SkillUseCount,
    PoisonDamage,
    PoisonTurn,
    Resist_Slash,
    Resist_Pierce,
    Resist_Blow,
    Resist_Shot,
    Resist_Magic,
    Resist_Reaction,
    Resist_Jump,
    Avoid_Slash,
    Avoid_Pierce,
    Avoid_Blow,
    Avoid_Shot,
    Avoid_Magic,
    Avoid_Reaction,
    Avoid_Jump,
    GainJewelRate,
    UsedJewel,
    UnitDefenseFire,
    UnitDefenseWater,
    UnitDefenseWind,
    UnitDefenseThunder,
    UnitDefenseShine,
    UnitDefenseDark,
  }
}
