﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidAreaParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidAreaParam : JSON_RaidMasterParam
  {
    public int id;
    public int order;
    public int period_id;
    public int boss_count;
    public int area_boss_id;
    public int clear_reward_id;
  }
}
