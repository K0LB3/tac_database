﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCardConditionsParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ConceptCardConditionsParam
  {
    public string iname;
    public int el_fire;
    public int el_watr;
    public int el_wind;
    public int el_thdr;
    public int el_lit;
    public int el_drk;
    public string un_group;
    public int units_cnds_type;
    public string job_group;
    public int jobs_cnds_type;
    public int sex;
    public int[] birth_id;
  }
}
