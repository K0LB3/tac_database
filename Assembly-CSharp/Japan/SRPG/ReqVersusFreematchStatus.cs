﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqVersusFreematchStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqVersusFreematchStatus : WebAPI
  {
    public ReqVersusFreematchStatus(Network.ResponseCallback response)
    {
      this.name = "vs/freematch/status";
      this.body = string.Empty;
      this.body = WebAPI.GetRequestString(this.body);
      this.callback = response;
    }

    public class EnableTimeSchedule
    {
      public long expired;
      public long next;
    }

    public class Response
    {
      public ReqVersusFreematchStatus.EnableTimeSchedule enabletime;
    }
  }
}
