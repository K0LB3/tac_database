﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_HasGold
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;

namespace SRPG
{
  public class ConditionsResult_HasGold : ConditionsResult
  {
    public ConditionsResult_HasGold(int condsNum)
    {
      this.mCurrentValue = MonoSingleton<GameManager>.Instance.Player.Gold;
      this.mTargetValue = condsNum;
      this.mIsClear = this.mCurrentValue >= this.mTargetValue;
    }

    public override string text
    {
      get
      {
        return string.Empty;
      }
    }

    public override string errorText
    {
      get
      {
        return LocalizedText.Get("sys.GOLD_NOT_ENOUGH");
      }
    }
  }
}
