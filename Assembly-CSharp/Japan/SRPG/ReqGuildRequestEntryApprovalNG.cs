﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRequestEntryApprovalNG
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildRequestEntryApprovalNG : WebAPI
  {
    public ReqGuildRequestEntryApprovalNG(string[] request_user_uids, Network.ResponseCallback response)
    {
      this.name = "guild/apply/reject";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"target_uids\":[");
      for (int index = 0; index < request_user_uids.Length; ++index)
      {
        stringBuilder.Append("\"" + request_user_uids[index] + "\"");
        if (index != request_user_uids.Length - 1)
          stringBuilder.Append(",");
      }
      stringBuilder.Append("]");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
