﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqRaidRescueCancel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  [FlowNode.NodeType("System/ReqRaid/Rescue/Cancel", 32741)]
  public class FlowNode_ReqRaidRescueCancel : FlowNode_ReqRaidBase
  {
    public override WebAPI GenerateWebAPI()
    {
      RaidBossData rescueRaidBossData = RaidManager.Instance.RescueRaidBossData;
      return (WebAPI) new ReqRaidRescueCancel(rescueRaidBossData.OwnerUID, rescueRaidBossData.AreaId, rescueRaidBossData.RaidBossInfo.BossId, rescueRaidBossData.RaidBossInfo.Round, new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback));
    }

    public override bool Success(WWWResult www)
    {
      return true;
    }
  }
}
