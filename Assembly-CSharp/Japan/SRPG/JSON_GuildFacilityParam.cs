﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildFacilityParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_GuildFacilityParam
  {
    public string iname;
    public string name;
    public string image;
    public int type;
    public int rel_cnds_type;
    public string rel_cnds_val1;
    public int rel_cnds_val2;
    public JSON_GuildFacilityEffectParam[] effects;
  }
}
