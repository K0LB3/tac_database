﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildSVB_Key
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class GuildSVB_Key
  {
    public static readonly string GUILD_ID = "guild_id";
    public static readonly string GUILD = "guild_data";
    public static readonly string COMMNAD = "guild_command";
    public static readonly string PLAYER = "player_data";
    public static readonly string CONFIRM = "guild_confirm";
    public static readonly string MEMBER = "member_data";
    public static readonly string VIEW_GUILD = "view_guild";
  }
}
