﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestMultiTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum QuestMultiTypes : short
  {
    NOT_MULTI = 0,
    RAID = 1,
    VERSUS = 2,
    EVENT_TOP = 100, // 0x0064
    RAID_EVENT = 101, // 0x0065
    VERSUS_EVENT = 102, // 0x0066
    TOWER_EVENT = 103, // 0x0067
  }
}
