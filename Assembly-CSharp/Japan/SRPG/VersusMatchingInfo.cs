﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusMatchingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Matching", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "DraftMatching", FlowNode.PinTypes.Input, 2)]
  public class VersusMatchingInfo : MonoBehaviour, IFlowInterface
  {
    public VersusMatchingInfo()
    {
      base.\u002Ector();
    }

    public void Start()
    {
      MonoSingleton<GameManager>.Instance.AudienceMode = false;
      GlobalVars.IsVersusDraftMode = false;
    }

    public void Activated(int pinID)
    {
      if (pinID == 1)
      {
        GlobalVars.IsVersusDraftMode = false;
        this.StartCoroutine(ProgressWindow.OpenVersusLoadScreenAsync());
      }
      else
      {
        if (pinID != 2)
          return;
        GlobalVars.IsVersusDraftMode = true;
        this.StartCoroutine(ProgressWindow.OpenVersusDraftLoadScreenAsync());
      }
    }
  }
}
