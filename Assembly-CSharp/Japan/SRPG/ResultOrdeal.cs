﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ResultOrdeal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class ResultOrdeal : MonoBehaviour
  {
    public ResultOrdeal()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      SceneBattle instance = SceneBattle.Instance;
      if (!Object.op_Implicit((Object) instance) || instance.ResultData == null)
        return;
      DataSource.Bind<BattleCore.Record>(((Component) this).get_gameObject(), instance.ResultData.Record);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
