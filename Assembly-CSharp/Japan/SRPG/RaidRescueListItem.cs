﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRescueListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class RaidRescueListItem : MonoBehaviour
  {
    private int mIndex;

    public RaidRescueListItem()
    {
      base.\u002Ector();
    }

    public int Index
    {
      get
      {
        return this.mIndex;
      }
    }

    public void Setup(int index, RaidRescueMember member)
    {
      this.mIndex = index;
      DataSource.Bind<RaidRescueMember>(((Component) this).get_gameObject(), member);
    }
  }
}
