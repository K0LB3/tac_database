﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitTobiraParamLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class UnitTobiraParamLevel : MonoBehaviour
  {
    [SerializeField]
    private GameObject OnGO;
    [SerializeField]
    private GameObject OffGO;
    [SerializeField]
    private int OwnLevel;

    public UnitTobiraParamLevel()
    {
      base.\u002Ector();
    }

    public void Refresh(int targetLevel)
    {
      if (Object.op_Equality((Object) this.OnGO, (Object) null) || Object.op_Equality((Object) this.OffGO, (Object) null))
        return;
      bool flag = targetLevel >= this.OwnLevel;
      this.OnGO.SetActive(flag);
      this.OffGO.SetActive(!flag);
    }
  }
}
