﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Quad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace SRPG
{
  [Serializable]
  public struct Quad
  {
    public Vector2 v0;
    public Color32 c0;
    public Vector2 v1;
    public Color32 c1;
    public Vector2 v2;
    public Color32 c2;
    public Vector2 v3;
    public Color32 c3;
  }
}
