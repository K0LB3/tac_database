﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_BtlComEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_BtlComEnd : Json_PlayerDataAll
  {
    public JSON_QuestProgress[] quests;
    public JSON_TrophyProgress[] trophyprogs;
    public Json_BtlQuestRanking quest_ranking;
    public Json_FirstClearItem[] fclr_items;
    public Json_BtlRewardConceptCard[] cards;
    public int is_mail_cards;
  }
}
