﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BaseIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SRPG
{
  public class BaseIcon : MonoBehaviour, IGameParameter, IPointerDownHandler, IHoldGesture, IEventSystemHandler
  {
    public BaseIcon()
    {
      base.\u002Ector();
    }

    public virtual bool HasTooltip
    {
      get
      {
        return true;
      }
    }

    protected virtual void ShowTooltip(Vector2 screen)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      HoldGestureObserver.StartHoldGesture((IHoldGesture) this);
    }

    public void OnPointerHoldStart()
    {
      if (!this.HasTooltip)
        return;
      RectTransform transform = (RectTransform) ((Component) this).get_transform();
      Vector2 screen = Vector2.op_Implicit(((Transform) transform).TransformPoint(Vector2.op_Implicit(Vector2.get_zero())));
      CanvasScaler componentInParent = (CanvasScaler) ((Component) transform).GetComponentInParent<CanvasScaler>();
      if (Object.op_Inequality((Object) componentInParent, (Object) null))
      {
        Vector3 localScale = ((Component) componentInParent).get_transform().get_localScale();
        ref Vector2 local1 = ref screen;
        local1.x = local1.x / localScale.x;
        ref Vector2 local2 = ref screen;
        local2.y = local2.y / localScale.y;
      }
      this.ShowTooltip(screen);
    }

    public void OnPointerHoldEnd()
    {
    }

    public virtual void UpdateValue()
    {
    }
  }
}
