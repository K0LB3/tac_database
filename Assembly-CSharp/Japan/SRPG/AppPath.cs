﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AppPath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public static class AppPath
  {
    public static string persistentDataPath
    {
      get
      {
        return Application.get_dataPath() + "/../data";
      }
    }

    public static string temporaryCachePath
    {
      get
      {
        return Application.get_dataPath() + "/../temp";
      }
    }

    public static string assetCachePath
    {
      get
      {
        return Application.get_dataPath() + "/..";
      }
    }

    public static string assetCachePathOld
    {
      get
      {
        return Application.get_dataPath() + "/..";
      }
    }
  }
}
