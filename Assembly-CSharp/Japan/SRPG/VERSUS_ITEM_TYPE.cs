﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VERSUS_ITEM_TYPE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum VERSUS_ITEM_TYPE
  {
    item,
    gold,
    coin,
    unit,
    artifact,
    award,
  }
}
