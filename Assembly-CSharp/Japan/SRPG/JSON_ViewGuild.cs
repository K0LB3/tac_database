﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ViewGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ViewGuild
  {
    public int id;
    public string name;
    public string award_id;
    public int level;
    public int count;
    public int max_count;
  }
}
