﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BattleUnitChg
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class BattleUnitChg : MonoBehaviour
  {
    public BattleUnitChg.SelectEvent OnSelectUnit;
    public RectTransform ListParent;
    public ListItemEvents UnitTemplate;
    public ListItemEvents EmptyTemplate;
    private const int SUB_UNIT_MAX = 2;
    private List<Unit> mSubUnitLists;
    private List<ListItemEvents> mUnitEvents;

    public BattleUnitChg()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (Object.op_Inequality((Object) this.UnitTemplate, (Object) null))
      {
        ((Component) this.UnitTemplate).get_gameObject().SetActive(false);
        if (Object.op_Equality((Object) this.ListParent, (Object) null))
          this.ListParent = ((Component) this.UnitTemplate).get_transform().get_parent() as RectTransform;
      }
      if (Object.op_Inequality((Object) this.EmptyTemplate, (Object) null))
        ((Component) this.EmptyTemplate).get_gameObject().SetActive(false);
      this.Refresh();
    }

    public void Refresh()
    {
      GameUtility.DestroyGameObjects<ListItemEvents>(this.mUnitEvents);
      this.mUnitEvents.Clear();
      if (Object.op_Equality((Object) this.UnitTemplate, (Object) null) || Object.op_Equality((Object) this.EmptyTemplate, (Object) null) || Object.op_Equality((Object) this.ListParent, (Object) null))
        return;
      SceneBattle instance = SceneBattle.Instance;
      BattleCore battleCore = (BattleCore) null;
      if (Object.op_Implicit((Object) instance))
        battleCore = instance.Battle;
      if (battleCore == null)
        return;
      this.mSubUnitLists.Clear();
      foreach (Unit unit in battleCore.Player)
      {
        if (unit != null && !unit.IsUnitFlag(EUnitFlag.CreatedBreakObj) && (!unit.IsUnitFlag(EUnitFlag.IsDynamicTransform) && !battleCore.StartingMembers.Contains(unit)))
          this.mSubUnitLists.Add(unit);
      }
      for (int index1 = 0; index1 < 2; ++index1)
      {
        if (index1 >= this.mSubUnitLists.Count)
        {
          ListItemEvents listItemEvents = (ListItemEvents) Object.Instantiate<ListItemEvents>((M0) this.EmptyTemplate);
          ((Component) listItemEvents).get_transform().SetParent((Transform) this.ListParent, false);
          this.mUnitEvents.Add(listItemEvents);
          ((Component) listItemEvents).get_gameObject().SetActive(true);
        }
        else
        {
          Unit mSubUnitList = this.mSubUnitLists[index1];
          if (mSubUnitList != null)
          {
            ListItemEvents listItemEvents = (ListItemEvents) Object.Instantiate<ListItemEvents>((M0) this.UnitTemplate);
            DataSource.Bind<Unit>(((Component) listItemEvents).get_gameObject(), mSubUnitList);
            ((Component) listItemEvents).get_transform().SetParent((Transform) this.ListParent, false);
            this.mUnitEvents.Add(listItemEvents);
            ((Component) listItemEvents).get_gameObject().SetActive(true);
            bool flag = !mSubUnitList.IsDead && mSubUnitList.IsEntry && mSubUnitList.IsSub;
            Selectable[] componentsInChildren = (Selectable[]) ((Component) listItemEvents).get_gameObject().GetComponentsInChildren<Selectable>(true);
            if (componentsInChildren != null)
            {
              for (int index2 = componentsInChildren.Length - 1; index2 >= 0; --index2)
                componentsInChildren[index2].set_interactable(flag);
            }
            listItemEvents.OnSelect = (ListItemEvents.ListItemEvent) (go =>
            {
              if (this.OnSelectUnit == null)
                return;
              Unit dataOfClass = DataSource.FindDataOfClass<Unit>(go, (Unit) null);
              if (dataOfClass == null)
                return;
              this.OnSelectUnit(dataOfClass);
            });
          }
        }
      }
    }

    public delegate void SelectEvent(Unit unit);
  }
}
