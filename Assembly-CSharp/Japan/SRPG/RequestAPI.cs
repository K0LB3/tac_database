﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RequestAPI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class RequestAPI : WebAPI
  {
    public RequestAPI(string url, Network.ResponseCallback response, string text)
    {
      this.name = url;
      this.body = WebAPI.GetRequestString(text);
      this.callback = response;
    }
  }
}
