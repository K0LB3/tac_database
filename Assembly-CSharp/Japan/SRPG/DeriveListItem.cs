﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DeriveListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class DeriveListItem : MonoBehaviour
  {
    [HeaderBar("▼派生先のスキル/アビリティの罫線")]
    [SerializeField]
    private RectTransform m_DeriveLineV;
    [SerializeField]
    private RectTransform m_DeriveLineH;

    public DeriveListItem()
    {
      base.\u002Ector();
    }

    public void SetLineActive(bool lineActive, bool verticalActive)
    {
      GameUtility.SetGameObjectActive((Component) this.m_DeriveLineH, lineActive);
      if (lineActive)
        GameUtility.SetGameObjectActive((Component) this.m_DeriveLineV, verticalActive);
      else
        GameUtility.SetGameObjectActive((Component) this.m_DeriveLineV, false);
    }
  }
}
