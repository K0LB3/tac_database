﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TobiraRecipeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_TobiraRecipeParam
  {
    public string recipe_iname;
    public int tobira_lv;
    public int cost;
    public int unit_piece_num;
    public int piece_elem_num;
    public int unlock_elem_num;
    public int unlock_birth_num;
    public JSON_TobiraRecipeMaterialParam[] mats;
  }
}
