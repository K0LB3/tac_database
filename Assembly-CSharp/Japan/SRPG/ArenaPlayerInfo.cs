﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class ArenaPlayerInfo : MonoBehaviour
  {
    [Space(10f)]
    public GameObject unit1;
    public GameObject unit2;
    public GameObject unit3;

    public ArenaPlayerInfo()
    {
      base.\u002Ector();
    }

    private void OnEnable()
    {
      this.UpdateValue();
    }

    public void UpdateValue()
    {
      ArenaPlayer dataOfClass = DataSource.FindDataOfClass<ArenaPlayer>(((Component) this).get_gameObject(), (ArenaPlayer) null);
      if (dataOfClass == null)
        return;
      DataSource.Bind<ArenaPlayer>(this.unit1, dataOfClass);
      DataSource.Bind<ArenaPlayer>(this.unit2, dataOfClass);
      DataSource.Bind<ArenaPlayer>(this.unit3, dataOfClass);
      ((BaseIcon) this.unit1.GetComponent<UnitIcon>()).UpdateValue();
      ((BaseIcon) this.unit2.GetComponent<UnitIcon>()).UpdateValue();
      ((BaseIcon) this.unit3.GetComponent<UnitIcon>()).UpdateValue();
    }
  }
}
