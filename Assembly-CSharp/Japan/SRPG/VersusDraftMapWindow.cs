﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusDraftMapWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class VersusDraftMapWindow : MonoBehaviour
  {
    public VersusDraftMapWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      QuestParam quest = instance.FindQuest(instance.VSDraftQuestId);
      if (quest == null)
        return;
      DataSource.Bind<QuestParam>(((Component) this).get_gameObject(), quest);
    }
  }
}
