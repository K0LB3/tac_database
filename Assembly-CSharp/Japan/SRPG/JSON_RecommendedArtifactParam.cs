﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RecommendedArtifactParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RecommendedArtifactParam
  {
    public string unit_iname;
    public string job_iname;
    public string arti1_iname;
    public string arti2_iname;
    public string arti3_iname;
  }
}
