﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqGuildMemberKick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Guild/ReqGuildMemberKick", 32741)]
  [FlowNode.Pin(1, "ギルドメンバー追放", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "ギルドメンバー追放完了", FlowNode.PinTypes.Output, 101)]
  public class FlowNode_ReqGuildMemberKick : FlowNode_Network
  {
    private const int PIN_INPUT_START_GUILD_MEMBER_KICK = 1;
    private const int PIN_OUTPUT_END_GUILD_MEMBER_KICK = 101;

    public override void OnActivate(int pinID)
    {
      GuildConfirm instance = GuildConfirm.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null))
        return;
      if (pinID == 1)
        this.ExecRequest((WebAPI) new ReqGuildMemberKick(instance.TargetMember.Uid, new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback)));
      ((Behaviour) this).set_enabled(true);
    }

    public override void OnSuccess(WWWResult www)
    {
      if (Network.IsError)
      {
        int errCode = (int) Network.ErrCode;
        this.OnRetry();
      }
      else
      {
        WebAPI.JSON_BodyResponse<FlowNode_ReqGuildMemberKick.Json_ResGuildMemberKick> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<FlowNode_ReqGuildMemberKick.Json_ResGuildMemberKick>>(www.text);
        DebugUtility.Assert(jsonObject != null, "res == null");
        Network.RemoveAPI();
        try
        {
          MonoSingleton<GameManager>.Instance.Player.Deserialize(jsonObject.body.guild);
        }
        catch (Exception ex)
        {
          DebugUtility.LogException(ex);
          return;
        }
        this.ActivateOutputLinks(101);
        ((Behaviour) this).set_enabled(false);
      }
    }

    public class Json_ResGuildMemberKick
    {
      public JSON_Guild guild;
    }
  }
}
