﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VersusEnableTimeScheduleParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_VersusEnableTimeScheduleParam
  {
    public string begin_time;
    public string open_time;
    public string quest_iname;
    public string[] add_date;
  }
}
