﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EAbilityTypeDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum EAbilityTypeDetail
  {
    Default,
    Job_Unique,
    Job_Basic,
    Job_Support,
    Job_Reaction,
    MasterAbility,
    CollaboAbility,
    WeaponAbility,
    VisionAbility,
    MapEffectAbility,
    TobiraAbility,
  }
}
