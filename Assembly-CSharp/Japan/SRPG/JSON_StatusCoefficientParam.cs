﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_StatusCoefficientParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_StatusCoefficientParam
  {
    public float hp;
    public float atk;
    public float def;
    public float matk;
    public float mdef;
    public float dex;
    public float spd;
    public float cri;
    public float luck;
    public float cmb;
    public float move;
    public float jmp;
  }
}
