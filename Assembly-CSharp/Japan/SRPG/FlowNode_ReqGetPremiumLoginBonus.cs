﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqGetPremiumLoginBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("System/ReqGetPremiumLoginBonus", 32741)]
  [FlowNode.Pin(1, "プレミアムログインボーナス情報をリクエスト", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "すでに取得している", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "リクエストを出して取得", FlowNode.PinTypes.Output, 102)]
  public class FlowNode_ReqGetPremiumLoginBonus : FlowNode_Network
  {
    private const int INPUT_GET_PREMIUM_LOGINBONUS = 1;
    private const int OUTPUT_AlREADY_GET_LOGINBONUS = 101;
    private const int OUTPUT_REQUEST_GET_LOGINBONUS = 102;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      if (MonoSingleton<GameManager>.Instance.Player.PremiumLoginBonus != null)
      {
        this.ActivateOutputLinks(101);
      }
      else
      {
        ((Behaviour) this).set_enabled(true);
        this.ExecRequest((WebAPI) new ReqGetPremiumLoginBonus(new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback)));
      }
    }

    public override void OnSuccess(WWWResult www)
    {
      if (Network.IsError)
      {
        int errCode = (int) Network.ErrCode;
        this.OnRetry();
      }
      else
      {
        WebAPI.JSON_BodyResponse<Json_LoginBonusTable> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<Json_LoginBonusTable>>(www.text);
        DebugUtility.Assert(jsonObject != null, "res == null");
        Network.RemoveAPI();
        try
        {
          MonoSingleton<GameManager>.Instance.Player.SetPremiumLoginBonus(jsonObject.body);
        }
        catch (Exception ex)
        {
          DebugUtility.LogException(ex);
          return;
        }
        this.ActivateOutputLinks(102);
        ((Behaviour) this).set_enabled(false);
      }
    }
  }
}
