﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidCompleteRewardDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class RaidCompleteRewardDataParam
  {
    private int mRound;
    private string mRewardId;

    public int Round
    {
      get
      {
        return this.mRound;
      }
    }

    public string RewardId
    {
      get
      {
        return this.mRewardId;
      }
    }

    public bool Deserialize(JSON_RaidCompleteRewardDataParam json)
    {
      if (json == null)
        return false;
      this.mRound = json.round;
      this.mRewardId = json.reward_id;
      return true;
    }
  }
}
