﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidRankingBeat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidRankingBeat : WebAPI
  {
    public ReqRaidRankingBeat(Network.ResponseCallback response)
    {
      this.name = "raidboss/ranking/beat";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public Json_RaidRankingList beat;
      public Json_RaidRankingList rescue;
    }
  }
}
