﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GachaHistoryItemData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class GachaHistoryItemData
  {
    private GachaHistoryData[] mHistorys;
    private string mGachaTitle;
    private long mDropAt;

    public GachaHistoryItemData(GachaHistoryData[] historys, string title, long drop_at)
    {
      this.mHistorys = historys;
      this.mGachaTitle = title;
      this.mDropAt = drop_at;
    }

    public GachaHistoryData[] historys
    {
      get
      {
        return this.mHistorys;
      }
    }

    public string gachaTitle
    {
      get
      {
        return this.mGachaTitle;
      }
    }

    public long drop_at
    {
      get
      {
        return this.mDropAt;
      }
    }

    public DateTime GetDropAt()
    {
      return GameUtility.UnixtimeToLocalTime(this.mDropAt);
    }
  }
}
