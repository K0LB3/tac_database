﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatLogParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ChatLogParam
  {
    public long id;
    public byte message_type;
    public string fuid;
    public string uid;
    public string icon;
    public string skin_iname;
    public string job_iname;
    public string message;
    public int stamp_id;
    public string name;
    public long posted_at;
    public long gid;
    public string guild_name;
    public int lower_level;
    public int is_auto_approval;
  }
}
