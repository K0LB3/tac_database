﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidRescueCancel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidRescueCancel : WebAPI
  {
    public ReqRaidRescueCancel(string uid, int area_id, int boss_id, int round, Network.ResponseCallback response)
    {
      this.name = "raidboss/rescue/cancel";
      this.body = WebAPI.GetRequestString<ReqRaidRescueCancel.RequestParam>(new ReqRaidRescueCancel.RequestParam()
      {
        uid = uid,
        area_id = area_id,
        boss_id = boss_id,
        round = round
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string uid;
      public int area_id;
      public int boss_id;
      public int round;
    }
  }
}
