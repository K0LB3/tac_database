﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AbilityDeriveParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class AbilityDeriveParam : BaseDeriveParam<AbilityParam>
  {
    public string BaseAbilityIname
    {
      get
      {
        return this.m_BaseParam.iname;
      }
    }

    public string DeriveAbilityIname
    {
      get
      {
        return this.m_DeriveParam.iname;
      }
    }

    public string BaseAbilityName
    {
      get
      {
        return this.m_BaseParam.name;
      }
    }

    public string DeriveAbilityName
    {
      get
      {
        return this.m_DeriveParam.name;
      }
    }
  }
}
