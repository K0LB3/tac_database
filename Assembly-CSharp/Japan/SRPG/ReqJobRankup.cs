﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqJobRankup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ReqJobRankup : WebAPI
  {
    public ReqJobRankup(long iid_job, Network.ResponseCallback response)
    {
      this.name = "unit/job/equip/lvup/";
      this.body = WebAPI.GetRequestString("\"iid\":" + (object) iid_job);
      this.callback = response;
    }
  }
}
