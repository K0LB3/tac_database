﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AwardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_AwardParam
  {
    public int id;
    public string iname;
    public string name;
    public string expr;
    public string icon;
    public string bg;
    public string txt_img;
    public string start_at;
    public int grade;
    public int tab;
  }
}
