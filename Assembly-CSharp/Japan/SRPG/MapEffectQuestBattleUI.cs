﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MapEffectQuestBattleUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class MapEffectQuestBattleUI : MonoBehaviour
  {
    public SRPG_Button ButtonMapEffect;
    public string PrefabMapEffectQuest;
    private LoadRequest mReqMapEffect;

    public MapEffectQuestBattleUI()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (!Object.op_Implicit((Object) this.ButtonMapEffect))
        return;
      this.ButtonMapEffect.AddListener((SRPG_Button.ButtonClickEvent) (button => this.ReqOpenMapEffect()));
    }

    private void ReqOpenMapEffect()
    {
      this.StartCoroutine(this.OpenMapEffect());
    }

    [DebuggerHidden]
    private IEnumerator OpenMapEffect()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new MapEffectQuestBattleUI.\u003COpenMapEffect\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
