﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EShopType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public enum EShopType
  {
    Normal,
    Tabi,
    Kimagure,
    Monozuki,
    Tour,
    Arena,
    Multi,
    AwakePiece,
    Artifact,
    Event,
    Limited,
    Guerrilla,
  }
}
