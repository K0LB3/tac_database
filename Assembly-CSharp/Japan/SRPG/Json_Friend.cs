﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_Friend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class Json_Friend
  {
    public string uid;
    public string fuid;
    public string name;
    public string type;
    public int lv;
    public long lastlogin;
    public int is_multi_push;
    public string multi_comment;
    public Json_Unit unit;
    public string created_at;
    public int is_favorite;
    public string award;
    public string wish;
    public string status;
    public JSON_ViewGuild guild;
  }
}
