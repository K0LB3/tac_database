﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuerrillaShopAdventQuestParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class GuerrillaShopAdventQuestParam
  {
    public int id;
    public string qid;

    public bool Deserialize(JSON_GuerrillaShopAdventQuestParam json)
    {
      this.id = json.id;
      this.qid = json.qid;
      return true;
    }
  }
}
