﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ToggledPulldownItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class ToggledPulldownItem : PulldownItem
  {
    public GameObject imageOn;
    public GameObject imageOff;

    public override void OnStatusChanged(bool enabled)
    {
      if (Object.op_Inequality((Object) this.imageOn, (Object) null))
        this.imageOn.SetActive(enabled);
      if (!Object.op_Inequality((Object) this.imageOff, (Object) null))
        return;
      this.imageOff.SetActive(!enabled);
    }
  }
}
