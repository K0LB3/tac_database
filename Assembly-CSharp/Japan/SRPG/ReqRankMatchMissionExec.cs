﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRankMatchMissionExec
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRankMatchMissionExec : WebAPI
  {
    public ReqRankMatchMissionExec(string iname, Network.ResponseCallback response)
    {
      this.name = "vs/rankmatch/mission/exec";
      this.body = WebAPI.GetRequestString<ReqRankMatchMissionExec.RequestParam>(new ReqRankMatchMissionExec.RequestParam()
      {
        iname = iname
      });
      this.callback = response;
    }

    [Serializable]
    private class RequestParam
    {
      public string iname;
    }
  }
}
