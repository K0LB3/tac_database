﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LogFall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;

namespace SRPG
{
  public class LogFall : BattleLog
  {
    public List<LogFall.Param> mLists = new List<LogFall.Param>();
    public bool mIsPlayDamageMotion;

    public void Add(Unit self, Grid landing = null)
    {
      this.mLists.Add(new LogFall.Param()
      {
        mSelf = self,
        mLanding = landing
      });
    }

    public struct Param
    {
      public Unit mSelf;
      public Grid mLanding;
    }
  }
}
