﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(10, "チャットウィンドウを開く", FlowNode.PinTypes.Input, 10)]
  public class GuildActivity : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_OPEN_CHATWINDOW = 10;
    [SerializeField]
    private float REFRESH_TIMEING;
    [SerializeField]
    private ChatLogItem mLogTemplate;
    [SerializeField]
    private ScrollRect mScrollRect;
    private readonly float FIRST_REFRESH_TIMING;
    private List<ChatLogItem> mCreatedChatLogItems;
    private float mRefreshTiming;
    private float mElapsedTime;
    private long mLastMessageId;
    private bool mIsNeedUpdateScroll;

    public GuildActivity()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 10)
        return;
      this.OpenChatWindow();
    }

    private void Awake()
    {
      this.mElapsedTime = this.REFRESH_TIMEING;
    }

    private void Start()
    {
      MonoSingleton<ChatWindow>.Instance.RequestGuildChatLog();
    }

    private void Update()
    {
      if (Object.op_Equality((Object) MonoSingleton<ChatWindow>.Instance, (Object) null))
        return;
      if (MonoSingleton<ChatWindow>.Instance.IsClose)
        MonoSingleton<ChatWindow>.Instance.SetActiveOpenCloseButton(false);
      this.CheckUpdateScroll();
      this.mRefreshTiming = this.mCreatedChatLogItems.Count > 0 ? this.REFRESH_TIMEING : this.FIRST_REFRESH_TIMING;
      this.mElapsedTime += Time.get_deltaTime();
      if ((double) this.mElapsedTime < (double) this.mRefreshTiming)
        return;
      this.mElapsedTime = 0.0f;
      this.Refresh();
    }

    public void Refresh()
    {
      ((Component) this.mLogTemplate).get_gameObject().SetActive(false);
      ChatLog chatLogInstance = MonoSingleton<ChatWindow>.Instance.GetChatLogInstance(ChatWindow.eChatType.Guild);
      if (chatLogInstance == null)
        return;
      for (int index = 0; index < this.mCreatedChatLogItems.Count; ++index)
        ((Component) this.mCreatedChatLogItems[index]).get_gameObject().SetActive(false);
      int num = chatLogInstance.messages.Count - this.mCreatedChatLogItems.Count;
      for (int index = 0; index < num; ++index)
      {
        ChatLogItem chatLogItem = (ChatLogItem) Object.Instantiate<ChatLogItem>((M0) this.mLogTemplate);
        ((Component) chatLogItem).get_transform().SetParent(((Component) this.mLogTemplate).get_transform().get_parent(), false);
        this.mCreatedChatLogItems.Add(chatLogItem);
      }
      for (int index = 0; index < chatLogInstance.messages.Count; ++index)
      {
        ((Component) this.mCreatedChatLogItems[index]).get_gameObject().SetActive(true);
        this.mCreatedChatLogItems[index].SetParam(chatLogInstance.messages[index], new SRPG_Button.ButtonClickEvent(this.OnTapUnitIcon));
      }
      if (this.mLastMessageId != chatLogInstance.LastMessageIdServer && Object.op_Inequality((Object) this.mScrollRect, (Object) null))
      {
        this.mIsNeedUpdateScroll = true;
        this.mLastMessageId = chatLogInstance.LastMessageIdServer;
      }
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    private void CheckUpdateScroll()
    {
      if (!this.mIsNeedUpdateScroll)
        return;
      this.mScrollRect.set_verticalNormalizedPosition(0.0f);
      this.mIsNeedUpdateScroll = false;
    }

    private void OpenChatWindow()
    {
      if (Object.op_Equality((Object) MonoSingleton<ChatWindow>.Instance, (Object) null))
        return;
      MonoSingleton<ChatWindow>.Instance.ChangeChatTypeTab(ChatWindow.eChatType.Guild);
      MonoSingleton<ChatWindow>.Instance.Open();
      MonoSingleton<ChatWindow>.Instance.SetActiveOpenCloseButton(true);
    }

    private void OnTapUnitIcon(SRPG_Button button)
    {
      if (Object.op_Equality((Object) button, (Object) null))
        return;
      ChatLogItem componentInParent = (ChatLogItem) ((Component) button).GetComponentInParent<ChatLogItem>();
      if (Object.op_Equality((Object) componentInParent, (Object) null) || componentInParent.ChatLogParam == null || string.IsNullOrEmpty(componentInParent.ChatLogParam.uid))
        return;
      FlowNode_Variable.Set("SelectUserID", componentInParent.ChatLogParam.uid);
      FlowNode_Variable.Set("IsBlackList", "0");
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 30);
    }
  }
}
