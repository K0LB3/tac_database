﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ShopTimeOutItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class ShopTimeOutItemInfo
  {
    public string ShopId;
    public int ItemId;
    public long End;

    public ShopTimeOutItemInfo(string shopId, int itemId, long end)
    {
      this.ShopId = shopId;
      this.ItemId = itemId;
      this.End = end;
    }
  }
}
