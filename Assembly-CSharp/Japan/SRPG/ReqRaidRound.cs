﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidRound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidRound : WebAPI
  {
    public ReqRaidRound(Network.ResponseCallback response)
    {
      this.name = "raidboss/round";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public int round;
    }
  }
}
