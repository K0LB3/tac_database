﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayTrickParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class MultiPlayTrickParam
  {
    public string tid;
    public bool val;
    public int cun;
    public int rnk;
    public int rcp;
    public int grx;
    public int gry;
    public int rac;
    public int ccl;
    public string tag;
  }
}
