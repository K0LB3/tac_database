﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VersusSchedule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_VersusSchedule
  {
    public string tower_iname;
    public string iname;
    public string begin_at;
    public string end_at;
    public string gift_begin_at;
    public string gift_end_at;
  }
}
