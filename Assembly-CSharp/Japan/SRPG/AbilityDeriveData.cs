﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AbilityDeriveData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class AbilityDeriveData
  {
    private AbilityDeriveParam m_Param;
    private bool m_IsAdd;
    private bool m_IsDisable;

    public AbilityDeriveData(AbilityDeriveParam param)
    {
      this.m_Param = param;
    }

    public bool IsAdd
    {
      get
      {
        return this.m_IsAdd;
      }
      set
      {
        this.m_IsAdd = value;
      }
    }

    public bool IsDisable
    {
      get
      {
        return this.m_IsDisable;
      }
      set
      {
        this.m_IsDisable = value;
      }
    }

    public AbilityDeriveParam Param
    {
      get
      {
        return this.m_Param;
      }
    }
  }
}
