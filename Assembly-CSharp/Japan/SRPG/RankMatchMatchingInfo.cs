﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RankMatchMatchingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Matching", FlowNode.PinTypes.Input, 1)]
  public class RankMatchMatchingInfo : MonoBehaviour, IFlowInterface
  {
    public RankMatchMatchingInfo()
    {
      base.\u002Ector();
    }

    public void Start()
    {
      MonoSingleton<GameManager>.Instance.AudienceMode = false;
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.StartCoroutine(ProgressWindow.OpenRankMatchLoadScreenAsync());
    }
  }
}
