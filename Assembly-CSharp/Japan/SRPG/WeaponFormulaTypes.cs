﻿// Decompiled with JetBrains decompiler
// Type: SRPG.WeaponFormulaTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum WeaponFormulaTypes
  {
    None,
    Atk,
    Mag,
    AtkSpd,
    MagSpd,
    AtkDex,
    MagDex,
    AtkLuk,
    MagLuk,
    AtkMag,
    SpAtk,
    SpMag,
    AtkSpdDex,
    MagSpdDex,
    AtkDexLuk,
    MagDexLuk,
    Luk,
    Dex,
    Spd,
    Cri,
    Def,
    Mnd,
    AtkRndLuk,
    MagRndLuk,
    AtkEAt,
    MagEMg,
    AtkDefEDf,
    MagMndEMd,
    LukELk,
    MHp,
  }
}
