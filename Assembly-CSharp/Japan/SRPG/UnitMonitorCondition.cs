﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitMonitorCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class UnitMonitorCondition
  {
    public string iname = string.Empty;
    public string tag = string.Empty;
    public int x = -1;
    public int y = -1;
    public int turn;
  }
}
