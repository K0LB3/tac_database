﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidDamageRatioRewardWeightParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class RaidDamageRatioRewardWeightParam
  {
    private int mDamageRatio;
    private string mRewardId;

    public int DamageRatio
    {
      get
      {
        return this.mDamageRatio;
      }
    }

    public string RewardId
    {
      get
      {
        return this.mRewardId;
      }
    }

    public bool Deserialize(JSON_RaidDamageRatioRewardRatioParam json)
    {
      if (json == null)
        return false;
      this.mDamageRatio = json.damage_ratio;
      this.mRewardId = json.reward_id;
      return true;
    }
  }
}
