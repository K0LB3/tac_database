﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TrophyTabList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class TrophyTabList : MonoBehaviour
  {
    [SerializeField]
    private int CREATE_TAB_COUNT_MAX;
    [SerializeField]
    private TrophyCategorys TrophyCategory;
    [SerializeField]
    private TrophyWindow TrophyWindow;
    [SerializeField]
    private TrophyList TrophyList;
    [SerializeField]
    private Transform TabHolder;
    [SerializeField]
    private TrophyRecordTab OriginalTab;
    [SerializeField]
    private ScrollRect ScrollRect;
    [SerializeField]
    private Button ButtonGetAll;
    private List<TrophyRecordTab> mTabs;
    private int mCurrentTabIndex;
    private CanvasGroup mCanvasGroup;

    public TrophyTabList()
    {
      base.\u002Ector();
    }

    private void OnEnable()
    {
      this.ScrollRect.set_normalizedPosition(new Vector2(1f, 1f));
      this.CreateTrophyRecord(true);
      MonoSingleton<GameManager>.Instance.OnDayChange += new GameManager.DayChangeEvent(this.OnTrophyReset);
    }

    private void OnDisable()
    {
      this.ClearAllItems();
      if (Object.op_Inequality((Object) MonoSingleton<GameManager>.GetInstanceDirect(), (Object) null))
        MonoSingleton<GameManager>.Instance.OnDayChange -= new GameManager.DayChangeEvent(this.OnTrophyReset);
      this.ScrollRect.set_normalizedPosition(new Vector2(1f, 1f));
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.OriginalTab, (Object) null))
        ((Component) this.OriginalTab).get_gameObject().SetActive(false);
      this.mCanvasGroup = (CanvasGroup) ((Component) this).GetComponent<CanvasGroup>();
      if (!Object.op_Equality((Object) this.mCanvasGroup, (Object) null))
        return;
      this.mCanvasGroup = (CanvasGroup) ((Component) this).get_gameObject().AddComponent<CanvasGroup>();
    }

    private void Update()
    {
      if (Object.op_Inequality((Object) this.mCanvasGroup, (Object) null) && (double) this.mCanvasGroup.get_alpha() < 1.0)
        this.mCanvasGroup.set_alpha(Mathf.Clamp01(this.mCanvasGroup.get_alpha() + Time.get_unscaledDeltaTime() * 3.333333f));
      if (!Object.op_Inequality((Object) this.ButtonGetAll, (Object) null) || this.mTabs.Count <= 0)
        return;
      bool isInCompletedData = this.mTabs[this.mCurrentTabIndex].CategoryData.IsInCompletedData;
      if (isInCompletedData == ((Selectable) this.ButtonGetAll).get_interactable())
        return;
      ((Selectable) this.ButtonGetAll).set_interactable(isInCompletedData);
    }

    private void OnTrophyReset()
    {
      this.CreateTrophyRecord(true);
    }

    private void ClearAllItems()
    {
      foreach (Component mTab in this.mTabs)
        Object.Destroy((Object) mTab.get_gameObject());
      this.mTabs.Clear();
    }

    public TrophyRecordTab GetCurrentSelectionTab()
    {
      return this.mTabs[this.mCurrentTabIndex];
    }

    public void CreateTrophyRecord(bool resetSelection = true)
    {
      if (Object.op_Inequality((Object) this.mCanvasGroup, (Object) null))
        this.mCanvasGroup.set_alpha(0.0f);
      this.ClearAllItems();
      if (!this.TrophyWindow.TrophyRecordDatas.ContainsKey(this.TrophyCategory))
      {
        ((Selectable) this.ButtonGetAll).set_interactable(false);
        this.TrophyList.ClearAllItems();
      }
      else
      {
        List<TrophyCategoryData> trophyRecordData = this.TrophyWindow.TrophyRecordDatas[this.TrophyCategory];
        if (trophyRecordData == null || trophyRecordData.Count <= 0)
        {
          ((Selectable) this.ButtonGetAll).set_interactable(false);
          this.TrophyList.ClearAllItems();
        }
        else
        {
          int createTabCountMax = this.CREATE_TAB_COUNT_MAX;
          SpriteSheet spriteSheet = AssetManager.Load<SpriteSheet>("RecordMission/RecordMission_Images");
          for (int _index = 0; _index < trophyRecordData.Count && createTabCountMax != 0; ++_index)
          {
            TrophyCategoryData _category_data = trophyRecordData[_index];
            Sprite banner = (Sprite) null;
            if (Object.op_Inequality((Object) spriteSheet, (Object) null) && _category_data.Param.banner != null)
              banner = spriteSheet.GetSprite(_category_data.Param.banner);
            TrophyRecordTab trophyRecordTab = this.MakeTrophyCategoryTab(_category_data.Param.iname, banner);
            if (Object.op_Inequality((Object) trophyRecordTab, (Object) null))
            {
              --createTabCountMax;
              trophyRecordTab.Setup(_index);
              trophyRecordTab.SetCategoryData(_category_data);
              trophyRecordTab.RefreshDisplayParam();
              this.mTabs.Add(trophyRecordTab);
              ((SRPG_Button) ((Component) trophyRecordTab).GetComponent<SRPG_Button>()).AddListener(new SRPG_Button.ButtonClickEvent(this.OnItemSelect));
            }
            if (this.mTabs.Count >= this.CREATE_TAB_COUNT_MAX)
              break;
          }
          this.mCurrentTabIndex = !resetSelection ? (this.mTabs.Count <= 0 || this.mCurrentTabIndex <= this.mTabs.Count - 1 ? 0 : this.mTabs.Count - 1) : 0;
          this.ActivateAllTabCursor(this.mCurrentTabIndex);
          TrophyRecordTab mTab = this.mTabs[this.mCurrentTabIndex];
          mTab.SetCursor(true);
          ((Selectable) this.ButtonGetAll).set_interactable(mTab.CategoryData.IsInCompletedData);
          this.TrophyList.CreateTrophies(mTab.CategoryData.Trophies);
          LayoutRebuilder.MarkLayoutForRebuild(((Component) this.ScrollRect).get_transform() as RectTransform);
        }
      }
    }

    public void RefreshTrophyRecord()
    {
      if (Object.op_Inequality((Object) this.mCanvasGroup, (Object) null))
        this.mCanvasGroup.set_alpha(0.0f);
      if (!this.TrophyWindow.TrophyRecordDatas.ContainsKey(this.TrophyCategory))
      {
        this.ClearAllItems();
        ((Selectable) this.ButtonGetAll).set_interactable(false);
        this.TrophyList.ClearAllItems();
      }
      else
      {
        List<TrophyCategoryData> trophyRecordData = this.TrophyWindow.TrophyRecordDatas[this.TrophyCategory];
        if (trophyRecordData == null || trophyRecordData.Count <= 0)
        {
          this.ClearAllItems();
          ((Selectable) this.ButtonGetAll).set_interactable(false);
          this.TrophyList.ClearAllItems();
        }
        else
        {
          int count = this.mTabs.Count;
          for (int index1 = 0; index1 < trophyRecordData.Count; ++index1)
          {
            for (int index2 = 0; index2 < this.mTabs.Count; ++index2)
            {
              if (trophyRecordData[index1].Param.hash_code == this.mTabs[index2].HashCode)
              {
                this.mTabs[index2].SetCategoryData(trophyRecordData[index1]);
                --count;
              }
            }
          }
          if (count != 0)
          {
            this.CreateTrophyRecord(false);
          }
          else
          {
            for (int index = 0; index < this.mTabs.Count; ++index)
              this.mTabs[index].RefreshDisplayParam();
            this.ActivateAllTabCursor(this.mCurrentTabIndex);
            TrophyRecordTab mTab = this.mTabs[this.mCurrentTabIndex];
            mTab.SetCursor(true);
            ((Selectable) this.ButtonGetAll).set_interactable(mTab.CategoryData.IsInCompletedData);
            this.TrophyList.CreateTrophies(mTab.CategoryData.Trophies);
          }
        }
      }
    }

    private TrophyRecordTab MakeTrophyCategoryTab(string title, Sprite banner = null)
    {
      TrophyRecordTab component = (TrophyRecordTab) ((GameObject) Object.Instantiate<GameObject>((M0) ((Component) this.OriginalTab).get_gameObject())).GetComponent<TrophyRecordTab>();
      component.Init(title, banner);
      ((Component) component).get_transform().SetParent(this.TabHolder, false);
      return component;
    }

    private void OnItemSelect(SRPG_Button button)
    {
      TrophyRecordTab component = (TrophyRecordTab) ((Component) button).GetComponent<TrophyRecordTab>();
      if (component.Index == this.mCurrentTabIndex)
        return;
      this.mCurrentTabIndex = component.Index;
      this.ActivateAllTabCursor(this.mCurrentTabIndex);
      component.SetCursor(true);
      TrophyCategoryData categoryData = component.CategoryData;
      this.TrophyList.CreateTrophies(categoryData.Trophies);
      ((Selectable) this.ButtonGetAll).set_interactable(categoryData.IsInCompletedData);
    }

    private void ActivateAllTabCursor(int activeIndex)
    {
      for (int index = 0; index < this.mTabs.Count; ++index)
        this.mTabs[index].SetCursor(index == activeIndex);
    }
  }
}
