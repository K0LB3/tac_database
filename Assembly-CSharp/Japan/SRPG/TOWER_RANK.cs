﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TOWER_RANK
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum TOWER_RANK
  {
    S,
    A_PLUS,
    A,
    A_MINUS,
    B_PLUS,
    B,
    B_MINUS,
    C_PLUS,
    C,
    C_MINUS,
    D_PLUS,
    D,
    D_MINUS,
    E_PLUS,
    E,
    E_MINUS,
    NUM,
  }
}
