﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidBossInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidBossInfo
  {
    public int no;
    public int boss_id;
    public int round;
    public int current_hp;
    public long start_time;
    public int is_reward;
    public int is_timeover;
    public int is_rescue_damage_zero;
    public int is_beat_resucue;
  }
}
