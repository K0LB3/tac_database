﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ChapterParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_ChapterParam
  {
    public string iname;
    public string name;
    public string expr;
    public string world;
    public long start;
    public long end;
    public string parent;
    public int hide;
    public string chap;
    public string banr;
    public string item;
    public string keyitem1;
    public int keynum1;
    public string keyitem2;
    public int keynum2;
    public string keyitem3;
    public int keynum3;
    public long keytime;
    public string hurl;
  }
}
