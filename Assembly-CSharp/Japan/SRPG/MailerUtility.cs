﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MailerUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using DeviceKit;

namespace SRPG
{
  public class MailerUtility
  {
    public static void Launch(string mailto, string subject, string body)
    {
      App.LaunchMailer(mailto, subject, body.Replace("\n", "%0A"));
    }
  }
}
