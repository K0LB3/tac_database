﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_JobSetParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_JobSetParam
  {
    public string iname;
    public string job;
    public int lrare;
    public int lplus;
    public string ljob1;
    public int llv1;
    public string ljob2;
    public int llv2;
    public string ljob3;
    public int llv3;
    public string cjob;
    public string target_unit;
    public int is_final_job;
  }
}
