﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_GuildMember
  {
    public long gid;
    public string uid;
    public int role_id;
    public string name;
    public int lv;
    public string award_id;
    public Json_Unit units;
    public long applied_at;
    public long joined_at;
    public long leave_at;
    public long lastlogin;
  }
}
