﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidSOSMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidSOSMember
  {
    public string fuid;
    public string name;
    public int lv;
    public Json_Unit unit;
    public int member_type;
    public long last_battle_time;
  }
}
