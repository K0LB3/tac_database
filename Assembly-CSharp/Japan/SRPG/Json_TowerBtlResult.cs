﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_TowerBtlResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_TowerBtlResult : Json_PlayerDataAll
  {
    public JSON_ReqTowerResuponse.Json_TowerPlayerUnit[] pdeck;
    public Json_Artifact[] artis;
    public int arrived_num;
    public int clear;
    public Json_TowerBtlEndRank ranking;
  }
}
