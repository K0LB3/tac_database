﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidNextArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class ReqRaidNextArea : WebAPI
  {
    public ReqRaidNextArea(Network.ResponseCallback response)
    {
      this.name = "raidboss/next_area";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public int area_id;
    }
  }
}
