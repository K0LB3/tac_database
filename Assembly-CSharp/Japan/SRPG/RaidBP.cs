﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidBP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  public class RaidBP
  {
    private int mCurrent = 5;
    private int mMax = 5;
    private DateTime mAt;
    private int mAddBPMinutes;

    public RaidBP(int addBPMinutes)
    {
      this.mAddBPMinutes = addBPMinutes;
    }

    public int Current
    {
      get
      {
        return this.mCurrent;
      }
    }

    public int Max
    {
      get
      {
        return this.mMax;
      }
    }

    public DateTime At
    {
      get
      {
        return this.mAt;
      }
    }

    public bool Deserialize(Json_RaidBP json)
    {
      this.mCurrent = json.pt;
      this.mMax = json.max;
      this.mAt = TimeManager.FromUnixTime(json.at).AddMinutes((double) this.mAddBPMinutes);
      return true;
    }

    public void AddPoint()
    {
      ++this.mCurrent;
    }

    public void AddMinutes()
    {
      this.mAt = this.mAt.AddMinutes((double) this.mAddBPMinutes);
    }
  }
}
