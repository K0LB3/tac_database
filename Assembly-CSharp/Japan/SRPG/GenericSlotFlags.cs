﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenericSlotFlags
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  [DisallowMultipleComponent]
  public class GenericSlotFlags : MonoBehaviour
  {
    [BitMask]
    public GenericSlotFlags.VisibleFlags Flags;

    public GenericSlotFlags()
    {
      base.\u002Ector();
    }

    [System.Flags]
    public enum VisibleFlags
    {
      Empty = 1,
      NonEmpty = 2,
      Locked = 4,
    }
  }
}
