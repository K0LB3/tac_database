﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestMissionProgressJudgeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum QuestMissionProgressJudgeType
  {
    NotSupport,
    GreatorEqual,
    Greator,
    LessEqual,
    Less,
    Equal,
    EqualZero,
  }
}
