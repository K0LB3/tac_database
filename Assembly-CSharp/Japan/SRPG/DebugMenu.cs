﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DebugMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

namespace SRPG
{
  public class DebugMenu : MonoBehaviour
  {
    public DebugMenu()
    {
      base.\u002Ector();
    }

    public static void Start()
    {
    }

    public static void Log(string tag, string text, string trace)
    {
    }

    public static void Log(string tag, string text)
    {
    }

    public static void Log(string text)
    {
    }

    public static void LogSystem(string tag, string text, string trace)
    {
    }

    public static void LogSystem(string tag, string text)
    {
    }

    public static void LogSystem(string text)
    {
    }

    public static void LogWarning(string tag, string text, string trace)
    {
    }

    public static void LogWarning(string tag, string text)
    {
    }

    public static void LogWarning(string text)
    {
    }

    public static void LogError(string tag, string text, string trace)
    {
    }

    public static void LogError(string tag, string text)
    {
    }

    public static void LogError(string text)
    {
    }
  }
}
