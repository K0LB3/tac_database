﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRewardReceiveWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RaidRewardReceiveWindow : MonoBehaviour
  {
    [SerializeField]
    private Text mTitleText;
    [SerializeField]
    private Text mMessageText;
    [SerializeField]
    private Transform mRewardTransform;
    [SerializeField]
    private RaidRewardIcon mRewardItem;

    public RaidRewardReceiveWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      RaidRewardData raidRewards = RaidManager.Instance.GetRaidRewards();
      for (int index = 0; index < raidRewards.Rewards.Length; ++index)
      {
        RaidRewardIcon raidRewardIcon = (RaidRewardIcon) Object.Instantiate<RaidRewardIcon>((M0) this.mRewardItem, this.mRewardTransform);
        raidRewardIcon.Initialize(raidRewards.Rewards[index]);
        ((Component) raidRewardIcon).get_gameObject().SetActive(true);
      }
      if (!Object.op_Inequality((Object) this.mTitleText, (Object) null) || !Object.op_Inequality((Object) this.mMessageText, (Object) null))
        return;
      string str1 = string.Empty;
      string str2 = string.Empty;
      switch (raidRewards.Kind)
      {
        case RaidRewardKind.Beat:
          str1 = "RAID_REWARD_RECEIVE_WINDOW_BEAT_TITLE";
          str2 = "RAID_REWARD_RECEIVE_WINDOW_BEAT_MESSAGE";
          break;
        case RaidRewardKind.DamageRatio:
          str1 = "RAID_REWARD_RECEIVE_WINDOW_DAMAGE_RATIO_TITLE";
          str2 = "RAID_REWARD_RECEIVE_WINDOW_DAMAGE_RATIO_MESSAGE";
          break;
        case RaidRewardKind.AreaClear:
          str1 = "RAID_REWARD_RECEIVE_WINDOW_AREA_CLEAR_TITLE";
          str2 = "RAID_REWARD_RECEIVE_WINDOW_AREA_CLEAR_MESSAGE";
          break;
        case RaidRewardKind.Complete:
          str1 = "RAID_REWARD_RECEIVE_WINDOW_COMPLETE_TITLE";
          str2 = "RAID_REWARD_RECEIVE_WINDOW_COMPLETE_MESSAGE";
          break;
      }
      this.mTitleText.set_text(LocalizedText.Get("sys." + str1));
      this.mMessageText.set_text(LocalizedText.Get("sys." + str2));
    }
  }
}
