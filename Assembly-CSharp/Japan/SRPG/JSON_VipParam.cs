﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VipParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_VipParam
  {
    public int exp;
    public int ticket;
    public int buy_coin_bonus;
    public int buy_coin_num;
    public int buy_stmn_num;
    public int reset_elite;
    public int reset_arena;
  }
}
