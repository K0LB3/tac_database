﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayVersusRanking
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(0, "Refresh", FlowNode.PinTypes.Input, 0)]
  public class MultiPlayVersusRanking : MonoBehaviour, IFlowInterface
  {
    private const int PIN_IN_REFRESH = 0;
    [SerializeField]
    private Text RankText;
    [SerializeField]
    private Text BestRankText;
    [SerializeField]
    private Transform IconHolder;
    [SerializeField]
    private GameObject ItemIcon;
    [SerializeField]
    private GameObject CoinIcon;
    [SerializeField]
    private GameObject GoldIcon;
    [SerializeField]
    private GameObject ArenaCoinIcon;

    public MultiPlayVersusRanking()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 0)
        return;
      this.Refresh();
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.ItemIcon, (Object) null))
        this.ItemIcon.SetActive(false);
      if (Object.op_Inequality((Object) this.CoinIcon, (Object) null))
        this.CoinIcon.SetActive(false);
      if (Object.op_Inequality((Object) this.GoldIcon, (Object) null))
        this.GoldIcon.SetActive(false);
      if (!Object.op_Inequality((Object) this.ArenaCoinIcon, (Object) null))
        return;
      this.ArenaCoinIcon.SetActive(false);
    }

    private void Refresh()
    {
      if (GlobalVars.ArenaAward == null)
        return;
      Json_ArenaAward arenaAward = GlobalVars.ArenaAward;
      if (Object.op_Inequality((Object) this.RankText, (Object) null))
        this.RankText.set_text(arenaAward.rank.ToString());
      if (Object.op_Inequality((Object) this.BestRankText, (Object) null))
        this.BestRankText.set_text(arenaAward.rank_best.ToString());
      if (GlobalVars.ArenaAward.reward == null || GlobalVars.ArenaAward.reward.rank == null)
        return;
      Json_ArenaRewardInfo rank = GlobalVars.ArenaAward.reward.rank;
      if (rank.coin > 0)
      {
        GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.CoinIcon);
        DataSource.Bind<int>(gameObject, rank.coin);
        gameObject.get_transform().SetParent(this.IconHolder, false);
        gameObject.SetActive(true);
      }
      if (rank.gold > 0)
      {
        GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.GoldIcon);
        DataSource.Bind<int>(gameObject, rank.gold);
        gameObject.get_transform().SetParent(this.IconHolder, false);
        gameObject.SetActive(true);
      }
      if (rank.arenacoin > 0)
      {
        GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ArenaCoinIcon);
        DataSource.Bind<int>(gameObject, rank.arenacoin);
        gameObject.get_transform().SetParent(this.IconHolder, false);
        gameObject.SetActive(true);
      }
      if (rank.items != null && rank.items.Length > 0)
      {
        GameManager instance = MonoSingleton<GameManager>.Instance;
        foreach (Json_Item jsonItem in rank.items)
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemIcon);
          ItemData data = new ItemData();
          data.Setup(0L, jsonItem.iname, jsonItem.num);
          DataSource.Bind<ItemData>(gameObject, data);
          gameObject.get_transform().SetParent(this.IconHolder, false);
          gameObject.SetActive(true);
        }
      }
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
