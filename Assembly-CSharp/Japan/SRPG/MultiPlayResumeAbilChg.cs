﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayResumeAbilChg
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class MultiPlayResumeAbilChg
  {
    public MultiPlayResumeAbilChg.Data[] acd;

    [Serializable]
    public class Data
    {
      public string fid;
      public string tid;
      public int tur;
      public int irs;
      public int exp;
      public int iif;
    }
  }
}
