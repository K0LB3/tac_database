﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestCampaignParentParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_QuestCampaignParentParam
  {
    public string iname;
    public string begin_at;
    public string end_at;
    public string[] children;
  }
}
