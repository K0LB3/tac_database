﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillEffectTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public enum SkillEffectTypes
  {
    None,
    Equipment,
    Attack,
    Defend,
    Heal,
    Buff,
    Debuff,
    Revive,
    Shield,
    ReflectDamage,
    DamageControl,
    FailCondition,
    CureCondition,
    DisableCondition,
    GemsGift,
    GemsIncDec,
    Guard,
    Teleport,
    Changing,
    RateHeal,
    RateDamage,
    PerfectAvoid,
    Throw,
    EffReplace,
    SetTrick,
    TransformUnit,
    SetBreakObj,
    ChangeWeather,
    RateDamageCurrent,
    TransformUnitTakeOverHP,
    DynamicTransformUnit,
  }
}
