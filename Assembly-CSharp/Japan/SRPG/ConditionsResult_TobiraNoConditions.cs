﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_TobiraNoConditions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class ConditionsResult_TobiraNoConditions : ConditionsResult
  {
    public ConditionsResult_TobiraNoConditions()
    {
      this.mIsClear = true;
    }

    public override string text
    {
      get
      {
        return LocalizedText.Get("sys.TOBIRA_CONDITIONS_NOTHING");
      }
    }

    public override string errorText
    {
      get
      {
        return LocalizedText.Get("sys.ITEM_NOT_ENOUGH");
      }
    }
  }
}
