﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRankingData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class RaidRankingData
  {
    private string mUID;
    private string mName;
    private int mLv;
    private int mRank;
    private int mScore;
    private UnitData mUnit;
    private string mSelectedAward;

    public string UID
    {
      get
      {
        return this.mUID;
      }
    }

    public string Name
    {
      get
      {
        return this.mName;
      }
    }

    public int Lv
    {
      get
      {
        return this.mLv;
      }
    }

    public int Rank
    {
      get
      {
        return this.mRank;
      }
    }

    public int Score
    {
      get
      {
        return this.mScore;
      }
    }

    public UnitData Unit
    {
      get
      {
        return this.mUnit;
      }
    }

    public string SelectedAward
    {
      get
      {
        return this.mSelectedAward;
      }
    }

    public bool Deserialize(Json_RaidRankingData json)
    {
      this.mUID = json.uid;
      this.mName = json.name;
      this.mLv = json.lv;
      this.mRank = json.rank;
      this.mScore = json.score;
      this.mSelectedAward = json.selected_award;
      if (json.unit != null)
      {
        this.mUnit = new UnitData();
        this.mUnit.Deserialize(json.unit);
      }
      return true;
    }
  }
}
