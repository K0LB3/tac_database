﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqGuildFacilityEnhance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Guild/ReqGuildFacilityEnhance", 32741)]
  [FlowNode.Pin(1, "ギルド施設強化", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "ギルド施設強化完了", FlowNode.PinTypes.Output, 101)]
  public class FlowNode_ReqGuildFacilityEnhance : FlowNode_Network
  {
    private const int PIN_INPUT_START_GUILD_FACILITY_ENHANCE = 1;
    private const int PIN_OUTPUT_END_GUILD_FACILITY_ENHANCE = 101;

    public override void OnActivate(int pinID)
    {
      if (pinID == 1)
      {
        if (UnityEngine.Object.op_Equality((UnityEngine.Object) GuildFacilityEnhance.Instance, (UnityEngine.Object) null) || GuildFacilityEnhance.Instance.TargetFacility == null || GuildFacilityEnhance.Instance.SelectedEnhanceMaterials.Count <= 0)
          return;
        this.ExecRequest((WebAPI) new ReqGuildFacilityEnhance(GuildFacilityEnhance.Instance.TargetFacility.Iname, GuildFacilityEnhance.Instance.SelectedEnhanceMaterials.ToArray(), new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback)));
      }
      ((Behaviour) this).set_enabled(true);
    }

    public override void OnSuccess(WWWResult www)
    {
      if (Network.IsError)
      {
        switch (Network.ErrCode)
        {
          case Network.EErrCode.Guild_InvestLimitOneDay:
            this.OnBack();
            break;
          case Network.EErrCode.Guild_InvestCoolTime:
            this.OnBack();
            break;
          default:
            this.OnRetry();
            break;
        }
      }
      else
      {
        WebAPI.JSON_BodyResponse<FlowNode_ReqGuildFacilityEnhance.Json_ResGuildFacilityEnhance> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<FlowNode_ReqGuildFacilityEnhance.Json_ResGuildFacilityEnhance>>(www.text);
        DebugUtility.Assert(jsonObject != null, "res == null");
        Network.RemoveAPI();
        try
        {
          if (jsonObject.body != null)
          {
            MonoSingleton<GameManager>.Instance.Deserialize(jsonObject.body.guild);
            MonoSingleton<GameManager>.Instance.Deserialize(jsonObject.body.player_guild);
            MonoSingleton<GameManager>.Instance.Deserialize(jsonObject.body.items);
            GuildManager.Instance.SetEntryRequest(jsonObject.body.applied_member);
          }
        }
        catch (Exception ex)
        {
          DebugUtility.LogException(ex);
          return;
        }
        this.ActivateOutputLinks(101);
        ((Behaviour) this).set_enabled(false);
      }
    }

    public class Json_ResGuildFacilityEnhance
    {
      public JSON_Guild guild;
      public JSON_PlayerGuild player_guild;
      public JSON_GuildMember[] applied_member;
      public Json_Item[] items;
    }
  }
}
