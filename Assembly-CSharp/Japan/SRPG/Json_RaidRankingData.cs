﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_RaidRankingData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using System;

namespace SRPG
{
  [Serializable]
  public class Json_RaidRankingData
  {
    public string uid;
    public string name;
    public int lv;
    public int rank;
    public int score;
    public Json_Unit unit;
    public string selected_award;
  }
}
