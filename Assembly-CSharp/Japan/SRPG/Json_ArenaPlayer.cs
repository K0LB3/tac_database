﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ArenaPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class Json_ArenaPlayer
  {
    public string result;
    public string fuid;
    public string name;
    public int lv;
    public int rank;
    public Json_Unit[] units;
    public long btl_at;
    public string award;
    public JSON_ViewGuild guild;
  }
}
