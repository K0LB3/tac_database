﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardTrustMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardTrustMaster : MonoBehaviour
  {
    [SerializeField]
    private RawImage mCardImage;
    [SerializeField]
    private RawImage mCardImageAdd;

    public ConceptCardTrustMaster()
    {
      base.\u002Ector();
    }

    public void SetData(ConceptCardData data)
    {
      string path = AssetPath.ConceptCard(data.Param);
      if (Object.op_Inequality((Object) this.mCardImage, (Object) null))
        MonoSingleton<GameManager>.Instance.ApplyTextureAsync(this.mCardImage, path);
      if (!Object.op_Inequality((Object) this.mCardImageAdd, (Object) null))
        return;
      MonoSingleton<GameManager>.Instance.ApplyTextureAsync(this.mCardImageAdd, path);
    }
  }
}
