﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ResultMask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ResultMask : MonoBehaviour
  {
    public RawImage ImgBg;

    public ResultMask()
    {
      base.\u002Ector();
    }

    public void SetBg(Texture2D tex)
    {
      if (!Object.op_Implicit((Object) this.ImgBg) || Object.op_Equality((Object) tex, (Object) null))
        return;
      this.ImgBg.set_texture((Texture) tex);
      ((Component) this.ImgBg).get_gameObject().SetActive(true);
    }
  }
}
