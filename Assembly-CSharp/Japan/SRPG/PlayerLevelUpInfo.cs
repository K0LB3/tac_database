﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PlayerLevelUpInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

namespace SRPG
{
  public class PlayerLevelUpInfo
  {
    public int LevelPrev;
    public int LevelNext;
    public int StaminaNext;
    public int StaminaMaxPrev;
    public int StaminaMaxNext;
    public int MaxFriendNumPrev;
    public int MaxFriendNumNext;
    public string[] UnlockInfo;
  }
}
