﻿// Decompiled with JetBrains decompiler
// Type: SetCanvasBounds
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

using UnityEngine;

public class SetCanvasBounds : MonoBehaviour
{
  [SerializeField]
  private bool IgnoreApplySafeAreaFlag;
  public RectTransform panel;
  private Rect lastSafeArea;

  public SetCanvasBounds()
  {
    base.\u002Ector();
  }

  public static Rect GetSafeArea()
  {
    return new Rect(0.0f, 0.0f, (float) Screen.get_width(), (float) Screen.get_height());
  }

  public static float CalcCanvasBoundsScale()
  {
    Rect safeArea = SetCanvasBounds.GetSafeArea();
    return ((Rect) ref safeArea).get_width() / (float) Screen.get_width();
  }

  private void Start()
  {
    if (this.IgnoreApplySafeAreaFlag)
      return;
    this.ApplySafeAreaScale(SetCanvasBounds.GetSafeArea());
  }

  private void ApplySafeAreaScale(Rect area)
  {
    if (Object.op_Inequality((Object) this.panel, (Object) null))
    {
      float num = ((Rect) ref area).get_width() / (float) Screen.get_width();
      Vector3 vector3;
      ((Vector3) ref vector3).\u002Ector(num, num, num);
      this.panel.set_sizeDelta(new Vector2(0.0f, 750f));
      this.panel.set_anchorMin(new Vector2(0.0f, 1f));
      this.panel.set_anchorMax(new Vector2(1f, 1f));
      this.panel.set_pivot(new Vector2(0.5f, 1f));
      ((Transform) this.panel).set_localScale(vector3);
    }
    this.lastSafeArea = area;
  }
}
