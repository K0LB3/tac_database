﻿// Decompiled with JetBrains decompiler
// Type: GamePropertyKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2FAA4F95-28C8-40DA-B3D0-15E33F3CF650
// Assembly location: D:\User\Downloads\Assembly-CSharp.dll

public class GamePropertyKey
{
  public const byte MaxPlayers = 255;
  public const byte IsVisible = 254;
  public const byte IsOpen = 253;
  public const byte PlayerCount = 252;
  public const byte Removed = 251;
  public const byte PropsListedInLobby = 250;
  public const byte CleanupCacheOnLeave = 249;
  public const byte MasterClientId = 248;
  public const byte ExpectedUsers = 247;
}
