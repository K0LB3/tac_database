import json
import os
import http.client
import urllib.request
import struct
import zlib
import threading
from queue import Queue

host_gl = "api.alcww.gumi.sg"
gameVersion_gl = "3b43e34d4f54bc49d5e84dd80c27a7a30148023d_a"

host_jp= "alchemist.gu3.jp"
gameVersion_jp = "66bff695:a"



class DownloadThread(threading.Thread):
    def __init__(self, queue, destfolder,host,version):
        super(DownloadThread, self).__init__()
        self.queue = queue
        self.destfolder = destfolder
        self.daemon = True
        self.host=host
        self.version=version
        self.failed=[]

    def run(self):
        while True:
            url = self.queue.get()
            try:
                self.download_url(url)
            except Exception as e:
                print ("   Error: %s"%e)
                self.failed.append(url['path'])
            self.queue.task_done()
        print(self.failed)    

    def download_url(self, asset):
        #change it to a different way if you require
        #name = url.split('/')[-1]
        #dest = os.path.join(self.destfolder, name)
        #print ("[%s] Downloading %s -> %s"%(self.ident, url, dest))
        #urllib.urlretrieve(url, dest)
        print (self.ident,'\t',asset['path'])
        data=Download_Asset(self.host,self.version,asset,typ='aatc')
        Save_Asset(self.destfolder,asset,data)

def Fetch_Assets(path,host_dl,version,assets,typ='aatc',numthreads=8):
    queue = Queue()
    def walk(assets):
        for key,asset in assets.items():
            if type(asset)!=dict:
                continue
            if 'id' in asset:
                queue.put(asset)
                print(asset['id'])
            else:
                walk(asset)

    walk(assets)

    for i in range(numthreads):
        t = DownloadThread(queue, path, host_dl, version)
        t.start()
    queue.join()

def Download_ALL_Text_Assets(path,AssetList,numthreads=4):
    host_dl=AssetList['host']
    version=AssetList['version']
    queue = Queue()
    for asset in AssetList['assets']:
        if 'id' in asset and 'Compressed' in asset['flags'] and 'RawData' in asset['flags']:
            queue.put(asset)
    for i in range(numthreads):
        t = DownloadThread(queue, path, host_dl, version)
        t.start()
    queue.join()

def Download_Asset(host_dl,version,asset,typ='aatc'):
    data = requestAsset(host_dl,version,typ,asset['id'])
    if 'Compressed' in asset['flags']:
        data=zlib.decompress(data)
    return(data)

def Save_Asset(path,asset,data):
    fpath=os.path.join(path,asset['path'].replace('/','\\'))
    os.makedirs(fpath.rsplit('\\',1)[0], exist_ok=True)
    with open(fpath, 'wb') as f:
        f.write(data)

def get_assetlist(region='global',listT='aatc',save=False):
    if region=='global':
        ver = req_chkver_gl()
        gameVersion=gameVersion_gl
    elif region=='japan':
        ver = req_chkver_jp()
        gameVersion=gameVersion_jp

    version= ver['assets']
    host_dl= ver['host_dl']
    #lists=['aatc','aatc','text']

    AssetList = requestAsset(host_dl,version,listT,"ASSETLIST")
    res= parse_assetlist(AssetList)
    res['gameVersion']=gameVersion
    res['version']=version
    res['host']= host_dl
    #format to path
    a2={}
    for asset in res['assets']:
        if 'IsFolder' in asset['flags']:
            continue
        f = asset['path'].split('/')
        path=a2
        for index,p in enumerate(f):
            if index!= len(f)-1:
                if p not in path:
                    path[p]={}
                path=path[p]
            else:
                path[p]=asset
    res['path']=a2
    if save:
        with open('AssetList-{}.json'.format(listT),'w') as f:
            json.dump(res,f,indent=2)
    return res


def parse_assetlist(fh):
    global pointer
    pointer=0

    bundleFlags = ["Compressed","RawData","Required","Scene","Tutorial","Multiplay","StreamingAsset","TutorialMovie","Persistent","DiffAsset",None,None,"IsLanguage","IsCombined","IsFolder"]

    assetlist = {
        'revision'  : readInt32(fh),
        'length'    : readInt32(fh)
    }    
    assetlist["assets"] = [{
            "id" :          "%08x" % readUInt32(fh),
            "size" :        readInt32(fh),
            "compSize" :    readInt32(fh),
            "path" :        readString(fh),
            "pathHash" :    readInt32(fh),
            "hash" :        readUInt32(fh),
            "flags" : [
                bundleFlags[index]               #flags
                for index, bit in enumerate( bin(readInt32(fh))[2:][::-1] )
                if bit == "1"
            ],
            "dependencies" : [
                readInt32(fh)     #array length
                for j in range(readInt32(fh))
            ],
            "additionalDependencies" : [
                readInt32(fh)     #array length
                for j in range(readInt32(fh))
            ],
            "additionalStreamingDependencies" : [
                readInt32(fh)     #array length
                for j in range(readInt32(fh))
            ],
        }
        for i in range(0, assetlist["length"])
    ]
    return assetlist
    
def readInt32(fh):
    byts = readBytes(fh,4)
    return  struct.unpack('i', byts)[0]

def readUInt32(fh):
    byts = readBytes(fh,4)
    return  struct.unpack('I', byts)[0]

def readString(fh):
    res = ""
    control = "1"
    while control == "1":
        bit_str = bin(ord(readBytes(fh,1)))[2:].zfill(8)
        control = bit_str[0]
        res += bit_str[1:]
    length = int(res,2)

    return struct.unpack("{}s".format(length), readBytes(fh,length))[0].decode("utf8") if length else ''

def readBytes(fh,size):
    global pointer
    bts=fh[pointer:size+pointer]
    pointer+=size
    return bts

##API ############################################
def requestAsset(host_dl,version,typ,name):
    #request asset list
    url = '{host_dl}/assets/{version}/{typ}/{name}'.format(
        host_dl=host_dl,
        version=version,
        typ=typ,
        name=name
        )
    print(url)
    asset = urllib.request.urlopen(url).read()
    return asset

def req_chkver_gl():
    host=host_gl
    gameVersion=gameVersion_gl
    body = json.dumps({
        "param": {
            "ver": gameVersion,
            "os": 'android'
        }
    })
    con = http.client.HTTPSConnection(host)
    con.connect()
    con.request("POST", "/chkver", body, {"Content-Type":"application/json; charset=utf-8"})
    res_body = con.getresponse().read()
    con.close()
    return  json.loads(res_body)['body']

def req_chkver_jp():
    host=host_jp
    gameVersion=gameVersion_jp

    body=json.dumps({'ver':gameVersion})
    con = http.client.HTTPSConnection(host)
    con.connect()
    con.request("POST", "/chkver2", body, {"Content-Type":"application/json; charset=utf-8"})
    res_body = con.getresponse().read()
    con.close()
    return  json.loads(res_body)['body']['environments']['alchemist']