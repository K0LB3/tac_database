import unitypack
# from unitypack.export import OBJMesh
# from unitypack.utils import extract_audioclip_samples
from unitypack.asset import Asset

import PIL.Image as Image
import PIL.ImageOps as ImageOps

import tempfile
import shutil
import sys
import os
import struct
import subprocess


from io import BytesIO #for bytes to file

# from pprint import pprint
# import traceback

# Global vars
processedTypes = [
	"TextAsset",
	"Texture2D",
	# "Mesh",
	# "Shader",
	# "AudioClip","MovieTexture"
]

compressonatorPath = os.path.join(os.environ["COMPRESSONATOR_ROOT"], "CompressonatorCLI.exe")
assetsPath = os.path.abspath(os.path.join("..", "_ASSETSFOLDER", "sg.gumi.alchemistww", "files", "new_aatc"))
outputPath = os.path.abspath(os.path.join("..", "_ASSETSFOLDER", "sg.gumi.alchemistww", "files", "new_aatc-extracted"))


# Functions
def writeWithDDSHeader(imgFile, texture2d, format):
	imgFile.write(bytes("DDS ", "ascii"))
	imgFile.write(struct.pack("I", 124))  # dwSize
	imgFile.write(struct.pack("I", 0x1 | 0x2 | 0x4 | 0x1000 | 0x20000 | 0x80000))  # dwFlags
	imgFile.write(struct.pack("I", texture2d.height))  # dwHeight
	imgFile.write(struct.pack("I", texture2d.width))  # dwWidth
	imgFile.write(struct.pack("I", texture2d.complete_image_size))  # dwPitchOrLinearSize
	imgFile.write(struct.pack("I", 0))  # dwDepth
	imgFile.write(struct.pack("I", texture2d.__dict__['_obj']['m_MipCount']))  # dwMipMapCount

	for i in range(0, 11):
		imgFile.write(struct.pack("I", 0))  # dwReserved1[11];

	# DDS_PIXELFORMAT
	imgFile.write(struct.pack("I", 32))  # dwSize
	imgFile.write(struct.pack("I", 0x1 | 0x4))  # dwFlags

	if format == 35:
		imgFile.write(bytes("ATC ", "ascii"))  # dwFourCC
	elif format == 36:
		imgFile.write(bytes("ATCI", "ascii"))  # dwFourCC
	else:
		print("writeWithDDSHeader called with invalid format", format)
		sys.exit()

	imgFile.write(struct.pack("I", 0))  # dwRGBBitCount
	imgFile.write(struct.pack("I", 0))  # dwRBitMask
	imgFile.write(struct.pack("I", 0))  # dwGBitMask
	imgFile.write(struct.pack("I", 0))  # dwBBitMask
	imgFile.write(struct.pack("I", 0))  # dwABitMask

	imgFile.write(struct.pack("I", 0x8 | 0x400000 | 0x1000))  # dwCaps
	imgFile.write(struct.pack("I", 0))  # dwCaps2
	imgFile.write(struct.pack("I", 0))  # dwCaps3
	imgFile.write(struct.pack("I", 0))  # dwCaps4
	imgFile.write(struct.pack("I", 0))  # dwReversed2

	imgFile.write(bytes(texture2d.image_data))


def writeWithKTXHeader(imgFile, texture2d, format):
	imgFile.write(bytearray([0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A]))  # identifier
	imgFile.write(struct.pack("I", 0x04030201))  # endianness
	imgFile.write(struct.pack("I", 0))  # glType
	imgFile.write(struct.pack("I", 1))  # glTypeSize
	imgFile.write(struct.pack("I", 0))  # glFormat

	if format == 34:  # ETC_RGB4
		imgFile.write(struct.pack("I", 0x8D64))  # glInternalFormat
		imgFile.write(struct.pack("I", 0))  # glBaseInternalFormat

		block_size = 64
		block_width = 4
		block_height = 4
	elif format == 47:  # ETC2_RGB8
		imgFile.write(struct.pack("I", 0x9278))  # glInternalFormat
		imgFile.write(struct.pack("I", 0))  # glBaseInternalFormat

		block_size = 128
		block_width = 4
		block_height = 4
	else:
		print("writeWithKTXHeader called with invalid format", format)
		sys.exit()

	imgFile.write(struct.pack("I", texture2d.width))  # pixelWidth
	imgFile.write(struct.pack("I", texture2d.height))  # pixelHeight
	imgFile.write(struct.pack("I", 0))  # pixelDepth
	imgFile.write(struct.pack("I", 0))  # numberOfArrayElements
	imgFile.write(struct.pack("I", 1))  # numberOfFaces
	imgFile.write(struct.pack("I", texture2d.__dict__['_obj']['m_MipCount']))  # numberOfMipmapLevels
	imgFile.write(struct.pack("I", 0))  # bytesOfKeyValueData

	# https://github.com/hglm/texgenpack/blob/master/file.c
	# https://github.com/hglm/texgenpack/blob/master/texture.c
	extended_width = (texture2d.width + block_width - 1)
	extended_height = (texture2d.height + block_height - 1)
	imageSize = int(extended_height / block_height) * int(extended_width / block_width) * int(block_size / 8)

	imgFile.write(struct.pack("I", imageSize))
	imgFile.write(bytes(texture2d.image_data))


def processTexture2D(data, outputFile):
	try:
		img = data.image

		if img is None:
			return

		if data.format in [1, 3, 4, 5]:  # Alpha8/RGB24/RGBA32/ARGB32
			channels = ImageOps.flip(img).split()
		elif data.format in [13, 7]:  # RGBA4444/RGB565
			channels = tuple(reversed(ImageOps.flip(img).split()))
		else:
			print("\n", "Unknown format", data.format, outputFile)
			return

		if len(channels) == 3:
			Image.merge("RGB", channels).save(outputFile)
		else:
			Image.merge("RGBA", channels).save(outputFile)

	except NotImplementedError:
		'''
		print("Name:", os.path.basename(outputFile))
		print("\tFormat",data.format)
		print("\tWidth",data.width)
		print("\tHeight",data.height)
		print("\tColor Space",data.color_space)
		print("\tLightmap Format",data.lightmap_format)
		print("\tMip",data.__dict__['_obj']['m_MipCount'])
		print("\tTexture Dimension",data.texture_dimension)
		print("\tTexture Settings",data.texture_settings)
		print("\tSize",data.complete_image_size)
		'''

		if data.format in [34, 47, 35, 36]:
			processUnimplementedTexture2D(data, outputFile)
		else:
			print("\n\tSkipping file: NotImplementedError", data.format, outputFile)


def processUnimplementedTexture2D(data, outputFile):
	if data.format in [34, 47]:  # ETC_RGB4, ETC2_RGBA8
		with tempfile.NamedTemporaryFile(suffix=".ktx", delete=False) as tmpFile:
			writeWithKTXHeader(tmpFile, data, data.format)

		process = subprocess.Popen([
			"texgenpack.exe",
			"--decompress",
			tmpFile.name,
			outputFile
		], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
		output, err = process.communicate()

	elif data.format in [35, 36]:  # ATC_RGB4, ATC_RGBA8
		with tempfile.NamedTemporaryFile(suffix=".dds", delete=False) as tmpFile:
			writeWithDDSHeader(tmpFile, data, data.format)

		#outputFile = outputFile.replace("André", "ANDR~2")
		process = subprocess.Popen([
			compressonatorPath,
			tmpFile.name,
			outputFile
		], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
		output, err = process.communicate()
	else:
		print("\n\tSkipping file: NotImplementedError", data.format, outputFile)
		return
	
	os.unlink(tmpFile.name)
	
	if process.returncode != 0:
		print("\nError running command: \n", output)
		return

	img = Image.open(outputFile)
	img = ImageOps.flip(img)
	img.save(outputFile)


def getAvailableFileName(path, filename="NONAME", extension=""):
	if path == "":
		filename = "."
	if filename == "":
		filename = "NONAME"

	index = 1
	while True:
		indexString = " ({})".format(index) if (index != 1) else ""
		extensionString = ".{}".format(extension) if (extension != "") else ""
		finalFilename = "{}{}{}".format(filename, indexString, extensionString)
		finalPath = os.path.join(path, finalFilename)

		if not os.path.isfile(finalPath):
			return finalPath
		else:
			index = index + 1


def processUnityFile(filePath, destFolder):
	cleanFilename = os.path.basename(filePath)

	with open(filePath, "rb") as f:

		firstChars = bytearray(f.read(12))
		f.seek(0)

		if firstChars[:len("UnityFS")] == bytearray("UnityFS".encode()):
			try:
				bundle = unitypack.load(f)
				processBundle(bundle, destFolder, cleanFilename)
			except:
				# traceback.print_exc()
				print("Error extracting bundle: ", filePath)
				return
		elif firstChars[:len("UnityWeb")] == bytearray("UnityWeb".encode()):
			# unitypack can't process these files
			# print("\n\tIgnoring file",filePath)
			pass
		elif firstChars[:len("UnityRaw")] == bytearray("UnityRaw".encode()) or firstChars[:len("UnityArchive")] == bytearray("UnityArchive".encode()):
			print("\n\tFile with unprocessed type found", filePath)
		else:
			try:  # might be .assets, lets try to open them like this
				asset = Asset.from_file(f)
				processAsset(asset, destFolder)
			except:  # just copy the file then, might be json or some other format
				# traceback.print_exc()
				print("\tUnable to load file (Copying)\t", filePath[len(assetsPath) + 1:])
				outputFile = getAvailableFileName(destFolder, cleanFilename)
				shutil.copyfile(filePath, outputFile)

		'''
		try:
			#.unity3d
			#Some files (UnityWeb) throw exception in asset.objects.items() (struct.error: unpack requires a buffer of 4 bytes)
			bundle = unitypack.load(f)
			processBundle(bundle,destFolder,cleanFilename)
		except:
			#traceback.print_exc()
			try:
				#.assets
				asset = Asset.from_file(f)
				processAsset(asset,destFolder,cleanFilename)
			except:
				#traceback.print_exc()
				print("\tUnable to load file (Copying)\t", filePath[len(assetsPath)+1:])
				outputFile = getAvailableFileName(destFolder,cleanFilename)
				shutil.copyfile(filePath,outputFile)
		'''


def processBundle(bundle, destFolder, cleanFilename):
	assetFileItemLength = 0

	for asset in bundle.assets:
		for id, object in asset.objects.items():
			if object.type in processedTypes:
				assetFileItemLength = assetFileItemLength + 1

	if assetFileItemLength > 1:
		destFolder = os.path.join(destFolder, cleanFilename)
		if not os.path.isdir(destFolder):
			os.makedirs(destFolder)

	for asset in bundle.assets:
		processAsset(asset, destFolder)


def processAsset(asset, destFolder):
	for id, object in asset.objects.items():

		# print("\tItem:",object.type,id)

		if object.type == "TextAsset":
			data = object.read()
			outputFile = getAvailableFileName(destFolder, data.name, "txt")

			with open(outputFile, "wb") as textFile:
				print(data.script, file=textFile)
		elif object.type == "Texture2D":
			data = object.read()
			outputFile = getAvailableFileName(destFolder, data.name, "png")
			processTexture2D(data, outputFile)


# #######################################

def main():
	if not os.path.exists(outputPath):
		os.makedirs(outputPath)

	numFiles = 0
	processedFiles = 0
	for currentFolder, subFolders, files in os.walk(assetsPath):
		numFiles += len(files)

	for currentFolder, subFolders, files in os.walk(assetsPath):
		for folder in subFolders:
			fullFolderPath = os.path.join(currentFolder, folder)
			outputSubFolder = os.path.join(outputPath, fullFolderPath[len(assetsPath) + 1:])
			if not os.path.exists(outputSubFolder):
				os.makedirs(outputSubFolder)

		for file in files:
			processedFiles += 1
			print("\rExtracting files [{}/{}] {:.2f}%".format(processedFiles, numFiles, processedFiles / numFiles * 100), end="")

			filePath = os.path.join(currentFolder, file)
			destFolder = os.path.join(outputPath, currentFolder[len(assetsPath) + 1:])
			# print("||Extracting file",filePath[len(assetsPath)+1:])
			processUnityFile(filePath, destFolder)


#main()
'''
elif object.type == "Mesh":
			outputFile = getAvailableFileName(destFolder,data.name,"obj")
			try:
				mesh_data = OBJMesh(data).export()
				with open(outputFile, "w", encoding="utf-8") as meshFile:
					print(mesh_data, file=meshFile)
			except Exception as e:
				pass
				#print("\tSkipping mesh file:", data.name, cleanFilename, str(e))
					
elif object.type == "Shader":
					outputFile = getAvailableFileName(destFolder,data.name,"cg")
					with open(outputFile, "w", encoding="utf-8") as shaderFile:
						print(data.script, file=shaderFile)
elif object.type == "AudioClip":
	print("AudioClip")
	samples = extract_audioclip_samples(data)
	for filename, sample in samples.items():
		outputFile = getAvailableFileName(destFolder,filename)
		with open(outputFile, "wb", encoding="utf-8") as audioFile:
			print(sample, file=audioFile)

elif object.type == "MovieTexture":
	print("MovieTexture")
	outputFile = getAvailableFileName(destFolder,data.name,"ogv")
	with open(outputFile, "wb", encoding="utf-8") as movieFile:
		print(data.movie_data, file=movieFile)

'''
def processUnityFile_bytes(ByteArray, filePath, destFolder):
	cleanFilename = os.path.basename(filePath)

	f=BytesIO(ByteArray)
	#with open(filePath, "rb") as f:
	if 1:
		firstChars = bytearray(f.read(12))
		f.seek(0)

		if firstChars[:len("UnityFS")] == bytearray("UnityFS".encode()):
			try:
				bundle = unitypack.load(f)
				processBundle(bundle, destFolder, cleanFilename)
			except:
				# traceback.print_exc()
				print("Error extracting bundle: ", filePath)
				return
		elif firstChars[:len("UnityWeb")] == bytearray("UnityWeb".encode()):
			# unitypack can't process these files
			# print("\n\tIgnoring file",filePath)
			pass
		elif firstChars[:len("UnityRaw")] == bytearray("UnityRaw".encode()) or firstChars[:len("UnityArchive")] == bytearray("UnityArchive".encode()):
			print("\n\tFile with unprocessed type found", filePath)
		else:
			try:  # might be .assets, lets try to open them like this
				asset = Asset.from_file(f)
				processAsset(asset, destFolder)
			except:  # just copy the file then, might be json or some other format
				# traceback.print_exc()
				print("\tUnable to load file (Copying)\t", filePath[len(assetsPath) + 1:])
				outputFile = getAvailableFileName(destFolder, cleanFilename)
				#shutil.copyfile(filePath, outputFile)
				with open(outputFile, 'wb') as f:
					f.write(ByteArray)