from AssetManager import Download_Asset,get_assetlist,json,os,urllib,Fetch_Assets,Download_ALL_Text_Assets

def main():
    path=os.path.join(os.path.dirname(os.path.realpath(__file__)),'resources')

####    GLOBAL   ##############################################################
    print('\n\n\n####\tGLOBAL\t####')
    print('Fetching Assetlist')
    assets_gl=get_assetlist('global')
    host_dl=assets_gl['host']
    version=assets_gl['version']

    print('MasterParam')
    f=Download_Asset(host_dl,version,assets_gl['path']['Data']['MasterParam'])
    param=json.loads(f,encoding='utf8')
    saveAsJSON(path,'MasterParam.json', convertParam(param),'GameFiles')

    print('QuestParam')
    f=Download_Asset(host_dl,version,assets_gl['path']['Data']['QuestParam'])
    param=json.loads(f,encoding='utf8')
    saveAsJSON(path,'QuestParam.json', convertParam(param),'GameFiles')

    print('QuestDropParam')
    f=Download_Asset(host_dl,version,assets_gl['path']['Data']['QuestDropParam'])
    param=json.loads(f,encoding='utf8')
    saveAsJSON(path,'QuestDropParam.json', convertParam(param),'GameFiles')

    name = 'LocalizedMasterParam'
    print(name)
    f=Download_Asset(host_dl,version,assets_gl['path']['Loc']['english'][name])
    param=f.decode(encoding="utf8", errors="strict")
    saveAsJSON(path,name+'.json', convertLoc(param),'GameFiles')

    name = 'LocalizedQuestParam'
    print(name)
    f=Download_Asset(host_dl,version,assets_gl['path']['Loc']['english'][name])
    param=f.decode(encoding="utf8", errors="strict")
    saveAsJSON(path,name+'.json', convertLoc(param),'GameFiles')

    name = 'sys'
    print(name)
    f=Download_Asset(host_dl,version,assets_gl['path']['Loc']['english'][name])
    param=f.decode(encoding="utf-8-sig", errors="strict")
    saveAsJSON(path,name+'.json', convertSys(param),'GameFiles')

    name = 'unit'
    print(name)
    f=Download_Asset(host_dl,version,assets_gl['path']['Loc']['english'][name])
    param=f.decode(encoding="utf-8-sig", errors="strict")
    unit = convertUnit(param)
    saveAsJSON(path,name+'.json', unit,'GameFiles')

####    JAPAN   ##############################################################
    print('\n\n\n####\tJAPAN\t####')
    print('Fetching Assetlist')
    assets_jp=get_assetlist('japan')
    host_dl=assets_jp['host']
    version=assets_jp['version']

    print('MasterParam')
    f=Download_Asset(host_dl,version,assets_jp['path']['Data']['MasterParam'])
    param=json.loads(f,encoding='utf8')
    saveAsJSON(path,'MasterParamJP.json', convertParam(param),'GameFiles')

    print('QuestParam')
    f=Download_Asset(host_dl,version,assets_jp['path']['Data']['QuestParam'])
    param=json.loads(f,encoding='utf8')
    saveAsJSON(path,'QuestParamJP.json', convertParam(param),'GameFiles')

    print('QuestDropParam')
    f=Download_Asset(host_dl,version,assets_jp['path']['Data']['QuestDropParam'])
    param=json.loads(f,encoding='utf8')
    saveAsJSON(path,'QuestDropParamJP.json', convertParam(param),'GameFiles')

    name = 'sys'
    print(name)
    f=Download_Asset(host_dl,version,assets_jp['path']['Loc']['japanese'][name])
    param=f.decode(encoding="utf16", errors="strict")
    saveAsJSON(path,name+'JP.json', convertSys(param,False),'GameFiles')

    name = 'unit'
    print(name)
    f=Download_Asset(host_dl,version,assets_jp['path']['Loc']['japanese'][name])
    param=f.decode(encoding="utf16", errors="strict")
    unitJP=convertUnit(param)
    saveAsJSON(path,name+'JP.json', unitJP,'GameFiles')

    unitJP.update(unit)
    saveAsJSON(path,name+'_AIO.json', unitJP,'GameFiles')

####    OTHER STUFF ###########################################################
    print('\n\n\n####\tOTHER STUFF\t####')
# wytesong's compendium
    print('wytesong\'s copendium')
    # https://docs.google.com/spreadsheets/d/1nNmHzEfU3OSt-VlGma4diO4405fmqDLWJ8LiiOsyRmg
    SSID = '1nNmHzEfU3OSt-VlGma4diO4405fmqDLWJ8LiiOsyRmg'
    url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?id="+SSID
    saveAsJSON(path,'wytesong.json', json.loads(download(url,'utf8')))

# Game's tierlist
    print('Game\'s tierlist')
    # https://docs.google.com/spreadsheets/d/1DWeFk0wiPaDKAYEcmf_9LnMFYy1nBy2lPTNAX52LkPU
    SSID = '1DWeFk0wiPaDKAYEcmf_9LnMFYy1nBy2lPTNAX52LkPU'
    url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?type=array&id="+SSID
    saveAsJSON(path,'tierlist_gl.json', json.loads(download(url,'utf8')))

#### MAPS #######
    print('\n\n##### Leftover Assets #####')
    Download_ALL_Text_Assets(os.path.join(path,'Assets'),assets_gl)
    Download_ALL_Text_Assets(os.path.join(path,'AssetsJP'),assets_jp)
    #Fetch_Assets(os.path.join(path,'Assets'), assets_gl['host'],assets_gl['version'],assets_gl['path'])
    #Fetch_Assets(os.path.join(path,'AssetsJP'), assets_jp['host'],assets_jp['version'],assets_jp['path'])



def saveAsJSON(path, name, var, subdir=False):
    if subdir:
        fpath=os.path.join(path,subdir)
    else:
        fpath=path
    os.makedirs(fpath, exist_ok=True)
    fpath=os.path.join(fpath,name)
    with open(fpath, "wb") as f:
        f.write(json.dumps(var, indent=4, ensure_ascii=False).encode('utf8'))

def download(url, decoder='utf-8-sig'):
    resource = urllib.request.urlopen(url)
    try:
        return resource.read().decode(decoder)
    except:
        print('failed decoding')

def convertLoc(f):
    loc = {}
    f = f.replace('\r', '').split('\n')
    for line in f:
        if len(line) > 1:
            Param = line.index('Param_')+6
            line = line[Param:]
            try:
                [line, text] = line.split('\t')
                lI = line.rindex('_')
                iname = line[:lI]
                type = line[lI+1:]
            except:
                lI = line.rindex('_')
                iname = line[:lI]
                type = line[lI+1:]
                text = ""

            if iname not in loc:
                loc[iname] = {}
            loc[iname][type.lower()] = text

    return loc

def convertUnit(f):
    unit = {}
    f = f.replace('\r', '').split('\n')
    for line in f:
        if len(line) > 1:
            lI = line.rindex('_')
            iname = line[:lI]
            try:
                fI = line.index('\t')
                type = line[lI+1:fI]
                text = line[fI+1:]
            except:
                type = line[lI+1:]
                text = ""

            if iname not in unit:
                unit[iname] = {}
            unit[iname][type.lower()] = text

    return unit

def convertSys(f,FIX=True):
    if FIX:
        f = f.replace("MagicAttack	Modify MATK","MagicAttack	Modify Magic ATK").replace('Modify ','')
        SYS_FIX={
            "Assist_Fire" : "Fire Unit Dmg",
            "Assist_Water" : "Water Unit Dmg",
            "Assist_Wind" : "Wind Unit Dmg",
            "Assist_Thunder" : "Thunder Unit Dmg",
            "Assist_Shine" : "Shine Unit Dmg",
            "Assist_Dark" : "Dark Unit Dmg",
            "Resist_Fire" : "Fire Unit Dmg Res",
            "Resist_Water" : "Water Unit Dmg Res",
            "Resist_Wind" : "Wind Unit Dmg Res",
            "Resist_Thunder" : "Thunder Unit Dmg Res",
            "Resist_Shine" : "Light Unit Dmg Res",
            "Resist_Dark" : "Dark Unit Dmg Res",
            "Assist_ESA_Fire" : "Water Skill Dmg vs Fire",
            "Assist_ESA_Water" : "Thunder Skill Dmg against Water",
            "Assist_ESA_Wind" : "Fire Skill Dmg against Wind",
            "Assist_ESA_Thunder" : "Wind Skill Dmg against Thunder",
            "Assist_ESA_Shine" : "Dark Skill Dmg against Light",
            "Assist_ESA_Dark" : "Light Skill Dmg against Dark",
            "Resist_ESA_Fire" : "Water Skill Dmg vs Fire Res",
            "Resist_ESA_Water" : "Thunder Skill Dmg against Water Res",
            "Resist_ESA_Wind" : "Fire Skill Dmg against Wind Res",
            "Resist_ESA_Thunder" : "Wind Skill Dmg against Thunder Res",
            "Resist_ESA_Shine" : "Dark Skill Dmg against Light Res",
            "Resist_ESA_Dark" : "Light Skill Dmg against Dark Res",
            "UnitDefenseFire" : "Fire Skill Dmg Red",
            "UnitDefenseWater" : "Water Skill Dmg Red",
            "UnitDefenseWind" : "Wind Skill Dmg Red",
            "UnitDefenseThunder" : "Thunder Skill Dmg Red",
            "UnitDefenseShine" : "Light Skill Dmg Red",
            "UnitDefenseDark" : "Dark Skill Dmg Red",
            "Assist_MaxDamageHp" : "Max HP Dmg",
            "Assist_MaxDamageMp" : "Max Jewel Dmg",
            "Resist_MaxDamageHp" : "Max HP Dmg Res",
            "Resist_MaxDamageMp" : "Max Jewel Dmg Res",
            "Tokkou": "Anti-Giant Dmg",
            "Assist_SideAttack" : "Side-Attack Up",
            "Assist_BackAttack" : "Back-Attack Up",
            "Resist_SideAttack" : "Side-Attack Res",
            "Resist_BackAttack" : "Back-Attack Res",
        }
        for key, val in SYS_FIX.items():
            f+= '\n{key}\t{val}'.format(key=key,val=val)
    sys={}
    f = f.replace('\r', '').split('\n')
    for line in f:
        line=line.split('\t')
        if len(line)==2:
            sys[line[0]]=line[1]
        else:
            sys[line[0]]=''
    return sys

def convertParam(f):
    # for main,tree in f.items():
    #     if type(tree)==list and type(tree[0]) == dict and 'iname' in tree[0]:
    #         new_tree={}
    #         for obj in tree:
    #             new_tree[obj['iname']]=obj
    #         f[main]=new_tree
    return f


main()
